# Rogue in the Dark

_Rogue in the Dark_ is a cosmic horror roguelike game.

## Inspiration

_Rogue in the Dark_ draws inspiration from a variety of sources belonging to different media, including:

- The original work of H. P. Lovecraft.
- _Call of Cthulhu_ pen&paper RPG.
- _Mansions of Madness_ boardgame.
- _Alone in the Dark_ computer adventure game.
- _The Evil Dead_ and _Rose Red_ horror movies.

Despite the obvious influence and inspiration taken from Lovecraft's work, and though it may draw elements from it, _Rogue in the Dark_ does not necessarily stick to Lovecraftian canon and lore.

## How to play

The UI is experimental and subject to changes as much as everything else.

Currently, the main controls are:

- directional arrows) move around and interact with objects
- 1-9) select inventory item
- q) quaff/drink potion
- w) wear clothing
- W) wield weapon
- o) open door/crate
- c) close door
- P) push furniture
- p) pull furniture
- g) pick up item
- .) wait a turn

## Developement status

_Rogue in the Dark_ is currently under heavy developement.

It has not reached release status yet, which means that it is not yet actually fun to play.

The current goal is to reach a Proof of Concept status, to test the game's basic concepts and mechanics.

use ritd_world::*;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum GameError {
    NoActiveActor,
    Running,
    PlayerTurn,
    GameOver,
    World(WorldError),
}

impl fmt::Display for GameError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            _ => "a game error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for GameError {}

impl From<WorldError> for GameError {
    fn from(world_error: WorldError) -> GameError {
        GameError::World(world_error)
    }
}

pub type GameResult<T> = Result<T, GameError>;

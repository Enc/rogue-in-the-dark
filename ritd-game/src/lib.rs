//! # Rogue in the Dungeon
//!
//! Rogue in the Dungeon is a traditional fantasy roguelike game.
//!
//! The player delves into a dungeon, fighting the creatures and monsters that inhabit it.
//! The goal is simply to descend as deep as possible before dying.
//!
//! This documentation is a technical reference for the internals of the game engine.

#![forbid(unsafe_code)]

mod dice;
mod error;
mod game;

pub use ritd_ai;
pub use ritd_world;

pub use error::{GameError, GameResult};
pub use game::Game;

use rand_xoshiro::Xoshiro256StarStar;

/// The chosen RNG type.
pub type GameRng = Xoshiro256StarStar;

/// The seed for the RNG.
pub type GameSeed = [u8; 32];

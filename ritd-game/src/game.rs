//! This module provides the main tools to create a game and play it.

use crate::{GameError, GameResult};
use crate::{GameRng, GameSeed};

use rand::prelude::*;
use rayon::prelude::*;
use ritd_ai::*;
use ritd_event::{Event, State};
use ritd_gen::*;
use ritd_world::*;
use serde::{Deserialize, Serialize};
use slotmap::SecondaryMap;

/// `Game` owns all the game's data and provides an interface to interact with it and make it progress.
#[derive(Serialize, Deserialize)]
pub struct Game {
    world: World,
    state: State,
    player: ActorKey,
    ais: SecondaryMap<ActorKey, Box<dyn Player>>,
    rng: GameRng,
}

impl Game {
    pub fn state(&self) -> State {
        self.state
    }

    /// Generate a new game from the provided seed.
    pub fn generate(seed: Option<GameSeed>, human: Box<dyn Player>) -> Game {
        let seed = seed.unwrap_or_else(random);
        let mut rng = GameRng::from_seed(seed);

        // generate the world
        let mut world = world_gen(&mut rng);

        // generate the player's actor
        // add the player's actor in the world
        // This is the wrong starting point, it should be at the main door instead!
        let landing = FloorCoord {
            coord: Coord::new(
                FLOOR_ROWS as CoordIndex - 2,
                (FLOOR_COLS as CoordIndex / 4) * 2 - 1,
            )
            .expect("coord is within bounds"),
            floor: 0,
        };
        let player = Actor::new(ActorKind::Player, Alignment::Player);
        let player = gen_actor(&mut world, player, landing, &mut rng);
        let mut ais = SecondaryMap::new();
        ais.insert(player, human);

        let state = State::Paused;

        Game {
            world,
            state,
            player,
            ais,
            rng,
        }
    }

    pub fn run(&mut self) -> GameResult<()> {
        match self.state {
            State::Running => panic!("should not be possible"),
            State::Paused => self.state = State::Running,
            State::Victory | State::Defeat | State::Corrupted => return Err(GameError::GameOver),
        }
        while matches!(self.state, State::Running) {
            if let Some(actor_key) = self.world.time().active_actor() {
                let action = if let Some(ai) = self
                    .ais
                    .entry(actor_key)
                    .map(|e| e.or_insert(Box::new(MobAI::new(self.rng.gen()))))
                {
                    let active_view = WorldView::new(actor_key, &self.world);
                    ai.decide(&active_view)
                } else {
                    // `SecondaryMap::entry` returns `None` if the key was removed from the originating `SlotMap`.
                    // In such case, just skip this `Entity`.
                    // TODO: assign AI to all actors at initialization,
                    // and make a missing one an error.
                    continue;
                };

                match action.act(actor_key, &self.world) {
                    Ok(events) => {
                        // Scheduling before or after Event::apply?
                        let time_cost = action.time_cost(actor_key, &self.world, &mut self.rng)?;
                        self.world.schedule_actor(actor_key, time_cost)?;

                        for event in &events {
                            let outcome = self.apply(event);
                            if outcome.is_err() {
                                self.state = State::Corrupted;
                                return outcome;
                            }
                        }

                        let ai = self.ais.get_mut(actor_key).expect("AI performing action");
                        let active_view = WorldView::new(actor_key, &self.world);
                        ai.outcome(&active_view, action, Ok(()));
                    }
                    Err(world_error) => {
                        let ai = self.ais.get_mut(actor_key).expect("AI performing action");
                        let active_view = WorldView::new(actor_key, &self.world);
                        ai.outcome(&active_view, action, Err(world_error));
                    }
                }
            } else {
                // If there is no active actor,
                // then it's time for the clock to tick.
                let outcome = self.apply(&Event::TimeTick);
                if outcome.is_err() {
                    self.state = State::Corrupted;
                    return outcome;
                }
            }
        }
        Ok(())
    }

    pub fn apply(&mut self, event: &Event) -> GameResult<()> {
        if event.is_applicable(&self.world).is_ok() {
            // If applying an event results in an error, then the Game state has become corrupted, so it should return error.
            let consequences =
                event.apply(&mut self.world, &mut self.state, self.player, &mut self.rng)?;
            // Game::broadcast before or after Event::apply?
            self.broadcast(event)?;

            // Run consequences
            // consequences.iter().for_each(|event| {
            //     let _ = self.apply(event).ok();
            // });
            for consequence in &consequences {
                self.apply(consequence).ok();
            }
        }

        Ok(())
    }

    fn broadcast(&mut self, event: &Event) -> GameResult<()> {
        // AIs react to the event.
        // TODO: ugly hack because SlotMap does not (yet) support rayon.
        // See https://github.com/orlp/slotmap/issues/98
        self.ais
            .iter_mut()
            .collect::<Vec<(ActorKey, &mut Box<dyn Player>)>>()
            .par_iter_mut()
            .for_each(|(actor_key, ai)| {
                if self.world.actor_exist(*actor_key).is_ok() {
                    let view = WorldView::new(*actor_key, &self.world);
                    if view.witness(event).unwrap_or(false) {
                        ai.react(&view, event);
                    }
                }
            });

        Ok(())
    }

    // fn time_tick(&mut self) -> GameResult<()> {
    //     self.world.time_tick()?;
    //     self.expand_fire()?;

    //     // Spawn new mobs
    //     let spawn_prob = 0.01 * self.world.evidence() as f64 / World::TOTAL_EVIDENCE as f64;
    //     if self.rng.gen_bool(spawn_prob) {
    //         let coord = if self.rng.gen() {
    //             Coord {
    //                 row: if self.rng.gen() { 1 } else { FLOOR_ROWS - 2 } as CoordIndex,
    //                 col: (self.rng.gen_range(0..FLOOR_COLS - 1) / 2 * 2 + 1) as CoordIndex,
    //             }
    //         } else {
    //             Coord {
    //                 row: (self.rng.gen_range(0..FLOOR_ROWS - 1) / 2 * 2 + 1) as CoordIndex,
    //                 col: if self.rng.gen() { 1 } else { FLOOR_COLS - 2 } as CoordIndex,
    //             }
    //         };
    //         // TODO: select floor in some sensible way
    //         let floor_coord = FloorCoord { coord, floor: 0 };
    //         // Try to spawn in selected coord, but just give up if it raises an error.
    //         self.spawn(floor_coord, ActorKind::Cultist).ok();
    //     }

    //     for actor_key in self.world.actors().keys().collect::<Vec<ActorKey>>() {
    //         self.actor_time_tick(actor_key)?;
    //     }
    //     self.broadcast(Event::TimeTick)?;
    //     Ok(())
    // }

    // fn expand_fire(&mut self) -> GameResult<()> {
    //     // TODO: reimplement fire
    //     //     let on_fire: Vec<FloorCoord> = self
    //     //         .world
    //     //         .tile_map()
    //     //         .tiles()
    //     //         .indexed_iter()
    //     //         .filter_map(|(idx, tile)| {
    //     //             if let Some(Furniture::Fire { .. }) = tile.feature() {
    //     //                 Some(FloorCoord::from(idx))
    //     //             } else {
    //     //                 None
    //     //             }
    //     //         })
    //     //         .collect();
    //     //     for coord in on_fire {
    //     //         for &dir in &DIRS {
    //     //             if let Some(fuel) = self
    //     //                 .world
    //     //                 .tile_map()
    //     //                 .tile(coord + dir)
    //     //                 .ok()
    //     //                 .and_then(Tile::feature)
    //     //                 .and_then(Furniture::flammable)
    //     //             {
    //     //                 self.world
    //     //                     .with_feature(coord + dir, Some(Furniture::Fire { fuel }))
    //     //                     .expect("set feature");
    //     //             }
    //     //         }
    //     //     }
    //     Ok(())
    // }

    // fn actor_time_tick(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     if let Ok(coord) = self.world.actor_coord(actor_key) {
    //         if self
    //             .world
    //             .tile_map()
    //             .tile(coord)
    //             .map_err(GameError::from)?
    //             .fire
    //             > 0
    //         {
    //             self.world.set_actor_on_fire(actor_key)?;
    //         }
    //     }

    //     burn(&mut self.world, actor_key)?;
    //     bleed(&mut self.world, actor_key)?;

    //     if is_dead(&self.world, actor_key)? {
    //         self.die(actor_key)?;
    //     }

    //     // let actor = self.world.actor_mut(actor_key)?;

    //     // // Burn
    //     // if actor.burning > 0 {
    //     //     actor.burn();
    //     //     // Queue effect
    //     // }

    //     // // Bleed
    //     // if actor.bleeding() > 0 {
    //     //     actor.bleed();
    //     //     // TODO: reimplement blood
    //     //     // let coord = self.world.actor_coord(actor_key)?;
    //     //     // let tile = self.world.tile_map().tile(coord)?;
    //     //     // if tile.feature().is_none() && tile.steppable() {
    //     //     //     self.world.with_feature(coord, Some(Furniture::Blood))?;
    //     //     // }
    //     // }
    //     Ok(())
    // }

    // /// Make `actor_key` perform an action.
    // fn act(&self, actor_key: ActorKey, action: Action) -> WorldResult<Vec<Event>> {
    //     match action {
    //         // Action::AscendStairs => self.ascend_stairs(actor_key)?,
    //         Action::Close(dir) => {
    //             let coord = self.world.actor_coord(actor_key)? + dir;
    //             let close = Event::Tile(coord, TileEvent::Close);
    //             close.is_applicable(&self.world)?;
    //             Ok(vec![close])
    //         }
    //         // Action::DescendStairs => self.descend_stairs(actor_key)?,
    //         Action::DropItem(item_key) => {
    //             let mut events = Vec::new();
    //             let drop = Event::DropItem {
    //                 actor_key,
    //                 item_key,
    //             };
    //             if let Some(armor_key) = self.world.equipped_armor(actor_key)? {
    //                 if armor_key == item_key {
    //                     let unwear = Event::Actor(actor_key, ActorEvent::Unwear);
    //                     unwear.is_applicable(&self.world)?;
    //                     events.push(unwear);
    //                 }
    //             } else if let Some(weapon_key) = self.world.equipped_weapon(actor_key)? {
    //                 if weapon_key == item_key {
    //                     let unwield = Event::Actor(actor_key, ActorEvent::Unwield);
    //                     unwield.is_applicable(&self.world)?;
    //                     events.push(unwield);
    //                 }
    //             } else {
    //                 drop.is_applicable(&self.world)?;
    //             }
    //             events.push(drop);
    //             Ok(events)
    //         }
    //         Action::EquipArmor(item_key) => {
    //             let mut events = Vec::new();
    //             self.world.has_item(actor_key, item_key)?;
    //             if let Item::Wearable(_) = *self.world.item(item_key)? {
    //                 if let Some(armor_key) = self.world.equipped_armor(actor_key)? {
    //                     if armor_key == item_key {
    //                         // TODO: fix error with "AlreadyWorn"
    //                         return Err(WorldError::NotAnArmor(item_key));
    //                     } else {
    //                         let unequip = Event::Actor(actor_key, ActorEvent::Unwear);
    //                         events.push(unequip);
    //                     }
    //                 }
    //                 let equip = Event::Actor(actor_key, ActorEvent::Wear(item_key));
    //                 events.push(equip);
    //                 Ok(events)
    //             } else {
    //                 Err(WorldError::NotAnArmor(item_key))
    //             }
    //         }
    //         Action::EquipWeapon(item_key) => {
    //             let mut events = Vec::new();
    //             self.world.has_item(actor_key, item_key)?;
    //             if let Item::Weapon(_) | Item::Firearm { .. } = *self.world.item(item_key)? {
    //                 if let Some(weapon_key) = self.world.equipped_weapon(actor_key)? {
    //                     if weapon_key == item_key {
    //                         // TODO: fix error with "AlreadyWielded"
    //                         return Err(WorldError::NotAWeapon(item_key));
    //                     } else {
    //                         let unequip = Event::Actor(actor_key, ActorEvent::Unwield);
    //                         events.push(unequip);
    //                     }
    //                 }
    //                 let equip = Event::Actor(actor_key, ActorEvent::Wield(item_key));
    //                 events.push(equip);
    //                 Ok(events)
    //             } else {
    //                 Err(WorldError::NotAWeapon(item_key))
    //             }
    //         }
    //         Action::Examine(dir) => {
    //             let coord = self.world.actor_coord(actor_key)? + dir;
    //             let examine = Event::Actor(actor_key, ActorEvent::Examine(coord));
    //             examine.is_applicable(&self.world)?;
    //             Ok(vec![examine])
    //         }
    //         Action::Fire(dir) => {
    //             let fire = Event::Actor(actor_key, ActorEvent::Fire(dir));
    //             fire.is_applicable(&self.world)?;
    //             Ok(vec![fire])
    //         }
    //         // Action::Jump(_dir) => todo!(), // self.jump(actor_key, dir)?,
    //         Action::MeleeAttack(dir) => {
    //             let target_coord = self.world.actor_coord(actor_key)? + dir;
    //             if let Some(target_key) = self.world.actor_at(target_coord) {
    //                 Ok(vec![Event::MeleeAttack {
    //                     actor_key,
    //                     target_key,
    //                 }])
    //             } else {
    //                 Err(WorldError::NoMeleeTarget(target_coord))
    //             }
    //         }
    //         Action::Open(dir) => {
    //             let coord = self.world.actor_coord(actor_key)? + dir;
    //             let open = Event::Tile(coord, TileEvent::Open);
    //             open.is_applicable(&self.world)?;
    //             Ok(vec![open])
    //         }
    //         Action::Pick => {
    //             let coord = self.world.actor_coord(actor_key)?;
    //             let item_key = self
    //                 .world
    //                 .item_at(ItemCoord::Lay(coord))
    //                 .ok_or(WorldError::NoItem(coord))?;
    //             let pick = Event::Pick {
    //                 actor_key,
    //                 item_key,
    //             };
    //             pick.is_applicable(&self.world)?;
    //             Ok(vec![pick])
    //         }
    //         Action::Pause => Ok(vec![Event::Pause]),
    //         Action::PrayAltar { dir, stat } => {
    //             let coord = self.world.actor_coord(actor_key)? + dir;
    //             let pray = Event::Pray {
    //                 actor_key,
    //                 coord,
    //                 stat,
    //             };
    //             pray.is_applicable(&self.world)?;
    //             Ok(vec![pray])
    //         }
    //         Action::Pull(dir) => {
    //             let pull = Event::Pull { actor_key, dir };
    //             pull.is_applicable(&self.world)?;
    //             Ok(vec![pull])
    //         }
    //         Action::Push(dir) => {
    //             let push = Event::Push { actor_key, dir };
    //             push.is_applicable(&self.world)?;
    //             Ok(vec![push])
    //         }
    //         Action::Quaff(item_key) => {
    //             let quaff = Event::Actor(actor_key, ActorEvent::Quaff(item_key));
    //             quaff.is_applicable(&self.world)?;
    //             Ok(vec![quaff])
    //         }
    //         Action::Read(item_key) => {
    //             let read = Event::Actor(actor_key, ActorEvent::Read(item_key));
    //             read.is_applicable(&self.world)?;
    //             Ok(vec![read])
    //         }
    //         Action::Step(dir) => {
    //             let step = Event::Actor(actor_key, ActorEvent::Step(dir));
    //             step.is_applicable(&self.world)?;
    //             Ok(vec![step])
    //         }
    //         Action::Throw { item_key, dir } => {
    //             let mut events = Vec::new();
    //             let throw = Event::Throw {
    //                 actor_key,
    //                 item_key,
    //                 dir,
    //             };
    //             if let Some(armor_key) = self.world.equipped_armor(actor_key)? {
    //                 if armor_key == item_key {
    //                     let unwear = Event::Actor(actor_key, ActorEvent::Unwear);
    //                     unwear.is_applicable(&self.world)?;
    //                     events.push(unwear);
    //                 }
    //             } else if let Some(weapon_key) = self.world.equipped_weapon(actor_key)? {
    //                 if weapon_key == item_key {
    //                     let unwield = Event::Actor(actor_key, ActorEvent::Unwield);
    //                     unwield.is_applicable(&self.world)?;
    //                     events.push(unwield);
    //                 }
    //             } else {
    //                 throw.is_applicable(&self.world)?;
    //             }
    //             events.push(throw);
    //             Ok(events)
    //         }
    //         Action::Unlock(dir) => {
    //             let coord = self.world.actor_coord(actor_key)? + dir;
    //             let unlock = Event::Unlock { actor_key, coord };
    //             unlock.is_applicable(&self.world)?;
    //             Ok(vec![unlock])
    //         }
    //         Action::Wait => Ok(vec![Event::Actor(actor_key, ActorEvent::Wait)]),
    //         Action::Zap { wand_key, dir } => {
    //             let zap = Event::Zap {
    //                 actor_key,
    //                 wand_key,
    //                 dir,
    //             };
    //             zap.is_applicable(&self.world)?;
    //             Ok(vec![zap])
    //         }
    //         _ => todo!(),
    //     }
    // }

    // fn time_cost(&mut self, actor_key: ActorKey, action: Action) -> GameResult<TimeUnit> {
    //     const QUICK: TimeUnit = 2;
    //     const NORMAL: TimeUnit = 3;
    //     const SLOW: TimeUnit = 5;

    //     match action {
    //         Action::Wait => Ok(QUICK),
    //         Action::MeleeAttack(_)
    //         | Action::Close(_)
    //         | Action::Examine(_)
    //         | Action::Fire(_)
    //         | Action::Jump(_)
    //         | Action::Open(_)
    //         | Action::Pick
    //         | Action::DropItem(_)
    //         | Action::Throw { .. }
    //         | Action::Quaff(_)
    //         | Action::Read(_)
    //         | Action::Unlock(_)
    //         | Action::Zap { .. } => Ok(NORMAL),
    //         Action::Step(..) | Action::Pull(..) | Action::Push(..) => {
    //             let actor = self.world.actor(actor_key)?;
    //             let dex = actor.dexterity as TimeUnit;
    //             let time = ((NORMAL * 6) / dex)
    //                 + if self.rng.gen_ratio((NORMAL * 6) % dex, dex) {
    //                     1
    //                 } else {
    //                     0
    //                 };

    //             Ok(time)
    //         }
    //         // Action::Throw { item_key, dir } => Ok(3),
    //         Action::DescendStairs
    //         | Action::AscendStairs
    //         | Action::EquipArmor(_)
    //         // | Action::EquipFirearm(_)
    //         | Action::EquipWeapon(_)
    //         | Action::PrayAltar { .. } => Ok(SLOW),
    //         Action::Pause => Ok(0),
    //         _ => todo!(),
    //     }
    // }

    // fn spawn(&mut self, floor_coord: FloorCoord, actor_kind: ActorKind) -> GameResult<()> {
    //     let actor = Actor::new(actor_kind, Alignment::Dungeon);
    //     let _ = self.world.insert_actor(actor, floor_coord)?;
    //     Ok(())
    // }

    // fn ascend_stairs(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)?;
    //     let tile = self.world.tile_map().tile(coord)?;

    //     if let TileKind::StairsUp = tile.kind {
    //         let stairs = self
    //             .world
    //             .plan()
    //             .get((coord.floor + 1) as usize)
    //             .expect("lower floor")
    //             .stairs
    //             .expect("stairs coord");
    //         let landing_coord = FloorCoord {
    //             coord: stairs,
    //             floor: coord.floor + 1,
    //         };
    //         self.world.move_actor(actor_key, landing_coord)?;
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::AscendStairs))
    //     } else {
    //         Err(GameError::World(WorldError::NotStairs(coord)))
    //     }
    // }

    // fn descend_stairs(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)?;
    //     let tile = self.world.tile_map().tile(coord)?;

    //     if let TileKind::StairsDown = tile.kind {
    //         let landing = self
    //             .world
    //             .plan()
    //             .get((coord.floor - 1) as usize)
    //             .expect("lower floor")
    //             .landing
    //             .expect("landing coord");
    //         let landing_coord = FloorCoord {
    //             coord: landing,
    //             floor: coord.floor - 1,
    //         };
    //         self.world.move_actor(actor_key, landing_coord)?;
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::DescendStairs))
    //     } else {
    //         Err(GameError::World(WorldError::NotStairs(coord)))
    //     }
    // }

    // fn step(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key).map_err(GameError::from)? + dir;
    //     let actor = self.world.actor(actor_key)?;
    //     // let tile = self.world.tile_map().tile(coord)?;
    //     if self.world.tile_map().is_impassable(coord)? {
    //         Err(GameError::World(WorldError::Impassable(coord)))
    //     } else if !self.world.is_steppable(coord)? && !actor.kind().flying() {
    //         Err(GameError::World(WorldError::NotSteppable(coord)))
    //     } else if let Some(_other_key) = self.world.actor_at(coord) {
    //         Err(GameError::World(WorldError::NotEmpty(coord)))
    //     } else {
    //         // TODO: vegetation?
    //         // Trample vegetation
    //         // if !actor.kind().flying() {
    //         //     if let Some(Furniture::Vegetation { .. }) = tile.feature() {
    //         //         self.world
    //         //             .with_feature(coord, Some(Furniture::Vegetation { trampled: true }))?;
    //         //     }
    //         // }

    //         self.world
    //             .step_actor(actor_key, dir)
    //             .map(|_| ())
    //             .map_err(GameError::from)?;
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::Step(dir)))?;

    //         Ok(())
    //     }
    // }

    // fn throw(&mut self, actor_key: ActorKey, item_key: ItemKey, dir: Dir) -> GameResult<()> {
    //     self.world.remove_from_inventory(actor_key, item_key)?;
    //     let coord = self.world.actor_coord(actor_key)?;
    //     let actor = self.world.actor(actor_key)?;
    //     let strength = actor.strength;
    //     self.world.place_item(item_key, ItemCoord::Fly(coord))?;
    //     self.broadcast(Event::Throw {
    //         actor_key,
    //         item_key,
    //         dir,
    //     })?;
    //     self.fly(item_key, strength, dir)
    // }

    // fn fly(&mut self, item_key: ItemKey, strength: u8, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.flying_item_coord(item_key)?;
    //     if strength == 0 || self.world.tile_map().is_impassable(coord + dir)? {
    //         self.fall_item(item_key)?;
    //     } else {
    //         self.world
    //             .move_item(item_key, ItemCoord::Fly(coord + dir))?;
    //         // self.broadcast(Event::Item(item_key, ItemEvent::Fly))?;
    //         if let Some(actor_key) = self.world.actor_at(coord + dir) {
    //             self.broadcast(Event::Hit {
    //                 item_key,
    //                 actor_key,
    //             })?;
    //             self.fall_item(item_key)?;
    //         } else {
    //             self.fly(item_key, strength - 1, dir)?;
    //         }
    //     }
    //     Ok(())
    // }

    // fn fall_item(&mut self, item_key: ItemKey) -> GameResult<()> {
    //     let coord = self.world.flying_item_coord(item_key)?;
    //     let item = self.world.item(item_key)?;
    //     if let Item::Potion(_) = item {
    //         if self.world.is_steppable(coord)? {
    //             self.broadcast(Event::Item(item_key, ItemEvent::PotionShatter))?;
    //             let _ = self.world.delete_item(item_key)?;
    //             // TODO: reimplement flooding
    //             // let tile = self.world.tile_map().tile(coord)?;
    //             // if tile.feature().is_none() {
    //             //     self.world.with_feature(coord, Some(Furniture::Flooded))?;
    //             // }
    //             return Ok(());
    //         }
    //     }
    //     let item_coord = self.roll_item(coord).expect("roll item to free tile");
    //     // if self.world.is_steppable(coord)? {
    //     let _ = self
    //         .world
    //         .move_item(item_key, ItemCoord::Lay(item_coord))
    //         .expect("place item on ground");
    //     Ok(())
    //     // } else if let TileKind::Chasm = tile.kind() {
    //     //     self.broadcast(Event::Item(item_key, ItemEvent::Disappear))?;
    //     //     let _ = self.world.delete_item(item_key).expect("delete item");
    //     //     Ok(())
    //     // } else {
    //     //     unreachable!();
    //     // }
    // }

    // fn fire(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     fire(&mut self.world, actor_key)?;
    //     let actor_coord = self.world.actor_coord(actor_key)?;
    //     let mut bullet_coord = actor_coord + dir;
    //     // self.broadcast(Event::Actor(actor_key, ActorEvent::Shoot))?;
    //     while !self.world.tile_map().is_impassable(bullet_coord)? {
    //         if let Some(target_key) = self.world.actor_at(bullet_coord) {
    //             self.shoot(actor_key, target_key)?;
    //             break;
    //         }
    //         bullet_coord = bullet_coord + dir;
    //     }
    //     self.broadcast(Event::Tile(bullet_coord, TileEvent::Shoot))
    // }

    // fn shoot(&mut self, actor_key: ActorKey, target_key: ActorKey) -> GameResult<()> {
    //     let aim = aim_roll(&self.world, actor_key, &mut self.rng)?;
    //     let dodge = dodge_roll(&self.world, actor_key, &mut self.rng)?;
    //     if aim >= dodge {
    //         self.shoot_hit(actor_key, target_key)
    //     } else {
    //         self.broadcast(Event::ShootMiss {
    //             actor_key,
    //             target_key,
    //         })
    //     }
    // }

    // fn shoot_hit(&mut self, actor_key: ActorKey, target_key: ActorKey) -> GameResult<()> {
    //     let mut damage = shot_damage_roll(&self.world, actor_key, &mut self.rng)?;
    //     self.broadcast(Event::ShootHit {
    //         actor_key,
    //         target_key,
    //     })?;
    //     let defense = defense(&self.world, target_key, damage.kind, &mut self.rng)?;
    //     damage.amount = damage.amount.saturating_sub(defense);
    //     self.take_damage(target_key, damage)?;
    //     Ok(())
    // }

    // fn equip_armor(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     // First unequip previously worn item
    //     let _ = self.world.unequip_armor_from_inventory(actor_key).ok();
    //     self.world.equip_armor_from_inventory(actor_key, item_key)?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Wear(item_key)))
    // }

    // TODO: unequip armor
    // fn unequip_armor(&mut self, actor_key: ActorKey) -> GameResult<ItemKey> {
    //     let item_key = self.world.inventory_unequip_armor(actor_key)?;
    //     // self.broadcast(Event::UnequipArmor {
    //     //     actor_key,
    //     //     item_key,
    //     // })?;
    //     Ok(item_key)
    // }

    // fn equip_firearm(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     let actor = self.world.actor_mut(actor_key)?;
    //     actor
    //         .equip_firearm(item_key)
    //         .map_err(|actor_error| GameError::Actor {
    //             actor_key,
    //             actor_error,
    //         })?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Wield(item_key)))
    // }

    // fn equip_weapon(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     // First unequip previously wielded weapon
    //     let _ = self.world.unequip_weapon_from_inventory(actor_key).ok();
    //     self.world
    //         .equip_weapon_from_inventory(actor_key, item_key)?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Wield(item_key)))
    // }

    // TODO: unequip weapon
    // fn unequip_weapon(&mut self, actor_key: ActorKey) -> GameResult<ItemKey> {
    //     let item_key = self.world.inventory_unequip_weapon(actor_key)?;
    //     // self.broadcast(Event::UnequipWeapon {
    //     //     actor_key,
    //     //     item_key,
    //     // })?;
    //     Ok(item_key)
    // }

    // fn jump(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)?;
    //     let tile = self.world.tile_map().tile(coord + dir)?;

    //     if let TileKind::Chasm = tile.kind() {
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::Jump))?;
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::Fall))?;
    //         self.new_floor();
    //         let actor = self.world.actor(actor_key)?;
    //         let amount = Die(actor.max_health_points() - 1).roll(1, &mut self.rng);
    //         let damage = Damage {
    //             amount,
    //             kind: DamageKind::Physical(PhysicalDamage::Bludgeoning),
    //         };
    //         self.take_damage(actor_key, damage)?;
    //         Ok(())
    //     } else {
    //         Err(GameError::NoChasm(coord))
    //     }
    // }

    // fn melee_attack(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let target_coord = self.world.actor_coord(actor_key)? + dir;
    //     let target_key = self
    //         .world
    //         .actor_at(target_coord)
    //         .ok_or(GameError::World(WorldError::NoMeleeTarget(target_coord)))?;
    //     self.broadcast(Event::MeleeAttack {
    //         actor_key,
    //         target_key,
    //     })?;
    //     let hit_roll = hit_roll(&self.world, actor_key, &mut self.rng)?;
    //     let dodge_roll = dodge_roll(&self.world, actor_key, &mut self.rng)?;
    //     if hit_roll >= dodge_roll {
    //         self.melee_hit(actor_key, target_key)?;
    //     } else {
    //         self.broadcast(Event::MeleeMiss {
    //             actor_key,
    //             target_key,
    //         })?;
    //     }
    //     Ok(())
    // }

    // fn melee_hit(&mut self, actor_key: ActorKey, target_key: ActorKey) -> GameResult<()> {
    //     let mut damage = damage(&self.world, actor_key, &mut self.rng)?;
    //     let defense = defense(&self.world, target_key, damage.kind, &mut self.rng)?;
    //     damage.amount = damage.amount.saturating_sub(defense);
    //     self.broadcast(Event::MeleeHit {
    //         actor_key,
    //         target_key,
    //     })?;
    //     self.take_damage(target_key, damage)?;
    //     Ok(())
    // }

    // fn close(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)? + dir;
    //     // if let Some(furniture) = self.world.tile_map().furniture(coord) {
    //     //     match furniture {
    //     //         Furniture::Crate { open } if !*open => {
    //     //             const INFESTED_PROB: f64 = 0.333;
    //     //             let ft_crate = Furniture::Crate { open: true };
    //     //             self.world.remove_furniture(coord)?;
    //     //             self.world.add_furniture(coord, ft_crate)?;
    //     //             if self.rng.gen_bool(INFESTED_PROB) {
    //     //                 self.spawn(coord, ActorKind::Rat)?;
    //     //             } else {
    //     //                 let hammer = Item::Weapon(Weapon::Hammer);
    //     //                 let hammer_coord = self.roll_item(coord)?;
    //     //                 self.world.add_item(hammer, ItemCoord::Lay(hammer_coord))?;
    //     //             }
    //     //             return Ok(());
    //     //         }
    //     //         _ => {}
    //     //     }
    //     // }

    //     // TODO: cannot close door if actor/item/furniture is in the way.
    //     match self.world.tile_map().tile(coord)?.kind {
    //         TileKind::Door(DoorState::Open) => {
    //             let door = TileKind::Door(DoorState::Closed);
    //             self.world.place_tile(coord, Tile::new(door))?;
    //             return Ok(());
    //         }
    //         _ => {}
    //     }

    //     Err(GameError::World(WorldError::Uninteracting(coord)))
    // }

    // fn examine(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)? + dir;
    //     if let Some(furniture) = self.world.tile_map().furniture(coord)? {
    //         match furniture {
    //             Furniture::Library { searched } if !searched => {
    //                 self.world.remove_furniture(coord)?;
    //                 let ft_library = Furniture::Library { searched: true };
    //                 self.world.add_furniture(coord, ft_library)?;

    //                 const EVIDENCE_PROB: f64 = 0.2;
    //                 const SANITY_PROB: f64 = EVIDENCE_PROB + 0.2;

    //                 let rand_val = self.rng.gen_range(0.0..1.0);
    //                 if rand_val < EVIDENCE_PROB {
    //                     self.reveal_evidence()?;
    //                 } else if rand_val < SANITY_PROB {
    //                     self.lose_sanity(actor_key)?;
    //                 } else {
    //                     let scroll = Item::Scroll(Scroll::Teleportation);
    //                     let scroll_coord = self.roll_item(coord)?;
    //                     self.world
    //                         .add_and_place_item(scroll, ItemCoord::Lay(scroll_coord))
    //                         .expect("place item");
    //                 }
    //                 return Ok(());
    //             }
    //             Furniture::Stove { searched } if !searched => {
    //                 self.world.remove_furniture(coord)?;
    //                 let ft_stove = Furniture::Stove { searched: true };
    //                 self.world.add_furniture(coord, ft_stove)?;

    //                 const FOOD_PROB: f64 = 0.8;

    //                 let rand_val = self.rng.gen_range(0.0..1.0);
    //                 if rand_val < FOOD_PROB {
    //                     self.heal(actor_key)?;
    //                 } else {
    //                     self.lose_sanity(actor_key)?;
    //                 }
    //                 return Ok(());
    //             }
    //             _ => {}
    //         }
    //     }

    //     Ok(())
    // }

    // fn open(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)? + dir;
    //     if let Some(furniture) = self.world.tile_map().furniture(coord)? {
    //         match furniture {
    //             Furniture::Crate { open } if !open => {
    //                 self.world.remove_furniture(coord)?;
    //                 let ft_crate = Furniture::Crate { open: true };
    //                 self.world.add_furniture(coord, ft_crate)?;

    //                 const INFESTED_PROB: f64 = 0.333;
    //                 const EVIDENCE_PROB: f64 = INFESTED_PROB + 0.166;
    //                 let rand_val = self.rng.gen_range(0.0..1.0);
    //                 if rand_val < INFESTED_PROB {
    //                     let spawn = if self.world.evidence()
    //                         < self.rng.gen_range(0..World::TOTAL_EVIDENCE)
    //                     {
    //                         ActorKind::Rat
    //                     } else {
    //                         ActorKind::Zombie
    //                     };
    //                     self.spawn(coord, spawn)?;
    //                 } else if rand_val < EVIDENCE_PROB {
    //                     self.reveal_evidence()?;
    //                 } else {
    //                     let hammer = Item::Weapon(Weapon::Hammer);
    //                     let hammer_coord = self.roll_item(coord)?;
    //                     self.world
    //                         .add_and_place_item(hammer, ItemCoord::Lay(hammer_coord))?;
    //                 }
    //                 return Ok(());
    //             }
    //             _ => {}
    //         }
    //     }

    //     match self.world.tile_map().tile(coord)?.kind {
    //         TileKind::Door(DoorState::Closed) => {
    //             let door = TileKind::Door(DoorState::Open);
    //             self.world.place_tile(coord, Tile::new(door))?;
    //             return Ok(());
    //         }
    //         TileKind::Door(DoorState::Locked(key)) => {
    //             let is_key = |item: &Item| {
    //                 if let Item::Key(keyhole) = item {
    //                     *keyhole == key
    //                 } else {
    //                     false
    //                 }
    //             };

    //             let item_key = *self
    //                 .world
    //                 .inventory(actor_key)?
    //                 .ok_or(GameError::World(WorldError::NoInventory(actor_key)))?
    //                 .iter()
    //                 .find(|item_key| is_key(self.world.item(**item_key).expect("item")))
    //                 .ok_or(GameError::World(WorldError::NoKey))?;
    //             self.world.remove_from_inventory(actor_key, item_key)?;
    //             self.world.delete_item(item_key)?;
    //             let door = TileKind::Door(DoorState::Open);
    //             self.world.place_tile(coord, Tile::new(door))?;
    //             return Ok(());
    //         }
    //         _ => {}
    //     }

    //     Err(GameError::World(WorldError::Uninteracting(coord)))
    // }

    // fn pick(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     let actor_coord = self.world.actor_coord(actor_key)?;
    //     let item_key = self
    //         .world
    //         .laying_item_at(actor_coord)
    //         .ok_or(GameError::World(WorldError::NoItem(actor_coord)))?;
    //     if self.world.is_inventory_full(actor_key)? {
    //         Err(GameError::World(WorldError::InventoryFull(actor_key)))
    //     } else {
    //         let _ = self.world.remove_item(item_key)?;
    //         // assert_eq!(item_coord, actor_coord);
    //         self.world
    //             .add_to_inventory(actor_key, item_key)
    //             .expect("add item to actor's inventory");
    //         self.broadcast(Event::Pick {
    //             actor_key,
    //             item_key,
    //         })
    //     }
    // }

    // fn pray_altar(&mut self, actor_key: ActorKey, dir: Dir, stat: Stat) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)? + dir;

    //     if let Some(Furniture::Altar { used }) = self.world.tile_map().furniture(coord)? {
    //         if used {
    //             Err(GameError::World(WorldError::UsedAltar(coord)))
    //         } else {
    //             let altar = Furniture::Altar { used: true };
    //             self.world.remove_furniture(coord)?;
    //             self.world.add_furniture(coord, altar)?;
    //             self.broadcast(Event::Actor(actor_key, ActorEvent::Blessed(stat)))?;
    //             increase_stat(&mut self.world, actor_key, stat)?;
    //             self.broadcast(Event::Actor(actor_key, ActorEvent::Upgrade(stat)))?;
    //             self.lose_sanity(actor_key)?;
    //             Ok(())
    //         }
    //     } else {
    //         Err(GameError::World(WorldError::NoAltar(coord)))
    //     }
    // }

    // fn lose_sanity(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     reduce_sanity(&mut self.world, actor_key)?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::LoseSanity))?;
    //     if is_insane(&self.world, actor_key)? {
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::Insane))?;
    //         self.die(actor_key)?;
    //     }
    //     Ok(())
    // }

    // fn pull(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)?;
    //     if self.world.tile_map().furniture(coord - dir)?.is_some() {
    //         if self.world.is_steppable(coord + dir)? && self.world.actor_at(coord + dir).is_none() {
    //             self.world.move_furniture(coord - dir, dir)?;
    //             self.step(actor_key, dir)?;
    //             // Swap place with items
    //             if let Some(item_key) = self.world.item_at(ItemCoord::Lay(coord)) {
    //                 let _ = self
    //                     .world
    //                     .move_item(item_key, ItemCoord::Lay(coord - dir))?;
    //             }
    //         }
    //         Ok(())
    //     } else {
    //         Err(GameError::World(WorldError::NoFurniture(coord - dir)))
    //     }
    // }

    // fn push(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     let coord = self.world.actor_coord(actor_key)?;
    //     if self.world.tile_map().furniture(coord + dir)?.is_some() {
    //         if self.world.is_steppable(coord + dir + dir)?
    //             && self.world.actor_at(coord + dir + dir).is_none()
    //         {
    //             self.world.move_furniture(coord + dir, dir)?;
    //             self.step(actor_key, dir)?;
    //             // Swap place with items
    //             if let Some(item_key) = self.world.item_at(ItemCoord::Lay(coord + dir + dir)) {
    //                 let _ = self
    //                     .world
    //                     .move_item(item_key, ItemCoord::Lay(coord + dir))?;
    //             }
    //         }
    //         Ok(())
    //     } else {
    //         Err(GameError::World(WorldError::NoFurniture(coord + dir)))
    //     }
    // }

    // fn quaff(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     if !self.world.has_item(actor_key, item_key)? {
    //         return Err(GameError::World(WorldError::NotInInventory {
    //             actor_key,
    //             item_key,
    //         }));
    //     }
    //     let item = *self.world.item(item_key)?;
    //     if let Item::Potion(potion) = item {
    //         self.world.remove_from_inventory(actor_key, item_key)?;
    //         self.world.delete_item(item_key)?;
    //         self.broadcast(Event::Actor(actor_key, ActorEvent::Quaff(potion)))?;
    //         match potion {
    //             Potion::Health => self.heal(actor_key)?,
    //         }
    //         Ok(())
    //     } else {
    //         Err(GameError::World(WorldError::CannotQuaff(item_key)))
    //     }
    // }

    // fn heal(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     heal(&mut self.world, actor_key)?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Heal))?;
    //     Ok(())
    // }

    // fn read(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     if !self.world.has_item(actor_key, item_key)? {
    //         return Err(GameError::World(WorldError::NotInInventory {
    //             actor_key,
    //             item_key,
    //         }));
    //     }
    //     let item = *self.world.item(item_key)?;
    //     match item {
    //         Item::Necronomicon => self.read_necronomicon(actor_key, item_key),
    //         Item::Scroll(scroll) => {
    //             self.world.remove_from_inventory(actor_key, item_key)?;
    //             self.world.delete_item(item_key)?;
    //             // self.broadcast(Event::Actor(actor_key, ActorEvent::Quaff(potion)))?;
    //             match scroll {
    //                 Scroll::Teleportation => {
    //                     self.teleport(actor_key)?;
    //                     // self.broadcast(Event::Actor(actor_key, ActorEvent::Heal))?;
    //                 }
    //             }
    //             Ok(())
    //         }
    //         _ => Err(GameError::World(WorldError::NonReadable(item_key))),
    //     }
    // }

    // fn teleport(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     let floor = self.rng.gen_range(0..FLOORS);
    //     let coord = self
    //         .world
    //         .plan()
    //         .get(floor)
    //         .expect("a random floor")
    //         .rooms
    //         .choose(&mut self.rng)
    //         .expect("a random room")
    //         .plan
    //         .inner_coords()
    //         .iter()
    //         .map(|&coord| FloorCoord {
    //             coord,
    //             floor: floor as CoordIndex,
    //         })
    //         .filter(|&coord| self.world.can_place_actor(coord).is_ok())
    //         .choose(&mut self.rng)
    //         .expect("a random coord");
    //     self.world
    //         .move_actor(actor_key, coord)
    //         .map(|_| ())
    //         .map_err(GameError::from)?;
    //     // self.broadcast(Event::Actor(actor_key, ActorEvent::Step))?;
    //     Ok(())
    // }

    // fn read_necronomicon(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     self.world.remove_from_inventory(actor_key, item_key)?;
    //     self.world.delete_item(item_key).expect("delete item");
    //     self.broadcast(Event::Necronomicon)?;
    //     self.reveal_evidence()?;
    //     Ok(())
    // }

    // fn reveal_evidence(&mut self) -> GameResult<()> {
    //     self.world.reveal_evidence();
    //     self.broadcast(Event::RevealEvidence)?;
    //     if self.world.evidence() >= World::TOTAL_EVIDENCE {
    //         self.state = State::Victory;
    //         self.broadcast(Event::Victory)?;
    //     }
    //     Ok(())
    // }

    // fn take_damage(&mut self, actor_key: ActorKey, damage: Damage) -> GameResult<()> {
    //     take_damage(&mut self.world, actor_key, damage)?;
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::TakeDamage(damage)))?;
    //     let actor = self.world.actor(actor_key)?;
    //     if actor.health_points == 0 {
    //         self.die(actor_key)?;
    //     }
    //     Ok(())
    // }

    // fn die(&mut self, actor_key: ActorKey) -> GameResult<()> {
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Die))?;
    //     let actor = self.world.actor(actor_key)?;
    //     if let Alignment::Player = actor.alignment() {
    //         self.state = State::Defeat;
    //     }

    //     // If actor_key has an inventory, drop all the items
    //     if let Some(inventory) = self.world.inventory(actor_key)? {
    //         let items = inventory.clone();
    //         for &item_key in &items {
    //             self.drop_item(actor_key, item_key)?;
    //         }
    //     }

    //     let _ = self.ais.remove(actor_key).expect("remove ai");
    //     let _ = self.world.remove_actor(actor_key)?;
    //     Ok(())
    // }

    // fn drop_item(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
    //     let item_coord = self.roll_item(self.world.actor_coord(actor_key)?)?;
    //     if let Some(worn_key) = self.world.equipped_armor(actor_key)? {
    //         if worn_key == item_key {
    //             self.world.unequip_armor_from_inventory(actor_key)?;
    //         }
    //     }
    //     if let Some(wielded_key) = self.world.equipped_weapon(actor_key)? {
    //         if wielded_key == item_key {
    //             self.world.unequip_weapon_from_inventory(actor_key)?;
    //         }
    //     }
    //     self.world.remove_from_inventory(actor_key, item_key)?;
    //     self.world
    //         .place_item(item_key, ItemCoord::Lay(item_coord))
    //         .expect("place item");
    //     self.broadcast(Event::DropItem {
    //         actor_key,
    //         item_key,
    //     })?;
    //     Ok(())
    // }

    // fn roll_item(&self, floor_coord: FloorCoord) -> GameResult<FloorCoord> {
    //     let weight_map = self
    //         .world
    //         .tile_map()
    //         .tiles()
    //         .index_axis(Axis(0), floor_coord.floor as Ix)
    //         .map(|tile| if tile.impassable() { None } else { Some(1) });
    //     let dijkstra_map = floor_coord.coord.dijkstra_map(weight_map.view(), None);
    //     let roll_coord = dijkstra_map
    //         .indexed_iter()
    //         .map(|(idx, dist)| {
    //             (
    //                 FloorCoord::from((floor_coord.floor as Ix, idx.0, idx.1)),
    //                 dist,
    //             )
    //         })
    //         .filter(|&(floor_coord, dist)| {
    //             self.world
    //                 .can_place_item(ItemCoord::Lay(floor_coord))
    //                 .is_ok()
    //                 && dist.is_some()
    //         })
    //         .min_by_key(|(_, dist)| dist.expect("distance"))
    //         .map(|(floor_coord, _)| floor_coord)
    //         .expect("coord where item falls");
    //     Ok(roll_coord)
    // }

    // fn zap(&mut self, actor_key: ActorKey, wand_key: ItemKey, dir: Dir) -> GameResult<()> {
    //     if !self.world.has_item(actor_key, wand_key)? {
    //         return Err(GameError::World(WorldError::NotInInventory {
    //             actor_key,
    //             item_key: wand_key,
    //         }));
    //     }
    //     let item = self.world.item(wand_key)?;

    //     if let Item::Wand(wand) = item {
    //         // let actor = self.world.actor(actor_key)?;
    //         match wand {
    //             Wand::Lightning => self.cast_lightning(actor_key, dir)?,
    //             Wand::FireBolt => self.cast_firebolt(actor_key, dir)?,
    //         }
    //         self.broadcast(Event::Zap {
    //             actor_key,
    //             wand_key,
    //             dir,
    //         })?;
    //         // TODO: wand charges
    //         Ok(())
    //     } else {
    //         Err(GameError::World(WorldError::UnusableItem(wand_key)))
    //     }
    // }

    // fn cast_firebolt(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::CastFireBolt))?;
    //     let intensity = self.world.actor(actor_key)?.intelligence;
    //     let coord = self.world.actor_coord(actor_key)?;
    //     self.firebolt(coord + dir, dir, intensity)?;
    //     Ok(())
    // }

    // fn cast_lightning(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::CastLightning))?;
    //     let intensity = self.world.actor(actor_key)?.intelligence;
    //     let coord = self.world.actor_coord(actor_key)?;
    //     self.lightning(coord + dir, dir, intensity)?;
    //     Ok(())
    // }

    // fn firebolt(&mut self, coord: FloorCoord, dir: Dir, intensity: u8) -> GameResult<()> {
    //     // consequences.push(Command::Burn(coord));
    //     // self.broadcast(Event::Tile(coord, TileEvent::Burn))?;
    //     self.broadcast(Event::Tile(coord, TileEvent::Firebolt(dir)))?;
    //     if self.world.actor_at(coord).is_some()
    //         || self.world.tile_map().is_impassable(coord + dir)?
    //     {
    //         // consequences.push(Command::FireBlast { coord, intensity });
    //         self.fireblast(coord, intensity)?;
    //     } else {
    //         // self.burn(coord, intensity)?;
    //         self.firebolt(coord + dir, dir, intensity)?;
    //     }
    //     Ok(())
    // }

    // fn fireblast(&mut self, floor_coord: FloorCoord, intensity: u8) -> GameResult<()> {
    //     let fire_map = self
    //         .world
    //         .tile_map()
    //         .tiles()
    //         .index_axis(Axis(0), floor_coord.floor as Ix)
    //         .map(|tile| tile.impassable());

    //     let fire_map = floor_coord.coord.l1(fire_map.view(), intensity / 3);
    //     for (idx, _) in fire_map.indexed_iter().filter(|(_, &fire)| fire) {
    //         let coord = FloorCoord::from((floor_coord.floor as Ix, idx.0, idx.1));
    //         self.burn(coord, intensity)?;
    //     }
    //     self.broadcast(Event::Fireblast)
    // }

    // fn burn(&mut self, coord: FloorCoord, intensity: u8) -> GameResult<()> {
    //     self.burn_tile(coord)?;
    //     if let Some(actor_key) = self.world.actor_at(coord) {
    //         self.burn_actor(actor_key, intensity)?;
    //     }
    //     if let Some(item_key) = self.world.laying_item_at(coord) {
    //         self.burn_item(item_key)?;
    //     }
    //     self.broadcast(Event::Tile(coord, TileEvent::Burn))
    // }

    // fn burn_actor(&mut self, actor_key: ActorKey, intensity: u8) -> GameResult<()> {
    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Burn))?;
    //     let damage = Damage {
    //         amount: intensity,
    //         kind: DamageKind::Elemental(Element::Fire),
    //     };
    //     self.take_damage(actor_key, damage)?;
    //     Ok(())
    // }

    // fn burn_item(&mut self, item_key: ItemKey) -> GameResult<()> {
    //     if self.world.item(item_key)?.flammable() {
    //         self.world.delete_item(item_key)?;
    //         self.broadcast(Event::Item(item_key, ItemEvent::Burn))?;
    //     }
    //     Ok(())
    // }

    // fn burn_tile(&mut self, coord: FloorCoord) -> GameResult<()> {
    //     self.world.set_on_fire(coord)?;
    //     self.broadcast(Event::Tile(coord, TileEvent::Burn))
    // }

    // fn lightning(&mut self, coord: FloorCoord, dir: Dir, intensity: u8) -> GameResult<()> {
    //     self.broadcast(Event::Tile(coord, TileEvent::Lightning))?;
    //     if let Some(actor_key) = self.world.actor_at(coord) {
    //         self.fulminate(actor_key, intensity)?;
    //     }
    //     if !self.world.tile_map().tile(coord)?.impassable() {
    //         self.lightning(coord + dir, dir, intensity)?;
    //     }
    //     Ok(())
    // }

    // fn fulminate(&mut self, actor_key: ActorKey, intensity: u8) -> GameResult<()> {
    //     const ELECTRICITY_DRY_MULT: usize = 3;
    //     const ELECTRICITY_WET_MULT: usize = 5;

    //     self.broadcast(Event::Actor(actor_key, ActorEvent::Fulminate))?;

    //     let _coord = self.world.actor_coord(actor_key)?;
    //     let num = ELECTRICITY_DRY_MULT;
    //     // TODO: flooded tile
    //     // if let Some(Furniture::Flooded) = self.world.tile_map().tile(coord)?.feature() {
    //     //     num = ELECTRICITY_WET_MULT;
    //     // } else {
    //     // num = ELECTRICITY_DRY_MULT;
    //     // }
    //     let damage = Damage {
    //         amount: Die(intensity).roll(num, &mut self.rng),
    //         kind: DamageKind::Elemental(Element::Electricity),
    //     };
    //     self.take_damage(actor_key, damage)?;
    //     Ok(())
    // }
}

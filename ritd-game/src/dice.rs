use super::GameRng;
use rand::distributions::{Distribution, Uniform};

#[derive(Clone, Copy, Debug)]
pub struct Die(pub u8);

impl Die {
    pub fn roll(self, n: usize, rng: &mut GameRng) -> u8 {
        Uniform::new_inclusive(1, self.0)
            .sample_iter(rng)
            .take(n)
            .sum::<u8>()
    }
}

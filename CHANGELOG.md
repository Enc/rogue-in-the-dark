# Changelog

## [Unreleased]

### Added

- [x] Floors with descending stairs.
- [x] Room-based proc-gen and item placement.
- [x] Basic combat-system.
- [x] Final boss and victory condition.
- [x] Basic inventory management (pick and drop items).
- [x] Containers (e.g. crates, chests).
- [x] Consumables (e.g. potions).
- [x] Throw items.
- [x] Doors to separate rooms.

### Changed

### Removed

## 0.1.0 - 2018-09-07 [YANKED]

Old game engine, pre-full-rewrite.

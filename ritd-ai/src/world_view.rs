use ritd_event::Event;
use ritd_world::*;

pub struct WorldView<'w> {
    actor_pov: ActorKey,
    world: &'w World,
    sight_map: [[bool; FLOOR_ROWS]; FLOOR_COLS],
}

impl<'w> WorldView<'w> {
    pub const TOTAL_EVIDENCE: u8 = World::TOTAL_EVIDENCE;

    // TODO: check actor_pov exists in world
    pub fn new(actor_pov: ActorKey, world: &'w World) -> WorldView<'w> {
        let sight_map = sight_map(world, actor_pov).expect("sight map");
        WorldView {
            actor_pov,
            world,
            sight_map,
        }
    }

    pub fn pov_key(&self) -> ActorKey {
        self.actor_pov
    }

    pub fn pov_actor(&self) -> &Actor {
        self.world.actor(self.actor_pov).expect("own pov actor")
    }

    pub fn pov_coord(&self) -> FloorCoord {
        self.world
            .actor_coord(self.actor_pov)
            .expect("own pov actor")
    }

    pub fn pov_inventory(&self) -> Option<&Inventory> {
        self.world
            .inventory(self.actor_pov)
            .expect("actor's inventory")
    }

    pub fn revealed_evidence(&self) -> u8 {
        self.world.evidence()
    }

    pub fn sight_map(&self) -> &[[bool; FLOOR_ROWS]; FLOOR_COLS] {
        &self.sight_map
    }

    pub fn can_see_coord(&self, coord: Coord) -> WorldResult<bool> {
        Ok(self.sight_map[coord])
    }

    pub fn can_see_tile(&self, floor_coord: FloorCoord) -> WorldResult<bool> {
        let actor = self.pov_actor();
        match actor.alignment() {
            Alignment::Dungeon => Ok(true),
            Alignment::Player => self.can_see_coord(floor_coord.coord),
        }
    }

    pub fn see_tile(&self, coord: FloorCoord) -> WorldResult<Option<&Tile>> {
        // if self.see_tile(coord)? {
        if self.can_see_tile(coord)? {
            // || self.see_tile(coord)? // a tile seen is always remembered
            self.world.tile_map().tile(coord).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn see_floor_tile(&self, coord: Coord) -> WorldResult<Option<&Tile>> {
        let self_coord = self.world.actor_coord(self.actor_pov)?;
        let floor_coord = FloorCoord {
            coord,
            floor: self_coord.floor,
        };
        self.see_tile(floor_coord)
    }

    pub fn see_actor(&self, actor_key: ActorKey) -> WorldResult<bool> {
        let pov_coord = self.pov_coord();
        if let Ok(coord) = self.world.actor_coord(actor_key) {
            if pov_coord.floor == coord.floor {
                let can_see = self.can_see_coord(coord.coord)?; // && self.is_lit(coord)?;
                Ok(can_see)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }

    pub fn actor(&self, actor_key: ActorKey) -> WorldResult<Option<&Actor>> {
        if self.see_actor(actor_key)? {
            self.world.actor(actor_key).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn actor_at(&self, floor_coord: FloorCoord) -> WorldResult<Option<ActorKey>> {
        if let Some(actor_key) = self.world.actor_at(floor_coord) {
            if self.see_actor(actor_key)? {
                Ok(Some(actor_key))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    pub fn actor_coord(&self, actor_key: ActorKey) -> WorldResult<FloorCoord> {
        self.see_actor(actor_key)?;
        self.world.actor_coord(actor_key)
    }

    // pub fn tile_map(&self, floor: CoordIndex) -> Array2<Option<(Tile, Option<Furniture>)>> {
    //     self.world
    //         .tile_map()
    //         .tiles()
    //         .index_axis(Axis(0), floor as Ix)
    //         .indexed_iter()
    //         .map(|(idx, &tile)|{
    //             let floor_coord = FloorCoord {
    //                 floor,
    //                 coord: Coord::from(idx),
    //             };
    //             if self.can_see_tile(floor_coord).expect("can see") {
    //                 let opt_furniture = self.world.tile_map().furniture(floor_coord).cloned();
    //                 Some((tile, opt_furniture))
    //             } else {
    //                 None
    //             }
    //         })
    //         .collect()
    // }

    pub fn tile_map(&self) -> &TileMap {
        if let Alignment::Dungeon = self.pov_actor().alignment() {
            self.world.tile_map()
        } else {
            todo!()
        }
    }

    // pub fn steppable_map(&self) -> ArrayView2<bool> {
    //     // if let Some(actor_key) = self.actor_pov {
    //     if self.world.memory_map(self.actor_pov).is_none() {
    //         let actor_coord = self.world.actor_coord(self.actor_pov).expect("actor coord");
    //         // if let Ok(coord) = self.world.map().actor_coord(actor_key) {
    //         self.world
    //             .tile_map()
    //             .steppable_map(actor_coord.floor as usize)
    //     // }
    //     } else {
    //         unimplemented!()
    //     }
    //     // } else {
    //     //     self.world.steppable_map()
    //     // }
    // }

    // pub fn impassable_map(&self) -> ArrayView2<bool> {
    //     todo!()
    //     // // if let Some(actor_key) = self.actor_pov {
    //     // if self.world.memory_map(self.actor_pov).is_none() {
    //     //     let actor_coord = self.world.actor_coord(self.actor_pov).expect("actor coord");
    //     //     // if let Ok(coord) = self.world.map().actor_coord(actor_key) {
    //     //     self.world
    //     //         .tile_map()
    //     //         .impassable_map(actor_coord.floor as usize)
    //     // // }
    //     // } else {
    //     //     unimplemented!()
    //     // }
    //     // // } else {
    //     // //     self.world.steppable_map()
    //     // // }
    // }

    pub fn see_item(&self, item_key: ItemKey) -> WorldResult<bool> {
        let pov_coord = self.pov_coord();
        match self.world.item_location(item_key)? {
            Some(ItemCoord::Lay(coord) | ItemCoord::Fly(coord))
                if pov_coord.floor == coord.floor =>
            {
                self.can_see_coord(coord.coord)
            }
            Some(_) => Ok(false),
            None => {
                if let Some(inventory) = self.world.inventory(self.actor_pov)? {
                    Ok(inventory.contains(&item_key))
                } else {
                    Ok(false)
                }
            }
        }
    }

    pub fn item(&self, item_key: ItemKey) -> WorldResult<Option<&Item>> {
        if self.see_item(item_key)? {
            self.world.item(item_key).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn item_at(&self, floor_coord: FloorCoord) -> WorldResult<Option<ItemKey>> {
        let opt_item = self
            .world
            .laying_item_at(floor_coord)
            .filter(|item_key| self.see_item(*item_key).expect("see item"));
        Ok(opt_item)
    }

    pub fn flying_item_at(&self, floor_coord: FloorCoord) -> WorldResult<Option<ItemKey>> {
        let opt_item = self
            .world
            .flying_item_at(floor_coord)
            .filter(|item_key| self.see_item(*item_key).expect("see item"));
        Ok(opt_item)
    }

    pub fn witness(&self, event: &Event) -> WorldResult<bool> {
        match *event {
            Event::Actor(actor_key, _) => self.see_actor(actor_key),
            Event::Tile(coord, _) => self.can_see_tile(coord),
            Event::MeleeAttack {
                actor_key,
                target_key,
            }
            | Event::MeleeHit {
                actor_key,
                target_key,
            }
            | Event::MeleeMiss {
                actor_key,
                target_key,
            } => Ok(self.see_actor(actor_key)? && self.see_actor(target_key)?),
            _ => Ok(true),
        }
    }

    pub fn pov_max_health_points(&self) -> WorldResult<u8> {
        max_health(self.world, self.actor_pov)
    }
}

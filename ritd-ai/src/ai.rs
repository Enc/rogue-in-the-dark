use super::Action;
use crate::WorldView;
use rand::prelude::*;
use rand_xoshiro::Xoshiro256StarStar;
use ritd_event::{ActorEvent, Event};
use ritd_world::{
    dijkstra_map, distance, Actor, Alignment, Coord, Dir, FloorCoord, Tile, TileKind, WorldResult,
    DIRS, FLOOR_COLS, FLOOR_ROWS,
};
use serde::{Deserialize, Serialize};

#[typetag::serde(tag = "type")]
pub trait Player: Send {
    fn decide(&mut self, world_view: &WorldView) -> Action;

    fn react(&mut self, world_view: &WorldView, event: &Event);

    fn outcome(&mut self, world_view: &WorldView, action: Action, outcome: WorldResult<()>);
}

/// The chosen RNG type.
pub type AiRng = Xoshiro256StarStar;

/// The seed for the RNG.
pub type AiSeed = [u8; 32];

#[derive(Debug, Serialize, Deserialize)]
pub struct MobAI {
    target: Option<FloorCoord>,
    waiting: bool,
    rng: AiRng,
}

impl MobAI {
    const ACTOR_WEIGTH: u8 = 15;
    const FURNITURE_WEIGHT: u8 = 5;
    const MAX_DIST: Option<u8> = Some(30);

    pub fn new(seed: AiSeed) -> MobAI {
        MobAI {
            target: None,
            waiting: false,
            rng: AiRng::from_seed(seed),
        }
    }

    fn update_target(&mut self, view: &WorldView) {
        let self_coord = view.actor_coord(view.pov_key()).expect("self position");
        self.target = view
            .sight_map()
            .iter()
            .enumerate()
            .flat_map(|(col, column)| {
                column.iter().enumerate().map(move |(row, see)| {
                    (
                        Coord::new(row, col).expect("coords are within bounds"),
                        *see,
                    )
                })
            })
            .filter_map(|(coord, see)| {
                if see {
                    let floor_coord = FloorCoord {
                        coord,
                        floor: self_coord.floor,
                    };
                    if let Some(actor_key) = view.actor_at(floor_coord).expect("actor at coord") {
                        if let Some(Alignment::Player) = view
                            .actor(actor_key)
                            .expect("actor at coord")
                            .map(Actor::alignment)
                        {
                            return Some(coord);
                        }
                    }
                }
                None
            })
            .min_by_key(|&coord| distance(coord, self_coord.coord))
            .map(|coord| FloorCoord {
                coord,
                floor: self_coord.floor,
            })
            .or(self.target);
    }
}

#[typetag::serde]
impl Player for MobAI {
    fn decide(&mut self, view: &WorldView) -> Action {
        // When AI does not know what to do, it skips a turn.
        if self.waiting {
            self.waiting = false;
            return Action::Wait;
        }

        let actor = view.pov_actor();
        let self_coord = view.actor_coord(view.pov_key()).expect("self position");

        if let Some(target_coord) = self.target {
            if FloorCoord::distance(self_coord, target_coord).expect("distance") == 1 {
                if let Some(target_key) = view.actor_at(target_coord).expect("actor at coord") {
                    if let Some(Alignment::Player) = view
                        .actor(target_key)
                        .expect("actor at coord")
                        .map(Actor::alignment)
                    {
                        return Action::MeleeAttack(
                            self_coord
                                .directions_to(target_coord)
                                .pop()
                                .expect("direction to target"),
                        );
                    }
                }
            }

            let weight_fn = |tile: &Tile| match tile.kind {
                TileKind::Door { .. } => Some(1),
                _ => {
                    if tile.steppable() || (actor.kind().flying() && !tile.impassable()) {
                        if tile.furniture.is_some() {
                            Some(Self::FURNITURE_WEIGHT)
                        } else {
                            Some(1)
                        }
                    } else {
                        None
                    }
                }
            };
            // Compute tiles' weight
            let floor_view = view
                .tile_map()
                .tiles_floor(self_coord.floor)
                .expect("TODO: manage error");
            let mut weight_map: [[Option<u8>; FLOOR_ROWS]; FLOOR_COLS] =
                [[None; FLOOR_ROWS]; FLOOR_COLS];
            for row in 0..FLOOR_ROWS {
                for col in 0..FLOOR_COLS {
                    let coord = Coord::new(row, col).expect("coords within bounds");
                    weight_map[coord] = weight_fn(&floor_view[coord]);
                    let floor_coord = FloorCoord {
                        coord,
                        floor: self_coord.floor,
                    };
                    // Adjust weight with other actors
                    // TODO: optimize by retrieving list of actors instead of checking every coord
                    if view
                        .actor_at(floor_coord)
                        .expect("see actor at coord")
                        .is_some()
                    {
                        weight_map[coord] = Some(Self::ACTOR_WEIGTH);
                    }
                }
            }
            let dijkstra_map = dijkstra_map(target_coord.coord, weight_map, Self::MAX_DIST);
            let mut dirs: Vec<(Dir, u8)> = DIRS
                .iter()
                .filter_map(|dir| {
                    dijkstra_map[(self_coord.coord + *dir).expect("???")].map(|dist| (*dir, dist))
                })
                .collect();
            dirs.shuffle(&mut self.rng);
            if let Some(&(dir, _)) = dirs.iter().min_by_key(|(_, dist)| *dist) {
                Action::Step(dir)
            } else {
                Action::Wait
            }
        } else {
            Action::Wait
        }
    }

    fn react(&mut self, view: &WorldView, event: &Event) {
        match event {
            Event::Actor(actor_key, ActorEvent::Step(_)) if *actor_key == view.pov_key() => {
                let self_coord = view.actor_coord(view.pov_key()).expect("self position");
                self.update_target(view);
                if Some(self_coord) == self.target {
                    self.target = None;
                }
            }
            Event::Actor(actor_key, ActorEvent::Step(_)) => {
                if let Some(Alignment::Player) = view
                    .actor(*actor_key)
                    .expect("view actor")
                    .map(Actor::alignment)
                {
                    self.update_target(view);
                }
            }
            _ => {}
        }
    }

    fn outcome(&mut self, _world_view: &WorldView, _action: Action, outcome: WorldResult<()>) {
        // Do something?
        if outcome.is_err() {
            self.waiting = true;
            // panic!("AIs chosen action failed");
        }
    }
}

use rand::Rng;
use ritd_event::{ActorEvent, Event, TileEvent};
use ritd_world::{
    ActorKey, Dir, Item, ItemCoord, ItemKey, Stat, TimeUnit, World, WorldError, WorldResult,
};
use serde::{Deserialize, Serialize};

#[non_exhaustive]
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Action {
    AscendStairs,
    Close(Dir),
    DescendStairs,
    DropItem(ItemKey),
    EquipArmor(ItemKey),
    EquipWeapon(ItemKey),
    // EquipFirearm(ItemKey),
    Examine(Dir),
    Fire(Dir),
    // Interact(FloorCoord),
    Jump(Dir),
    MeleeAttack(Dir),
    Open(Dir),
    Pause,
    Pick,
    PrayAltar { stat: Stat, dir: Dir },
    Pull(Dir),
    Push(Dir),
    Quaff(ItemKey),
    Read(ItemKey),
    Step(Dir),
    Throw { item_key: ItemKey, dir: Dir },
    Unlock(Dir),
    Wait,
    Zap { wand_key: ItemKey, dir: Dir },
}

impl Action {
    pub fn act(&self, actor_key: ActorKey, world: &World) -> WorldResult<Vec<Event>> {
        match *self {
            // Action::AscendStairs => ascend_stairs(actor_key)?,
            Action::Close(dir) => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let close = Event::Tile(coord, TileEvent::Close);
                close.is_applicable(world)?;
                Ok(vec![close])
            }
            // Action::DescendStairs => descend_stairs(actor_key)?,
            Action::DropItem(item_key) => {
                let mut events = Vec::new();
                let drop = Event::DropItem {
                    actor_key,
                    item_key,
                };
                if let Some(armor_key) = world.equipped_armor(actor_key)? {
                    if armor_key == item_key {
                        let unwear = Event::Actor(actor_key, ActorEvent::Unwear);
                        unwear.is_applicable(world)?;
                        events.push(unwear);
                    }
                } else if let Some(weapon_key) = world.equipped_weapon(actor_key)? {
                    if weapon_key == item_key {
                        let unwield = Event::Actor(actor_key, ActorEvent::Unwield);
                        unwield.is_applicable(world)?;
                        events.push(unwield);
                    }
                } else {
                    drop.is_applicable(world)?;
                }
                events.push(drop);
                Ok(events)
            }
            Action::EquipArmor(item_key) => {
                let mut events = Vec::new();
                world.has_item(actor_key, item_key)?;
                if let Item::Wearable(_) = *world.item(item_key)? {
                    if let Some(armor_key) = world.equipped_armor(actor_key)? {
                        if armor_key == item_key {
                            // TODO: fix error with "AlreadyWorn"
                            return Err(WorldError::NotAnArmor(item_key));
                        } else {
                            let unequip = Event::Actor(actor_key, ActorEvent::Unwear);
                            events.push(unequip);
                        }
                    }
                    let equip = Event::Actor(actor_key, ActorEvent::Wear(item_key));
                    events.push(equip);
                    Ok(events)
                } else {
                    Err(WorldError::NotAnArmor(item_key))
                }
            }
            Action::EquipWeapon(item_key) => {
                let mut events = Vec::new();
                world.has_item(actor_key, item_key)?;
                if let Item::Weapon(_) | Item::Firearm { .. } = *world.item(item_key)? {
                    if let Some(weapon_key) = world.equipped_weapon(actor_key)? {
                        if weapon_key == item_key {
                            // TODO: fix error with "AlreadyWielded"
                            return Err(WorldError::NotAWeapon(item_key));
                        } else {
                            let unequip = Event::Actor(actor_key, ActorEvent::Unwield);
                            events.push(unequip);
                        }
                    }
                    let equip = Event::Actor(actor_key, ActorEvent::Wield(item_key));
                    events.push(equip);
                    Ok(events)
                } else {
                    Err(WorldError::NotAWeapon(item_key))
                }
            }
            Action::Examine(dir) => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let examine = Event::Actor(actor_key, ActorEvent::Examine(coord));
                examine.is_applicable(world)?;
                Ok(vec![examine])
            }
            Action::Fire(dir) => {
                let fire = Event::Actor(actor_key, ActorEvent::Fire(dir));
                fire.is_applicable(world)?;
                Ok(vec![fire])
            }
            // Action::Jump(_dir) => todo!(), // jump(actor_key, dir)?,
            Action::MeleeAttack(dir) => {
                let target_coord = (world.actor_coord(actor_key)? + dir)?;
                if let Some(target_key) = world.actor_at(target_coord) {
                    Ok(vec![Event::MeleeAttack {
                        actor_key,
                        target_key,
                    }])
                } else {
                    Err(WorldError::NoMeleeTarget(target_coord))
                }
            }
            Action::Open(dir) => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let open = Event::Tile(coord, TileEvent::Open);
                open.is_applicable(world)?;
                Ok(vec![open])
            }
            Action::Pick => {
                let coord = world.actor_coord(actor_key)?;
                let item_key = world
                    .item_at(ItemCoord::Lay(coord))
                    .ok_or(WorldError::NoItem(coord))?;
                let pick = Event::Pick {
                    actor_key,
                    item_key,
                };
                pick.is_applicable(world)?;
                Ok(vec![pick])
            }
            Action::Pause => Ok(vec![Event::Pause]),
            Action::PrayAltar { dir, stat } => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let pray = Event::Pray {
                    actor_key,
                    coord,
                    stat,
                };
                pray.is_applicable(world)?;
                Ok(vec![pray])
            }
            Action::Pull(dir) => {
                let pull = Event::Pull { actor_key, dir };
                pull.is_applicable(world)?;
                Ok(vec![pull])
            }
            Action::Push(dir) => {
                let push = Event::Push { actor_key, dir };
                push.is_applicable(world)?;
                Ok(vec![push])
            }
            Action::Quaff(item_key) => {
                let quaff = Event::Actor(actor_key, ActorEvent::Quaff(item_key));
                quaff.is_applicable(world)?;
                Ok(vec![quaff])
            }
            Action::Read(item_key) => {
                let read = Event::Actor(actor_key, ActorEvent::Read(item_key));
                read.is_applicable(world)?;
                Ok(vec![read])
            }
            Action::Step(dir) => {
                let step = Event::Actor(actor_key, ActorEvent::Step(dir));
                step.is_applicable(world)?;
                Ok(vec![step])
            }
            Action::Throw { item_key, dir } => {
                let mut events = Vec::new();
                let throw = Event::Throw {
                    actor_key,
                    item_key,
                    dir,
                };
                if let Some(armor_key) = world.equipped_armor(actor_key)? {
                    if armor_key == item_key {
                        let unwear = Event::Actor(actor_key, ActorEvent::Unwear);
                        unwear.is_applicable(world)?;
                        events.push(unwear);
                    }
                } else if let Some(weapon_key) = world.equipped_weapon(actor_key)? {
                    if weapon_key == item_key {
                        let unwield = Event::Actor(actor_key, ActorEvent::Unwield);
                        unwield.is_applicable(world)?;
                        events.push(unwield);
                    }
                } else {
                    throw.is_applicable(world)?;
                }
                events.push(throw);
                Ok(events)
            }
            Action::Unlock(dir) => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let unlock = Event::Unlock { actor_key, coord };
                unlock.is_applicable(world)?;
                Ok(vec![unlock])
            }
            Action::Wait => Ok(vec![Event::Actor(actor_key, ActorEvent::Wait)]),
            Action::Zap { wand_key, dir } => {
                let zap = Event::Zap {
                    actor_key,
                    wand_key,
                    dir,
                };
                zap.is_applicable(world)?;
                Ok(vec![zap])
            }
            _ => todo!(),
        }
    }

    pub fn time_cost<R: Rng>(
        &self,
        actor_key: ActorKey,
        world: &World,
        rng: &mut R,
    ) -> WorldResult<TimeUnit> {
        const QUICK: TimeUnit = 2;
        const NORMAL: TimeUnit = 3;
        const SLOW: TimeUnit = 5;

        match *self {
            Action::Wait => Ok(QUICK),
            Action::MeleeAttack(_)
            | Action::Close(_)
            | Action::Examine(_)
            | Action::Fire(_)
            | Action::Jump(_)
            | Action::Open(_)
            | Action::Pick
            | Action::DropItem(_)
            | Action::Throw { .. }
            | Action::Quaff(_)
            | Action::Read(_)
            | Action::Unlock(_)
            | Action::Zap { .. } => Ok(NORMAL),
            Action::Step(..) | Action::Pull(..) | Action::Push(..) => {
                let actor = world.actor(actor_key)?;
                let dex = actor.dexterity as TimeUnit;
                let time = ((NORMAL * 6) / dex)
                    + if rng.gen_ratio((NORMAL * 6) % dex, dex) {
                        1
                    } else {
                        0
                    };
                Ok(time)
            }
            // Action::Throw { item_key, dir } => Ok(3),
            Action::DescendStairs
            | Action::AscendStairs
            | Action::EquipArmor(_)
            // | Action::EquipFirearm(_)
            | Action::EquipWeapon(_)
            | Action::PrayAltar { .. } => Ok(SLOW),
            // TODO: 0 time still puts actor at the end of queue?
            Action::Pause => Ok(0),
        }
    }
}

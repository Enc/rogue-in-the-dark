use crate::actor::{DamageKind, PhysicalDamage};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Item {
    Key(Key),
    Wearable(Wearable),
    Potion(Potion),
    Scroll(Scroll),
    Wand(Wand),
    Weapon(Weapon),
    Firearm { firearm: Firearm, ammo: u8 },
    Necronomicon,
}

impl Item {
    pub fn flammable(&self) -> bool {
        matches!(self, Item::Scroll(_))
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Firearm {
    Revolver,
    Shotgun,
}

impl Firearm {
    pub fn damage_mult(self) -> usize {
        match self {
            Firearm::Revolver => 3,
            Firearm::Shotgun => 5,
        }
    }

    pub const fn ammo_capacity(self) -> u8 {
        match self {
            Firearm::Revolver => 6,
            Firearm::Shotgun => 2,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Potion {
    Health,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Scroll {
    Teleportation,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Wand {
    Lightning,
    FireBolt,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Weapon {
    Axe,
    Dagger,
    Knife,
    Hammer,
}

impl Weapon {
    pub fn damage(&self) -> DamageKind {
        match *self {
            Weapon::Axe => DamageKind::Physical(PhysicalDamage::Slashing),
            Weapon::Dagger => DamageKind::Physical(PhysicalDamage::Piercing),
            Weapon::Knife => DamageKind::Physical(PhysicalDamage::Piercing),
            Weapon::Hammer => DamageKind::Physical(PhysicalDamage::Bludgeoning),
        }
    }

    pub fn damage_mult(&self) -> usize {
        match *self {
            Weapon::Axe => 3,
            Weapon::Dagger => 2,
            Weapon::Knife => 2,
            Weapon::Hammer => 4,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Wearable {
    Robe,
    BulletproofVest,
}

impl Wearable {
    pub fn defense(&self) -> u8 {
        match *self {
            Wearable::Robe => 2,
            Wearable::BulletproofVest => 4,
        }
    }

    pub fn dex_malus(&self) -> u8 {
        match *self {
            Wearable::Robe => 1,
            Wearable::BulletproofVest => 2,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub enum Key {
    Iron,
    Brass,
    Silver,
}

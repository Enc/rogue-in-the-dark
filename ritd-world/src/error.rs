use crate::furniture::Furniture;
use crate::world::SptMapError;
use crate::{ActorKey, Coord, Dir, FloorCoord, ItemCoord, ItemKey, TimeError};
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum WorldError {
    CannotQuaff(ItemKey),
    CannotRead(ItemKey),
    CannotZap(ItemKey),
    InventoryFull(ActorKey),
    InventoryNonEmpty(ActorKey),
    NoChasm(FloorCoord),
    NoFurniture(FloorCoord),
    NoInventory(ActorKey),
    NoItem(FloorCoord),
    NoKey,
    NoMeleeTarget(FloorCoord),
    NonReadable(ItemKey),
    // Old WorldError's
    CannotSplitRoom,
    Furniture(FloorCoord, Furniture),
    ImpassableTerrain(FloorCoord),
    InInventory {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    MissingActor(ActorKey),
    MissingItem(ItemKey),
    MissingTile(FloorCoord),
    NoMemoryMap(ActorKey),
    NotAnArmor(ItemKey),
    NotAWeapon(ItemKey),
    NotStairs(FloorCoord),
    NotSameFloor,
    OccupiedCoord {
        actor_key: ActorKey,
        coord: FloorCoord,
    },
    OutOfRange {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    Unsteppable(FloorCoord),
    ActorMap(SptMapError<FloorCoord, ActorKey>),
    ItemMap(SptMapError<ItemCoord, ItemKey>),
    ItemOnMap(ItemKey),
    // Tile {
    //     coord: FloorCoord,
    //     tile_error: TileError,
    // },
    Time(TimeError),
    Impassable(FloorCoord),
    NoAltar(FloorCoord),
    NoVegetation(FloorCoord),
    NoWeaponEquipped(ActorKey),
    NoArmorEquipped(ActorKey),
    NoFirearmEquipped(ActorKey),
    NotAFirearm(ItemKey),
    NotFlammable(ItemKey),
    NotInInventory {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    NotOnSameTile {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    NotSteppable(FloorCoord),
    NotEmpty(FloorCoord),
    OutOfAmmo(ItemKey),
    Uninteracting(FloorCoord),
    UnusableItem(ItemKey),
    UsedAltar(FloorCoord),
    Wield {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Worn {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    MissingFloor(usize),
    CoordOutOfBounds(Coord),
    CoordStepOutOfBounds(Coord, Dir),
}

impl fmt::Display for WorldError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            _ => "a game error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for WorldError {}

impl From<SptMapError<ItemCoord, ItemKey>> for WorldError {
    fn from(item_map_error: SptMapError<ItemCoord, ItemKey>) -> WorldError {
        WorldError::ItemMap(item_map_error)
    }
}

impl From<SptMapError<FloorCoord, ActorKey>> for WorldError {
    fn from(actor_map_error: SptMapError<FloorCoord, ActorKey>) -> WorldError {
        WorldError::ActorMap(actor_map_error)
    }
}

impl From<TimeError> for WorldError {
    fn from(time_error: TimeError) -> WorldError {
        WorldError::Time(time_error)
    }
}

pub type WorldResult<T> = Result<T, WorldError>;

mod grid;
mod spt_map;
mod tile_map;

pub use grid::*;
pub use spt_map::{SptMap, SptMapError};
pub use tile_map::TileMap;

use crate::actor::Actor;
use crate::error::{WorldError, WorldResult};
use crate::floor::FloorPlan;
use crate::furniture::Furniture;
use crate::time::{Time, TimeUnit};
use crate::Tile;
use crate::{item::Item, ActorKey, ItemKey, FLOORS};
use serde::{Deserialize, Serialize};
use slotmap::{DenseSlotMap, SecondaryMap, SlotMap};
use std::collections::BTreeSet;

pub type Inventory = BTreeSet<ItemKey>;

/// `World` contains and manages the game world's data.
#[derive(Serialize, Deserialize)]
pub struct World {
    time: Time,
    plan: [FloorPlan; FLOORS],
    tile_map: TileMap,
    items: SlotMap<ItemKey, Item>,
    items_map: SptMap<ItemCoord, ItemKey>,
    actors: DenseSlotMap<ActorKey, Actor>,
    actors_map: SptMap<FloorCoord, ActorKey>,
    inventories: SecondaryMap<ActorKey, Inventory>,
    ownership: SecondaryMap<ItemKey, ActorKey>,
    equipped_weapons: SecondaryMap<ActorKey, ItemKey>,
    equipped_armors: SecondaryMap<ActorKey, ItemKey>,
    evidence: u8,
}

impl World {
    pub const TOTAL_EVIDENCE: u8 = 5 * FLOORS as u8;

    // TODO: remove rng parameter
    pub fn new(plan: [FloorPlan; FLOORS], tile_map: TileMap) -> World {
        World {
            time: Time::default(),
            plan,
            tile_map,
            items: SlotMap::default(),
            items_map: SptMap::default(),
            actors: DenseSlotMap::default(),
            actors_map: SptMap::default(),
            inventories: SecondaryMap::default(),
            ownership: SecondaryMap::default(),
            equipped_weapons: SecondaryMap::default(),
            equipped_armors: SecondaryMap::default(),
            evidence: 0,
        }
    }

    pub fn evidence(&self) -> u8 {
        self.evidence
    }

    pub fn reveal_evidence(&mut self) {
        self.evidence += 1;
    }

    pub fn plan(&self) -> &[FloorPlan; FLOORS] {
        &self.plan
    }

    pub fn can_place_actor(&self, coord: FloorCoord) -> WorldResult<()> {
        self.actors_map
            .can_insert(coord)
            .map_err(WorldError::ActorMap)
        // if self.tile_map.is_impassable(coord)? {
        //     Err(WorldError::Impassable(coord))
        // } else {
        //     Ok(())
        // }
    }

    pub fn can_place_item(&self, location: ItemCoord) -> WorldResult<()> {
        self.items_map
            .can_insert(location)
            .map_err(WorldError::ItemMap)?;
        if let ItemCoord::Lay(coord) = location {
            if !self.is_steppable(coord)? {
                return Err(WorldError::NotSteppable(coord));
            }
        }
        Ok(())
    }

    pub fn tile_map(&self) -> &TileMap {
        &self.tile_map
    }

    pub fn time(&self) -> &Time {
        &self.time
    }

    pub fn add_furniture(&mut self, coord: FloorCoord, furniture: Furniture) -> WorldResult<()> {
        self.tile_map.add_furniture(coord, furniture)
    }

    pub fn remove_furniture(&mut self, coord: FloorCoord) -> WorldResult<Option<Furniture>> {
        self.tile_map.remove_furniture(coord)
    }

    pub fn move_furniture(&mut self, coord: FloorCoord, dir: Dir) -> WorldResult<()> {
        self.tile_map.move_furniture(coord, dir)
    }

    pub fn place_tile(&mut self, coord: FloorCoord, tile: Tile) -> WorldResult<()> {
        self.tile_map.place_tile(coord, tile)
    }

    pub fn set_on_fire(&mut self, floor_coord: FloorCoord) -> WorldResult<()> {
        self.tile_map.set_on_fire(floor_coord)
    }

    pub fn set_actor_on_fire(&mut self, actor_key: ActorKey) -> WorldResult<()> {
        self.actor_mut(actor_key)?.burning = 10;
        Ok(())
    }

    pub fn is_steppable(&self, coord: FloorCoord) -> WorldResult<bool> {
        self.tile_map.is_steppable(coord)
    }

    /// Move an `Actor` and return its old `Coord`.
    pub fn move_actor(
        &mut self,
        actor_key: ActorKey,
        coord: FloorCoord,
    ) -> WorldResult<FloorCoord> {
        self.actor_exist(actor_key)?;
        // self.can_place_actor(coord)?;
        // Move actor to its new coordinates
        self.actors_map
            .r#move(actor_key, coord)
            .map_err(WorldError::ActorMap)
    }

    pub fn actor_at(&self, coord: FloorCoord) -> Option<ActorKey> {
        self.actors_map.key(coord)
    }

    pub fn actors(&self) -> &DenseSlotMap<ActorKey, Actor> {
        &self.actors
    }

    pub fn actor(&self, actor_key: ActorKey) -> WorldResult<&Actor> {
        self.actors
            .get(actor_key)
            .ok_or(WorldError::MissingActor(actor_key))
    }

    pub(crate) fn actor_mut(&mut self, actor_key: ActorKey) -> WorldResult<&mut Actor> {
        self.actors
            .get_mut(actor_key)
            .ok_or(WorldError::MissingActor(actor_key))
    }

    // Check if `ActorKey` corresponds to an existing `Actor`.
    pub fn actor_exist(&self, actor_key: ActorKey) -> WorldResult<()> {
        // Check if `the actor_key` corresponds to an existing actor…
        if self.actors.contains_key(actor_key) {
            Ok(())
        } else {
            // …if not, return an error.
            Err(WorldError::MissingActor(actor_key))
        }
    }

    /// Add a new `Actor` into the `World` and return its `ActorKey`.
    ///
    /// * Add the `Actor` to the `World`.
    /// * Add the `Actor` to the time schedule.
    /// * Add the `Actor` to the map.
    pub fn insert_actor(&mut self, actor: Actor, coord: FloorCoord) -> WorldResult<ActorKey> {
        // Check actor can be placed on coord.
        self.can_place_actor(coord)?;

        // Add `actor` to set of `actors`.
        let actor_key = self.actors.insert(actor);

        // Add `actor` to time schedule.
        self.time
            .insert(actor_key)
            .expect("actor inserted in schedule");

        self.actors_map
            .insert(actor_key, coord)
            .map_err(WorldError::ActorMap)?;

        // TODO: check if actor has Inventory
        if self
            .inventories
            .insert(actor_key, Inventory::default())
            .is_some()
        {
            panic!("inventory already present");
        }

        Ok(actor_key)
    }

    pub fn actor_coord(&self, actor_key: ActorKey) -> WorldResult<FloorCoord> {
        self.actor_exist(actor_key)?;
        let coord = self.actors_map.coord(actor_key).expect("actor's coord");
        Ok(coord)
    }

    /// Remove an actor from the world,
    /// taking care to also remove it from time schedule and from the map.
    /// Returns the removed actor.
    pub fn remove_actor(&mut self, actor_key: ActorKey) -> WorldResult<Actor> {
        // Check the given actor exists.
        self.actor_exist(actor_key)?;

        if let Some(inventory) = self.inventories.get(actor_key) {
            if !inventory.is_empty() {
                return Err(WorldError::InventoryNonEmpty(actor_key));
            }
        }
        if let Some(inventory) = self.inventories.remove(actor_key) {
            assert!(inventory.is_empty());
        }

        self.actors_map
            .remove(actor_key)
            .expect("remove actor from map");

        self.time
            .remove(actor_key)
            .expect("remove actor from schedule");

        // Remove actor from storage
        let actor = self.actors.remove(actor_key).expect("actor to delete");

        Ok(actor)
    }

    /// Move an `Actor` by a step in a given `Dir`.
    /// Returns the actor's previous coordinates.
    pub fn step_actor(&mut self, actor_key: ActorKey, dir: Dir) -> WorldResult<FloorCoord> {
        let coord = self.actor_coord(actor_key)?;
        let coord_step = (coord + dir)?;
        self.move_actor(actor_key, coord_step)
    }

    /// Schedule `actor_key` to activate in `waiting_time`.
    pub fn schedule_actor(
        &mut self,
        actor_key: ActorKey,
        waiting_time: TimeUnit,
    ) -> WorldResult<()> {
        // Check if `the actor_key` corresponds to an existing actor.
        self.actor_exist(actor_key)?;

        self.time.schedule_actor(actor_key, waiting_time)?;
        Ok(())
    }

    /// Make world's time tick.
    pub fn time_tick(&mut self) -> WorldResult<()> {
        self.tile_map.update_fire();
        self.time.tick()?;
        Ok(())
    }

    pub fn item(&self, item_key: ItemKey) -> WorldResult<&Item> {
        self.items
            .get(item_key)
            .ok_or(WorldError::MissingItem(item_key))
    }

    pub fn item_mut(&mut self, item_key: ItemKey) -> WorldResult<&mut Item> {
        self.items
            .get_mut(item_key)
            .ok_or(WorldError::MissingItem(item_key))
    }

    // Check if `ItemKey` corresponds to an existing `Item`.
    pub fn item_exist(&self, item_key: ItemKey) -> WorldResult<()> {
        // Check if the `item_key` corresponds to an existing actor…
        if self.items.contains_key(item_key) {
            Ok(())
        } else {
            // …if not, return an error.
            Err(WorldError::MissingItem(item_key))
        }
    }

    /// Insert a new `Item` into the `World` and return its `ItemKey`.
    pub fn add_item(&mut self, item: Item) -> WorldResult<ItemKey> {
        // Add `item` to set of `items`.
        let item_key = self.items.insert(item);
        Ok(item_key)
    }

    /// Place an `Item` into the `World` and return its `ItemKey`.
    pub fn place_item(&mut self, item_key: ItemKey, location: ItemCoord) -> WorldResult<()> {
        self.item_exist(item_key)?;
        self.can_place_item(location)?;
        self.items_map
            .insert(item_key, location)
            .expect("insert item");
        Ok(())
    }

    pub fn add_and_place_item(&mut self, item: Item, location: ItemCoord) -> WorldResult<ItemKey> {
        self.can_place_item(location)?;
        // Add `item` to set of `items`.
        let item_key = self.items.insert(item);
        self.items_map
            .insert(item_key, location)
            .expect("insert item");
        Ok(item_key)
    }

    pub fn move_item(&mut self, item_key: ItemKey, location: ItemCoord) -> WorldResult<ItemCoord> {
        // Check that the given item exists.
        self.item_exist(item_key)?;
        self.can_place_item(location)?;

        let old_location = self
            .items_map
            .r#move(item_key, location)
            .expect("move item");

        Ok(old_location)
    }

    pub fn remove_item(&mut self, item_key: ItemKey) -> WorldResult<ItemCoord> {
        // Check that the given item exists.
        self.item_exist(item_key)?;

        let old_location = self.items_map.remove(item_key).expect("move item");

        Ok(old_location)
    }

    pub fn item_location(&self, item_key: ItemKey) -> WorldResult<Option<ItemCoord>> {
        // Check that the given item exists.
        self.item_exist(item_key)?;
        Ok(self.items_map.coord(item_key))
    }

    pub fn item_coord(&self, item_key: ItemKey) -> WorldResult<FloorCoord> {
        match self.item_location(item_key)? {
            None | Some(ItemCoord::Fly(_)) => Err(WorldError::MissingItem(item_key)),
            Some(ItemCoord::Lay(coord)) => Ok(coord),
        }
    }

    pub fn flying_item_coord(&self, item_key: ItemKey) -> WorldResult<FloorCoord> {
        match self.item_location(item_key)? {
            None | Some(ItemCoord::Lay(_)) => Err(WorldError::MissingItem(item_key)),
            Some(ItemCoord::Fly(coord)) => Ok(coord),
        }
    }

    pub fn item_at(&self, location: ItemCoord) -> Option<ItemKey> {
        self.items_map.key(location)
    }

    pub fn laying_item_at(&self, coord: FloorCoord) -> Option<ItemKey> {
        self.item_at(ItemCoord::Lay(coord))
    }

    pub fn flying_item_at(&self, coord: FloorCoord) -> Option<ItemKey> {
        self.item_at(ItemCoord::Fly(coord))
    }

    /// Move an `Item` and return its old `Coord`.
    /// Remove an item from the world map.
    pub fn delete_item(&mut self, item_key: ItemKey) -> WorldResult<Item> {
        if self.items_map.coord(item_key).is_some() {
            return Err(WorldError::ItemOnMap(item_key));
        }

        if let Some(&actor_key) = self.ownership.get(item_key) {
            return Err(WorldError::InInventory {
                actor_key,
                item_key,
            });
        }

        let item = self
            .items
            .remove(item_key)
            .ok_or(WorldError::MissingItem(item_key))?;

        Ok(item)
    }

    pub fn inventory(&self, actor_key: ActorKey) -> WorldResult<Option<&Inventory>> {
        self.actor_exist(actor_key)?;
        Ok(self.inventories.get(actor_key))
    }

    pub fn has_item(&self, actor_key: ActorKey, item_key: ItemKey) -> WorldResult<bool> {
        self.actor_exist(actor_key)?;
        self.item_exist(item_key)?;
        Ok(self.ownership.get(item_key).cloned() == Some(actor_key))
        // if self.items.contains_key(item_key) {
        //     Ok(())
        // } else {
        //     Err(GameError::NotInInventory { actor_key, item_key })
        // }
    }

    // TODO: This is a good candidate to not be a method of World
    pub fn is_inventory_full(&self, actor_key: ActorKey) -> WorldResult<bool> {
        self.actor_exist(actor_key)?;

        if let Some(inventory) = self.inventories.get(actor_key) {
            let actor = self.actors.get(actor_key).expect("actor"); // The actor has already been checked to exist
            Ok(inventory.len() >= actor.strength as usize)
        } else {
            Err(WorldError::NoInventory(actor_key))
        }
    }

    pub fn add_to_inventory(&mut self, actor_key: ActorKey, item_key: ItemKey) -> WorldResult<()> {
        self.actor_exist(actor_key)?;
        // self.item_exist(item_key)?;

        if self.item_location(item_key)?.is_some() {
            return Err(WorldError::ItemOnMap(item_key));
        }

        if let Some(&actor_key) = self.ownership.get(item_key) {
            return Err(WorldError::InInventory {
                actor_key,
                item_key,
            });
        }

        if let Some(inventory) = self.inventories.get_mut(actor_key) {
            let actor = self.actors.get(actor_key).expect("actor"); // The actor has already been checked to exist
            if inventory.len() < actor.strength as usize {
                inventory.insert(item_key);
                let previous_owner = self.ownership.insert(item_key, actor_key);
                assert!(previous_owner.is_none(), "the items was not owned");
                Ok(())
            } else {
                Err(WorldError::InventoryFull(actor_key))
            }
        } else {
            Err(WorldError::NoInventory(actor_key))
        }
    }

    pub fn remove_from_inventory(
        &mut self,
        actor_key: ActorKey,
        item_key: ItemKey,
    ) -> WorldResult<()> {
        self.actor_exist(actor_key)?;
        // self.item_exist(item_key)?;

        if let Some(inventory) = self.inventories.get_mut(actor_key) {
            if inventory.remove(&item_key) {
                if let Some(&weapon_key) = self.equipped_weapons.get(actor_key) {
                    if weapon_key == item_key {
                        return Err(WorldError::Wield {
                            actor_key,
                            item_key,
                        });
                    }
                }
                if let Some(&armor_key) = self.equipped_armors.get(actor_key) {
                    if armor_key == item_key {
                        return Err(WorldError::Worn {
                            actor_key,
                            item_key,
                        });
                    }
                }
                let owner_key = self.ownership.remove(item_key).expect("item was owned");
                assert_eq!(actor_key, owner_key);
                Ok(())
            } else {
                Err(WorldError::NotInInventory {
                    actor_key,
                    item_key,
                })
            }
        } else {
            Err(WorldError::NoInventory(actor_key))
        }
    }

    pub fn equipped_weapon(&self, actor_key: ActorKey) -> WorldResult<Option<ItemKey>> {
        self.actor_exist(actor_key)?;

        Ok(self.equipped_weapons.get(actor_key).cloned())
    }

    pub fn equip_weapon_from_inventory(
        &mut self,
        actor_key: ActorKey,
        item_key: ItemKey,
    ) -> WorldResult<()> {
        // self.actor_exist(actor_key)?;
        // self.item_exist(item_key)?;

        if self.has_item(actor_key, item_key)? {
            let item = self.item(item_key).expect("item exists");
            if matches!(item, Item::Weapon(_) | Item::Firearm { .. }) {
                // self.remove_from_inventory(actor_key, item_key).expect("remove item");
                // let previous_owner = self.ownership.insert(item_key, actor_key);
                // assert!(previous_owner.is_none(), "no previous owner");
                self.equipped_weapons.insert(actor_key, item_key);
                Ok(())
            } else {
                Err(WorldError::NotAWeapon(item_key))
            }
        } else {
            Err(WorldError::NotInInventory {
                actor_key,
                item_key,
            })
        }
    }

    pub fn unequip_weapon_from_inventory(&mut self, actor_key: ActorKey) -> WorldResult<ItemKey> {
        self.actor_exist(actor_key)?;

        let weapon_key = self
            .equipped_weapons
            .remove(actor_key)
            .ok_or(WorldError::NoWeaponEquipped(actor_key))?;
        Ok(weapon_key)
    }

    pub fn equipped_armor(&self, actor_key: ActorKey) -> WorldResult<Option<ItemKey>> {
        self.actor_exist(actor_key)?;

        Ok(self.equipped_armors.get(actor_key).cloned())
    }

    pub fn equip_armor_from_inventory(
        &mut self,
        actor_key: ActorKey,
        item_key: ItemKey,
    ) -> WorldResult<()> {
        // self.actor_exist(actor_key)?;
        // self.item_exist(item_key)?;

        if self.has_item(actor_key, item_key)? {
            let item = self.item(item_key).expect("item exists");
            if matches!(item, Item::Wearable(_)) {
                // self.remove_from_inventory(actor_key, item_key).expect("remove item");
                // let previous_owner = self.ownership.insert(item_key, actor_key);
                // assert!(previous_owner.is_none(), "no previous owner");
                self.equipped_armors.insert(actor_key, item_key);
                Ok(())
            } else {
                Err(WorldError::NotAnArmor(item_key))
            }
        } else {
            Err(WorldError::NotInInventory {
                actor_key,
                item_key,
            })
        }
    }

    pub fn unequip_armor_from_inventory(&mut self, actor_key: ActorKey) -> WorldResult<ItemKey> {
        self.actor_exist(actor_key)?;

        let armor_key = self
            .equipped_armors
            .remove(actor_key)
            .ok_or(WorldError::NoArmorEquipped(actor_key))?;
        Ok(armor_key)
    }
}

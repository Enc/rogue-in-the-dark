//! # Rogue in the Dungeon
//!
//! Rogue in the Dungeon is a traditional fantasy roguelike game.
//!
//! The player delves into a dungeon, fighting the creatures and monsters that inhabit it.
//! The goal is simply to descend as deep as possible before dying.
//!
//! This documentation is a technical reference for the internals of the game engine.

#![forbid(unsafe_code)]

mod actor;
mod error;
mod floor;
mod furniture;
mod item;
mod rules;
mod tile;
mod time;
mod world;

pub use actor::{
    Actor, ActorKind, Alignment, Damage, DamageKind, Element, PhysicalDamage, Stat, STATS,
};
pub use error::{WorldError, WorldResult};
pub use floor::*;
pub use furniture::Furniture;
pub use item::{Firearm, Item, Key, Potion, Scroll, Wand, Weapon, Wearable};
pub use rules::*;
pub use tile::{DoorState, FloorKind, Tile, TileKind, WallKind};
pub use time::{TimeError, TimeResult, TimeUnit};
pub use world::{
    dijkstra_map, distance, Coord, CoordIndex, Dir, FloorCoord, Inventory, ItemCoord, TileMap,
    World, DIRS,
};

// Re-export slotmap
pub use slotmap;
use slotmap::new_key_type;
use std::collections::{HashMap, HashSet};

new_key_type! {
    /// The key type for `Actor`s stored in a `SlotMap`.
    pub struct ActorKey;

    /// The key type for `Item`s stored in a `SlotMap`.
    pub struct ItemKey;
}

/// The chosen map type.
pub type Map<K, V> = HashMap<K, V>;

/// The chosen set type.
pub type Set<T> = HashSet<T>;

/// The vertical size of the mansion.
pub const FLOOR_ROWS: usize = 27;

/// The horizontal size of the mansion.
pub const FLOOR_COLS: usize = 81;

/// The number of floors in the mansion.
pub const FLOORS: usize = 1;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Furniture {
    Altar { used: bool },
    Armchair,
    Bed,
    Chair,
    Crate { open: bool },
    Library { searched: bool },
    Stove { searched: bool },
    Table,
}

// Blood,
// Fire { fuel: u8 },
// Flooded,
// Vegetation { trampled: bool },

impl Furniture {
    pub const fn impassable(self) -> bool {
        matches!(self, Furniture::Library { .. })
    }

    pub const fn steppable(self) -> bool {
        false
        // !self.impassable()
        //     && match self {
        //         // Furniture::Door { open } => open,
        //         // Furniture::Blood
        //         // | Furniture::Fire { .. }
        //         // | Furniture::Flooded
        //         // | Furniture::Vegetation { .. } => true,
        //         _ => false,
        //     }
    }

    pub const fn opaque(self) -> bool {
        matches!(self, Furniture::Library { .. })
    }

    pub const fn flammable(&self) -> bool {
        matches!(
            self,
            Furniture::Armchair
                | Furniture::Bed
                | Furniture::Chair
                | Furniture::Crate { .. }
                | Furniture::Library { .. }
                | Furniture::Table
        )
    }

    pub const fn is_movable(self) -> bool {
        matches!(self, Self::Armchair | Self::Chair | Self::Crate { .. })
    }
}

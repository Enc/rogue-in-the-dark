//! Contains the `TileMap` object.

use crate::{
    Coord, Dir, FloorCoord, Furniture, Tile, WorldError, WorldResult, DIRS, FLOOR_COLS, FLOOR_ROWS,
};
use serde::{Deserialize, Serialize};
use serde_with::serde_as;

/// A grid of `Tile`s
#[serde_as]
#[derive(Default, Serialize, Deserialize)]
pub struct TileMap {
    #[serde_as(as = "Vec<[[_; FLOOR_ROWS]; FLOOR_COLS]>")]
    tiles: Vec<[[Tile; FLOOR_ROWS]; FLOOR_COLS]>,
}

impl TileMap {
    // const FILLING_TILE: TileKind = TileKind::Floor(FloorKind::Plank);

    pub fn add_floor(&mut self, floor: [[Tile; FLOOR_ROWS]; FLOOR_COLS]) {
        self.tiles.push(floor);
    }

    pub fn tile(&self, coord: FloorCoord) -> WorldResult<&Tile> {
        let floor = self
            .tiles
            .get(coord.floor)
            .ok_or(WorldError::MissingTile(coord))?;
        Ok(&floor[coord.coord])
    }

    fn tile_mut(&mut self, coord: FloorCoord) -> WorldResult<&mut Tile> {
        let floor = self
            .tiles
            .get_mut(coord.floor)
            .ok_or(WorldError::MissingTile(coord))?;
        Ok(&mut floor[coord.coord])
    }

    pub fn place_tile(&mut self, coord: FloorCoord, tile: Tile) -> WorldResult<()> {
        *self.tile_mut(coord)? = tile;
        Ok(())
    }

    // pub fn tiles(&self) -> &Array3<Tile> {
    //     &self.tiles
    // }

    pub fn tiles_floor(&self, floor: usize) -> WorldResult<&[[Tile; FLOOR_ROWS]; FLOOR_COLS]> {
        self.tiles.get(floor).ok_or(WorldError::MissingFloor(floor))
    }

    pub fn furniture(&self, coord: FloorCoord) -> WorldResult<Option<Furniture>> {
        self.tile(coord).map(|tile| tile.furniture)
    }

    pub fn opacity_map(&self, floor: usize) -> WorldResult<[[bool; FLOOR_ROWS]; FLOOR_COLS]> {
        let floor = self.tiles_floor(floor)?;
        let opacity = floor.map(|column| column.map(|tile| tile.opaque()));
        Ok(opacity)
    }

    pub fn is_opaque(&self, coord: FloorCoord) -> WorldResult<bool> {
        self.tile(coord).map(|tile| tile.opaque())
    }

    pub fn is_steppable(&self, coord: FloorCoord) -> WorldResult<bool> {
        self.tile(coord).map(|tile| tile.steppable())
    }

    pub fn is_impassable(&self, coord: FloorCoord) -> WorldResult<bool> {
        self.tile(coord).map(|tile| tile.impassable())
    }

    pub fn add_furniture(&mut self, coord: FloorCoord, furniture: Furniture) -> WorldResult<()> {
        if let Some(furniture) = self.tile(coord)?.furniture {
            Err(WorldError::Furniture(coord, furniture))
        } else {
            let _ = self.tile_mut(coord)?.furniture.replace(furniture); //.expect_none("no feature here");
            Ok(())
        }
    }

    pub fn remove_furniture(&mut self, coord: FloorCoord) -> WorldResult<Option<Furniture>> {
        Ok(self.tile_mut(coord)?.furniture.take())
    }

    pub fn move_furniture(&mut self, coord: FloorCoord, dir: Dir) -> WorldResult<()> {
        // TODO: make it so that sight_map is not recomputed twice
        let new_coord = (coord + dir)?;
        if let Some(furniture) = self.tile(new_coord)?.furniture {
            Err(WorldError::Furniture(new_coord, furniture))
        } else if let Some(furniture) = self.remove_furniture(coord)? {
            self.add_furniture(new_coord, furniture)
                .expect("add furniture");
            Ok(())
        } else {
            Err(WorldError::NoFurniture(coord))
        }
    }

    // /// Add vegetation using cellular automata.
    // fn add_vegetation(&mut self, rng: &mut GameRng) {
    //     const CELLULAR_ITER: u8 = 12;
    //     const CELLULAR_PROB: f64 = 0.5;
    //     let distribution = Bernoulli::new(CELLULAR_PROB).unwrap();

    //     for row in 0..FLOOR_FLOOR_ROWS {
    //         for col in 0..FLOOR_FLOOR_COLS {
    //             for floor in 0..FLOORS {
    //                 let coord = Coord::from((row, col, floor));
    //                 let tile = self.tile(coord).expect("tile");
    //                 if tile.kind().is_fertile() && tile.feature().is_none() && rng.sample(distribution)
    //                 {
    //                     self.with_feature(coord, Some(Feature::Vegetation { trampled: false }))
    //                         .expect("set feature");
    //                 }
    //             }
    //         }
    //     }

    //     for _ in 0..CELLULAR_ITER {
    //         for row in 0..FLOOR_FLOOR_ROWS {
    //             for col in 0..FLOOR_FLOOR_COLS {
    //                 for floor in 0..FLOORS {
    //                     let coord = Coord::from((row, col, floor));
    //                     self.vegetation_generation(coord, rng);
    //                 }
    //             }
    //         }
    //     }
    // }

    // /// Next cellular automata generation for vegetation.
    // fn vegetation_generation(&mut self, coord: Coord, rng: &mut GameRng) {
    //     let tile = self.tiles[Ix3::from(coord)];
    //     if tile.kind().is_fertile() {
    //         let mut neighbors = 0;
    //         if let None | Some(Feature::Vegetation { .. }) = tile.feature() {
    //             for &dir in &DIRS {
    //                 if let Ok(tile) = self.tile(coord + dir) {
    //                     if let Some(Feature::Flooded) | Some(Feature::Vegetation { .. }) =
    //                         tile.feature()
    //                     {
    //                         neighbors += 1;
    //                     }
    //                 }
    //             }
    //             match neighbors {
    //                 0 | 1 => self.with_feature(coord, None).expect("set feature"),
    //                 3 => self
    //                     .with_feature(coord, Some(Feature::Vegetation { trampled: false }))
    //                     .expect("set feature"),
    //                 4 if rng.gen() => self.with_feature(coord, None).expect("set feature"),
    //                 _ => {}
    //             }
    //         }
    //     }
    // }

    // /// Add water to level using Perlin noise.
    // fn add_water(&mut self, rng: &mut GameRng) {
    //     let mut noise = Fbm::new();
    //     noise = noise.set_seed(rng.gen());
    //     // noise.set_octaves(self, octaves: usize);
    //     noise = noise.set_frequency(0.05);
    //     // noise.set_lacunarity(self, lacunarity: f64);
    //     // noise.set_persistence(self, persistence: f64);
    //     for ((row, col), tile) in self.tiles.indexed_iter_mut() {
    //         let heigth = noise.get([row as f64, col as f64]);
    //         if heigth < -0.15f64 && tile.feature().is_none() {
    //             match tile.kind() {
    //                 TileKind::Wall(_) => *tile = Tile::new_wall(WallKind::Molded),
    //                 TileKind::Floor { .. } => tile.with_feature(Some(Feature::Flooded)),
    //                 _ => continue,
    //             }
    //         }
    //     }
    // }

    pub fn update_fire(&mut self) {
        let old_fire_map: Vec<[[bool; FLOOR_ROWS]; FLOOR_COLS]> = self
            .tiles
            .iter()
            .map(|floor| floor.map(|column| column.map(|tile| tile.fire > 0)))
            .collect();
        for (floor, floor_tiles) in old_fire_map.iter().enumerate() {
            for (row, column_tiles) in floor_tiles.iter().enumerate() {
                for (col, on_fire) in column_tiles.iter().enumerate() {
                    if *on_fire {
                        let coord = Coord::new(row, col).expect("TODO: manage error value");
                        let floor_coord = FloorCoord { coord, floor };
                        for &dir in &DIRS {
                            if let Ok(coord) = floor_coord + dir {
                                if self.is_flammable(coord).unwrap_or(false) {
                                    self.set_on_fire(coord).expect("set tile on fire");
                                }
                            }
                        }
                    }
                }
            }
        }
        self.tiles
            .iter_mut()
            .flatten()
            .flatten()
            .filter(|tile| tile.fire > 0)
            .for_each(Tile::burn);
    }

    pub fn is_flammable(&self, floor_coord: FloorCoord) -> WorldResult<bool> {
        self.tile(floor_coord).map(|tile| tile.flammable())
    }

    pub fn set_on_fire(&mut self, floor_coord: FloorCoord) -> WorldResult<()> {
        self.tile_mut(floor_coord)?.set_on_fire();
        Ok(())
    }
}

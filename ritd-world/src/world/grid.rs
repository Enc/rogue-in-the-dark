//! Types and methods to handle the world's geometry.

use crate::{WorldError, WorldResult, FLOOR_COLS, FLOOR_ROWS};
use serde::{Deserialize, Serialize};
use std::{
    collections::VecDeque,
    ops::{Add, Index, IndexMut, Sub},
};

pub type CoordIndex = usize;

/// The four cardinal directions.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Dir {
    North,
    South,
    East,
    West,
}

impl Dir {
    const fn delta(self) -> (isize, isize) {
        match self {
            Dir::North => (-1, 0),
            Dir::South => (1, 0),
            Dir::West => (0, -1),
            Dir::East => (0, 1),
        }
    }

    pub const fn opposite(self) -> Dir {
        match self {
            Dir::North => Dir::South,
            Dir::South => Dir::North,
            Dir::West => Dir::East,
            Dir::East => Dir::West,
        }
    }
}

/// A convenience array listing the four cardinal directions.
pub const DIRS: [Dir; 4] = [Dir::North, Dir::East, Dir::South, Dir::West];

/// A Cartesian coordinate.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Coord {
    row: CoordIndex,
    col: CoordIndex,
}

impl<T> Index<Coord> for [[T; FLOOR_ROWS]; FLOOR_COLS] {
    type Output = T;

    fn index(&self, coord: Coord) -> &Self::Output {
        &self[coord.col()][coord.row()]
    }
}

impl<T> IndexMut<Coord> for [[T; FLOOR_ROWS]; FLOOR_COLS] {
    fn index_mut(&mut self, coord: Coord) -> &mut Self::Output {
        &mut self[coord.col()][coord.row()]
    }
}

impl Add<Dir> for Coord {
    type Output = WorldResult<Coord>;

    fn add(self, other: Dir) -> WorldResult<Coord> {
        let (d_row, d_col) = other.delta();
        let row = self
            .row
            .checked_add_signed(d_row)
            .ok_or(WorldError::CoordStepOutOfBounds(self, other))?;
        let col = self
            .col
            .checked_add_signed(d_col)
            .ok_or(WorldError::CoordStepOutOfBounds(self, other))?;
        Coord::new(row, col)
    }
}

impl Sub<Dir> for Coord {
    type Output = WorldResult<Coord>;

    fn sub(self, other: Dir) -> WorldResult<Coord> {
        self + other.opposite()
    }
}

impl Coord {
    pub const fn new(row: CoordIndex, col: CoordIndex) -> WorldResult<Self> {
        let coord = Coord { row, col };
        if row < FLOOR_ROWS && col < FLOOR_COLS {
            Ok(coord)
        } else {
            Err(WorldError::CoordOutOfBounds(coord))
        }
    }

    pub const fn row(&self) -> CoordIndex {
        self.row
    }

    pub const fn col(&self) -> CoordIndex {
        self.col
    }
}

/// Manhattan distance between coords.
pub const fn distance(from: Coord, to: Coord) -> CoordIndex {
    let delta_row = (to.row() as isize - from.row() as isize).unsigned_abs();
    let delta_col = (to.col() as isize - from.col() as isize).unsigned_abs();
    delta_row + delta_col
}

pub fn ring(center: Coord, radius: CoordIndex) -> Vec<Coord> {
    // Degenerate case
    if radius == 0 {
        return vec![center];
    }

    let mut coords = Vec::new();

    // col : conter.col - radius .. center.col
    // row = center.row + center.col - radius - col
    // 0 <= row
    // // 0 <= center.row + conter.col - radius - col
    // // col <= center.row + center.col - radius
    // // col < center.row + center.col - radius + 1
    // 0 <= col
    let start = center.col().saturating_sub(radius);
    let end = center
        .col()
        .min((1 + center.row() + center.col()).saturating_sub(radius));
    coords.extend((start..end).map(|col| {
        Coord::new(center.row() + center.col() - radius - col, col)
            .expect("bounds already enforced")
    }));

    // row : center.row - radius .. center.row
    // col = center.col + radius + row - center.row (increasing)
    // 0 <= row
    // col < COLS
    // // center.col + radius + row - center.row <= COLS
    // // row < COLS + center.row - radius - center.col
    let start = center.row().saturating_sub(radius);
    let end = center
        .row()
        .min((FLOOR_COLS + center.row()).saturating_sub(center.col() + radius));
    coords.extend((start..end).map(|row| {
        Coord::new(row, center.col() + radius + row - center.row())
            .expect("bounds already enforced")
    }));

    // col : (center.col, center.col + radius] (reversed)
    // row = center.row + center.col + radius - col (decreasing; increasing when col reversed)
    // col < COLS
    // row < ROWS
    // // center.row + center.col + radius - col < ROWS
    // // col > center.row + center.col + radius - ROWS
    let start = center
        .col()
        .max((center.row() + center.col() + radius).saturating_sub(FLOOR_ROWS));
    let end = FLOOR_COLS.min(center.col() + radius + 1);
    coords.extend(((start + 1)..end).rev().map(|col| {
        Coord::new(center.row() + center.col() + radius - col, col)
            .expect("bounds already enforced")
    }));

    // row : (center.row, center.row + radius] (reversed)
    // col = center.col + row - radius - center.row
    // col >= 0
    // // center.col + row - radius - center.row >= 0
    // // row >= center.row + radius - center.col
    // row < ROWS
    let start = (center.row() + 1).max((center.row() + radius).saturating_sub(center.col()));
    let end = FLOOR_ROWS.min(center.row() + radius + 1);
    coords.extend((start..end).rev().map(|row| {
        Coord::new(row, center.col() + row - radius - center.row())
            .expect("bounds already enforced")
    }));

    coords
}

pub fn directions_to(from: Coord, to: Coord) -> Vec<Dir> {
    use std::cmp::Ordering;
    let mut dirs = Vec::new();

    match from.row().cmp(&to.row()) {
        Ordering::Greater => dirs.push(Dir::North),
        Ordering::Less => dirs.push(Dir::South),
        Ordering::Equal => {}
    }

    match from.col().cmp(&to.col()) {
        Ordering::Greater => dirs.push(Dir::West),
        Ordering::Less => dirs.push(Dir::East),
        Ordering::Equal => {}
    }

    dirs
}

pub fn l1(
    origin: Coord,
    opacity_map: &[[bool; FLOOR_ROWS]; FLOOR_COLS],
    max_radius: usize,
) -> WorldResult<[[bool; FLOOR_ROWS]; FLOOR_COLS]> {
    let mut paths: [[(u16, u16); FLOOR_ROWS]; FLOOR_COLS] = [[(0, 0); FLOOR_ROWS]; FLOOR_COLS];
    paths[origin] = (1, 0);

    for radius in 1..=max_radius {
        let ring = ring(origin, radius);
        for coord in ring {
            for dir in directions_to(origin, coord) {
                let coord_dir = (coord - dir).expect("moves towards origin");
                let (free_paths, blocked_paths) = paths[coord_dir];
                assert_ne!(
                    (free_paths, blocked_paths),
                    (0, 0),
                    "on coord {coord_dir:?} with center {origin:?} and radius {radius}"
                );
                let (next_free_paths, next_blocked_paths) = &mut paths[coord];
                if opacity_map[coord_dir] {
                    *next_blocked_paths += free_paths + blocked_paths;
                } else {
                    *next_free_paths += free_paths;
                    *next_blocked_paths += blocked_paths;
                }
            }
        }
    }

    let mut seen_tiles = [[false; FLOOR_ROWS]; FLOOR_COLS];
    for row in 0..FLOOR_ROWS {
        for col in 0..FLOOR_COLS {
            let coord = Coord::new(row, col).expect("coords within bounds");
            let (free_paths, blocked_paths) = paths[coord];
            // if 3 * free_paths > 2 * blocked_paths && !opacity_map[coord] {
            if 3 * free_paths > 2 * blocked_paths {
                seen_tiles[coord] = true;
            }
        }
    }

    Ok(seen_tiles)
}

pub fn l1_fov(
    origin: Coord,
    opacity_map: &[[bool; FLOOR_ROWS]; FLOOR_COLS],
    max_radius: usize,
) -> WorldResult<[[bool; FLOOR_ROWS]; FLOOR_COLS]> {
    let seen_tiles = l1(origin, opacity_map, max_radius)?;

    let expanded_seen_tiles = l1_expand(opacity_map, &seen_tiles);
    Ok(expanded_seen_tiles)
    // Ok(seen_tiles)
}

fn l1_expand(
    opacity_map: &[[bool; FLOOR_ROWS]; FLOOR_COLS],
    seen_tiles: &[[bool; FLOOR_ROWS]; FLOOR_COLS],
) -> [[bool; FLOOR_ROWS]; FLOOR_COLS] {
    let mut expanded_seen_tiles = *seen_tiles;
    for row in 0..FLOOR_ROWS {
        for col in 0..FLOOR_COLS {
            let coord = Coord::new(row, col).expect("must be within bounds");
            if opacity_map[coord] {
                for coord_dir in DIRS.iter().filter_map(|dir| (coord + *dir).ok()) {
                    expanded_seen_tiles[coord] = expanded_seen_tiles[coord]
                        || (seen_tiles[coord_dir] && !opacity_map[coord_dir]);
                }
                // TODO: fix ugly hack
                for delta_row in [-1, 1] {
                    for delta_col in [-1, 1] {
                        if let Ok(coord_dir) = Coord::new(
                            (row as isize + delta_row) as usize,
                            (col as isize + delta_col) as usize,
                        ) {
                            expanded_seen_tiles[coord] = expanded_seen_tiles[coord]
                                || (seen_tiles[coord_dir] && !opacity_map[coord_dir]);
                        }
                    }
                }
            }
        }
    }
    expanded_seen_tiles
}

pub fn dijkstra_map(
    origin: Coord,
    weigth_map: [[Option<u8>; FLOOR_ROWS]; FLOOR_COLS],
    max_dist: Option<u8>,
) -> [[Option<u8>; FLOOR_ROWS]; FLOOR_COLS] {
    let mut dijkstra_map = [[None; FLOOR_ROWS]; FLOOR_COLS];
    dijkstra_map[origin] = Some(0);
    let mut walkers: VecDeque<(Coord, u8)> = vec![(origin, 0)].into();

    while let Some((walker_coord, walker_dist)) = walkers.pop_front() {
        if max_dist.map(|max| walker_dist < max).unwrap_or(true) {
            for dir in &DIRS {
                if let Ok(step_coord) = walker_coord + *dir {
                    if let Some(weigth) = weigth_map[step_coord] {
                        let dijkstra_value = &mut dijkstra_map[step_coord];
                        if dijkstra_value
                            .map(|dist| walker_dist + weigth < dist)
                            .unwrap_or(true)
                        {
                            *dijkstra_value = Some(walker_dist + weigth);
                            walkers.push_back((step_coord, walker_dist + weigth));
                        }
                    }
                }
            }
        }
    }

    dijkstra_map
}

/// A Cartesian coordinate.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub struct FloorCoord {
    pub coord: Coord,
    pub floor: CoordIndex,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Serialize, Deserialize)]
pub enum ItemCoord {
    Lay(FloorCoord),
    Fly(FloorCoord),
}

impl Add<Dir> for FloorCoord {
    type Output = WorldResult<FloorCoord>;

    fn add(self, other: Dir) -> Self::Output {
        Ok(FloorCoord {
            coord: (self.coord + other)?,
            floor: self.floor,
        })
    }
}

impl Sub<Dir> for FloorCoord {
    type Output = WorldResult<FloorCoord>;

    fn sub(self, other: Dir) -> Self::Output {
        Ok(FloorCoord {
            coord: (self.coord - other)?,
            floor: self.floor,
        })
    }
}

impl FloorCoord {
    /// Manhattan distance between coords.
    pub fn distance(from: Self, to: Self) -> Option<CoordIndex> {
        if from.floor == to.floor {
            Some(distance(from.coord, to.coord))
        } else {
            None
        }
    }

    pub fn directions_to(&self, other: Self) -> Vec<Dir> {
        if self.floor == other.floor {
            directions_to(self.coord, other.coord)
        } else {
            Vec::new()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Coord, Dir, FLOOR_COLS, FLOOR_ROWS};

    #[test]
    fn ring_center() {
        let center = Coord::new(FLOOR_ROWS / 2, FLOOR_COLS / 2).expect("coords within bounds");
        let ring = super::ring(center, 1);
        assert_eq!(
            ring,
            vec![
                (center + Dir::West).expect("coord within bounds"),
                (center + Dir::North).expect("coord within bounds"),
                (center + Dir::East).expect("coord within bounds"),
                (center + Dir::South).expect("coord within bounds")
            ]
        );
    }

    #[test]
    fn ring_nw_corner() {
        let center = Coord::new(0, 0).expect("coords within bounds");
        let ring = super::ring(center, 1);
        assert_eq!(
            ring,
            vec![
                (center + Dir::East).expect("coord within bounds"),
                (center + Dir::South).expect("coord within bounds")
            ]
        );
    }

    #[test]
    fn ring_ne_corner() {
        let center = Coord::new(0, FLOOR_COLS - 1).expect("coords within bounds");
        let ring = super::ring(center, 1);
        assert_eq!(
            ring,
            vec![
                (center + Dir::West).expect("coord within bounds"),
                (center + Dir::South).expect("coord within bounds")
            ]
        );
    }

    #[test]
    fn ring_se_corner() {
        let center = Coord::new(FLOOR_ROWS - 1, FLOOR_COLS - 1).expect("coords within bounds");
        let ring = super::ring(center, 1);
        assert_eq!(
            ring,
            vec![
                (center + Dir::West).expect("coord within bounds"),
                (center + Dir::North).expect("coord within bounds")
            ]
        );
    }

    #[test]
    fn ring_sw_corner() {
        let center = Coord::new(FLOOR_ROWS - 1, 0).expect("coords within bounds");
        let ring = super::ring(center, 1);
        assert_eq!(
            ring,
            vec![
                (center + Dir::North).expect("coord within bounds"),
                (center + Dir::East).expect("coord within bounds")
            ]
        );
    }
}

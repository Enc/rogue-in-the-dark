//! Contains the `TileMap` object.

use super::{Dir, Coord, Furniture};
use crate::tile::Tile;
use crate::{FloorKind, Map, FloorPlan};
use serde::{Deserialize, Serialize};
use serde_with::serde_as;

#[derive(Clone, Copy, Debug)]
pub enum FloorError {
    MissingTile(Coord),
    Furniture(Coord),
    NoFurniture(Coord),
}

pub type FloorResult<T> = Result<T, FloorError>;

/// A grid of `Tile`s
/// TODO: consider memoizing maps
#[serde_as]
#[derive(Serialize, Deserialize)]
pub struct Floor<const ROWS: usize, const COLS: usize> {
    kind: FloorKind,
    plan: FloorPlan,
    #[serde_as(as = "[[_; COLS]; ROWS]")]
    tiles: [[Tile; COLS]; ROWS],
    furniture_map: Map<Coord, Furniture>,
    // #[serde_as(as = "[[_; COLS]; ROWS]")]
    // impassable_map: [[bool; COLS]; ROWS],
    // #[serde_as(as = "[[_; COLS]; ROWS]")]
    // steppable_map: [[bool; COLS]; ROWS],
    // #[serde_as(as = "[[_; COLS]; ROWS]")]
    // fire_map: [[u8; COLS]; ROWS],
}

impl<const ROWS: usize, const COLS: usize>  Floor<ROWS, COLS> {
    const FILLING_TILE: Tile = Tile::Floor(FloorKind::Plank);

    fn new(kind: FloorKind, plan: FloorPlan) -> Floor<ROWS, COLS> {
        Floor {
            kind,
            plan,
            tiles: [[Self::FILLING_TILE; COLS]; ROWS],
            furniture_map: Map::default(),
            // fire_map: [[0; COLS]; ROWS],
        }
    }

    pub fn kind(&self) -> FloorKind {
        self.kind
    }

    pub fn plan(&self) -> &FloorPlan {
        &self.plan
    }

    pub fn tile(&self, coord: Coord) -> FloorResult<&Tile> {
        self.tiles
            .get(coord.col as usize)
            .ok_or(FloorError::MissingTile(coord))?
            .get(coord.row as usize)
            .ok_or(FloorError::MissingTile(coord))
    }

    pub fn place_tile(&mut self, coord: Coord, tile: Tile) -> FloorResult<()> {
        *self
            .tiles
            .get_mut(coord.col as usize)
            .ok_or(FloorError::MissingTile(coord))?
            .get_mut(coord.row as usize)
            .ok_or(FloorError::MissingTile(coord))? = tile;
        Ok(())
    }

    pub fn tiles(&self) -> &[[Tile; COLS]; ROWS] {
        &self.tiles
    }

    pub fn is_opaque(&self, coord: Coord) -> FloorResult<bool> {
        let tile = self.tile(coord)?.opaque();
        let furniture = self
            .furniture(coord)
            .map(|furniture| furniture.opaque())
            .unwrap_or(false);
        Ok(tile || furniture)
    }

    pub fn is_steppable(&self, coord: Coord) -> FloorResult<bool> {
        let tile = self.tile(coord)?.steppable();
        let furniture = self
            .furniture(coord)
            .map(|furniture| furniture.steppable())
            .unwrap_or(true);
        Ok(tile && furniture)
    }

    pub fn is_impassable(&self, coord: Coord) -> FloorResult<bool> {
        let tile = self.tile(coord)?.impassable();
        let furniture = self
            .furniture(coord)
            .map(|furniture| furniture.impassable())
            .unwrap_or(true);
        Ok(tile || furniture)
    }

    pub fn add_furniture(&mut self, coord: Coord, furniture: Furniture) -> FloorResult<()> {
        if let Some(furniture) = self.furniture_map.get(&coord) {
            Err(FloorError::Furniture(coord))
        } else {
            self.furniture_map.insert(coord, furniture); //.expect_none("no feature here");
            Ok(())
        }
    }

    pub fn remove_furniture(&mut self, coord: Coord) -> FloorResult<Option<Furniture>> {
        let opt_furniture = self.furniture_map.remove(&coord);
        Ok(opt_furniture)
    }

    pub fn move_furniture(&mut self, coord: Coord, dir: Dir) -> FloorResult<()> {
        // TODO: make it so that sight_map is not recomputed twice
        let new_coord = coord + dir;
        if let Some(furniture) = self.furniture_map.get(&new_coord) {
            Err(FloorError::Furniture(new_coord))
        } else if let Some(furniture) = self.remove_furniture(coord)? {
            self.add_furniture(new_coord, furniture)
                .expect("add furniture");
            Ok(())
        } else {
            Err(FloorError::NoFurniture(coord))
        }
    }

    pub fn furniture(&self, coord: Coord) -> Option<&Furniture> {
        self.furniture_map.get(&coord)
    }

    // pub fn update_fire(&mut self) {
    //     let old_fire_map = self.fire_map.clone();
    //     for (idx, _) in old_fire_map.indexed_iter().filter(|(_, &val)| val > 0) {
    //         let floor_coord = Coord::from(idx);
    //         for &dir in &DIRS {
    //             if self.is_flammable(floor_coord + dir).unwrap_or(false) {
    //                 self.set_on_fire(floor_coord + dir);
    //             }
    //         }
    //     }
    //     self.fire_map
    //         .iter_mut()
    //         .for_each(|val| *val = val.saturating_sub(1));
    // }

    // pub fn is_flammable(&self, floor_coord: Coord) -> FloorResult<bool> {
    //     let tile_flammable = self.tile(floor_coord)?.flammable();
    //     let furniture_flammable = self.furniture_map.contains_key(&floor_coord);
    //     Ok(tile_flammable && furniture_flammable)
    // }

    // pub fn set_on_fire(&mut self, floor_coord: Coord) {
    //     const FIRE_VAL: u8 = 4;
    //     if let Some(val) = self.fire_map.get_mut(Ix3::from(floor_coord)) {
    //         *val = FIRE_VAL;
    //     }
    // }
}

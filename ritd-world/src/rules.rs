use crate::world::l1_fov;
use crate::{
    dijkstra_map, Actor, Coord, CoordIndex, Damage, DamageKind, Element, FloorCoord, ItemCoord,
    PhysicalDamage, Stat, FLOORS, FLOOR_COLS, FLOOR_ROWS,
};
use crate::{item::Item, ActorKey};
use crate::{World, WorldError, WorldResult};
use rand::prelude::*;

pub fn can_place(world: &World, actor: &Actor, floor_coord: FloorCoord) -> WorldResult<()> {
    if world.tile_map().is_impassable(floor_coord)? {
        Err(WorldError::Impassable(floor_coord))
    } else if !world.is_steppable(floor_coord)? && !actor.kind().flying() {
        Err(WorldError::NotSteppable(floor_coord))
    } else if let Some(_other_key) = world.actor_at(floor_coord) {
        Err(WorldError::NotEmpty(floor_coord))
    } else {
        Ok(())
    }
}

pub fn hit_roll<R: Rng>(world: &World, actor_key: ActorKey, rng: &mut R) -> WorldResult<u8> {
    let actor = world.actor(actor_key)?;
    Ok(rng.gen_range(1..=actor.dexterity))
}

pub fn dodge_roll<R: Rng>(world: &World, actor_key: ActorKey, rng: &mut R) -> WorldResult<u8> {
    let actor = world.actor(actor_key)?;
    Ok(rng.gen_range(1..=actor.dexterity))
}

pub fn aim_roll<R: Rng>(world: &World, actor_key: ActorKey, rng: &mut R) -> WorldResult<u8> {
    let actor = world.actor(actor_key)?;
    Ok(rng.gen_range(1..=actor.perception))
}

pub fn damage<R: Rng>(world: &World, actor_key: ActorKey, rng: &mut R) -> WorldResult<Damage> {
    let kind;
    let mult;
    let actor = world.actor(actor_key)?;

    if let Some(item_key) = world.equipped_weapon(actor_key)? {
        if let Item::Weapon(weapon) = world.item(item_key).expect("equipped weapon") {
            kind = weapon.damage();
            mult = weapon.damage_mult();
        } else {
            kind = DamageKind::Physical(PhysicalDamage::Bludgeoning);
            mult = 1;
        }
    } else {
        kind = DamageKind::Physical(PhysicalDamage::Bludgeoning);
        mult = 1;
    }
    let amount = rng.gen_range(1..=actor.strength.min(mult as u8));
    Ok(Damage { amount, kind })
}

pub fn defense<R: Rng>(
    world: &World,
    actor_key: ActorKey,
    damage_kind: DamageKind,
    rng: &mut R,
) -> WorldResult<u8> {
    match damage_kind {
        DamageKind::Physical(PhysicalDamage::Piercing) | DamageKind::Elemental(_) => Ok(0),
        DamageKind::Physical(PhysicalDamage::Bludgeoning)
        | DamageKind::Physical(PhysicalDamage::Slashing) => defense_roll(world, actor_key, rng),
    }
}

pub fn defense_roll<R: Rng>(world: &World, actor_key: ActorKey, rng: &mut R) -> WorldResult<u8> {
    if let Some(armor_key) = world.equipped_armor(actor_key)? {
        let armor = world.item(armor_key).expect("armor");
        if let Item::Wearable(wearable) = armor {
            Ok(rng.gen_range(1..=wearable.defense()))
        } else {
            panic!("non-wearable item worn");
        }
    } else {
        Ok(0)
    }
}

pub fn fire(world: &mut World, actor_key: ActorKey) -> WorldResult<()> {
    if let Some(item_key) = world.equipped_weapon(actor_key)? {
        if let Item::Firearm { ammo, .. } = world.item_mut(item_key).expect("equipped weapon") {
            if *ammo > 0 {
                *ammo -= 1;
                Ok(())
            } else {
                Err(WorldError::OutOfAmmo(item_key))
            }
        } else {
            Err(WorldError::NotAFirearm(item_key))
        }
    } else {
        Err(WorldError::NoFirearmEquipped(actor_key))
    }
}

// TODO: add errors
// TODO: fix damage amount calculation
pub fn shot_damage_roll<R: Rng>(
    world: &World,
    actor_key: ActorKey,
    rng: &mut R,
) -> WorldResult<Damage> {
    if let Some(item_key) = world.equipped_weapon(actor_key)? {
        if let Item::Firearm { firearm, .. } = world.item(item_key).expect("equipped weapon") {
            let kind = DamageKind::Physical(PhysicalDamage::Piercing);
            let mult = firearm.damage_mult();
            let amount = rng.gen_range(1..=mult as u8);
            Ok(Damage { amount, kind })
        } else {
            Err(WorldError::NotAFirearm(item_key))
        }
    } else {
        Err(WorldError::NoFirearmEquipped(actor_key))
    }
}

pub fn take_damage(world: &mut World, actor_key: ActorKey, damage: Damage) -> WorldResult<()> {
    let actor = world.actor_mut(actor_key)?;
    match damage.kind {
        DamageKind::Elemental(Element::Fire) => actor.burning = actor.burning.max(damage.amount),
        DamageKind::Physical(PhysicalDamage::Slashing) => actor.bleeding += damage.amount,
        _ => actor.health_points = actor.health_points.saturating_sub(damage.amount),
    }
    Ok(())
}

pub fn heal(world: &mut World, actor_key: ActorKey) -> WorldResult<()> {
    let max_health = max_health(world, actor_key)?;
    let actor = world.actor_mut(actor_key)?;
    actor.health_points = max_health;
    Ok(())
}

pub fn reduce_sanity(world: &mut World, actor_key: ActorKey) -> WorldResult<()> {
    let actor = world.actor_mut(actor_key)?;
    actor.sanity = actor.sanity.saturating_sub(1);
    Ok(())
}

pub fn burn(world: &mut World, actor_key: ActorKey) -> WorldResult<()> {
    let actor = world.actor_mut(actor_key)?;
    if actor.burning > 0 {
        actor.burning = actor.burning.saturating_sub(1);
        actor.health_points = actor.health_points.saturating_sub(1);
    }
    Ok(())
}

pub fn bleed(world: &mut World, actor_key: ActorKey) -> WorldResult<()> {
    let actor = world.actor_mut(actor_key)?;
    if actor.bleeding > 0 {
        actor.bleeding = actor.bleeding.saturating_sub(1);
        actor.health_points = actor.health_points.saturating_sub(1);
    }
    Ok(())
}

pub fn is_dead(world: &World, actor_key: ActorKey) -> WorldResult<bool> {
    let actor = world.actor(actor_key)?;
    Ok(actor.health_points == 0)
}

pub fn is_insane(world: &World, actor_key: ActorKey) -> WorldResult<bool> {
    let actor = world.actor(actor_key)?;
    Ok(actor.sanity == 0)
}

pub fn increase_stat(world: &mut World, actor_key: ActorKey, stat: Stat) -> WorldResult<()> {
    let actor = world.actor_mut(actor_key)?;
    match stat {
        Stat::Con => actor.constitution += 1,
        Stat::Dex => actor.dexterity += 1,
        Stat::Int => actor.intelligence += 1,
        Stat::Per => actor.perception += 1,
        Stat::Str => actor.strength += 1,
    }
    Ok(())
}

pub fn max_health(world: &World, actor_key: ActorKey) -> WorldResult<u8> {
    let actor = world.actor(actor_key)?;
    Ok(actor.base_max_health())
}

pub fn random_actor_coord<R: Rng>(
    world: &World,
    actor: &Actor,
    rng: &mut R,
) -> WorldResult<FloorCoord> {
    let floor = rng.gen_range(0..FLOORS);
    let coord = world
        .plan()
        .get(floor)
        .expect("a random floor")
        .rooms
        .choose(rng)
        .expect("a random room")
        .plan
        .inner_coords()
        .iter()
        .map(|&coord| FloorCoord {
            coord,
            floor: floor as CoordIndex,
        })
        .filter(|&coord| can_place(world, actor, coord).is_ok())
        .choose(rng)
        .expect("a random coord");
    Ok(coord)
}

// TODO: replace expect with error
pub fn roll_item(world: &World, floor_coord: FloorCoord) -> WorldResult<FloorCoord> {
    let floor_map = world.tile_map().tiles_floor(floor_coord.floor)?;
    let mut weight_map = [[None; FLOOR_ROWS]; FLOOR_COLS];
    for (col, row) in floor_map.iter().enumerate() {
        for (row, tile) in row.iter().enumerate() {
            if !tile.impassable() {
                weight_map[col][row] = Some(1);
            }
        }
    }
    let dijkstra_map = dijkstra_map(floor_coord.coord, weight_map, None);
    let roll_coord = dijkstra_map
        .iter()
        .enumerate()
        .flat_map(|(col, row)| {
            row.iter().enumerate().map(move |(row, dist)| {
                (
                    FloorCoord {
                        floor: floor_coord.floor,
                        coord: Coord::new(row, col).expect("coords within bounds"),
                    },
                    dist,
                )
            })
        })
        .filter(|&(floor_coord, dist)| {
            world.can_place_item(ItemCoord::Lay(floor_coord)).is_ok() && dist.is_some()
        })
        .min_by_key(|(_, dist)| dist.expect("distance"))
        .map(|(floor_coord, _)| floor_coord)
        .expect("coord where item falls");
    Ok(roll_coord)
}

pub fn sight_range(world: &World, actor_key: ActorKey) -> WorldResult<u8> {
    let actor = world.actor(actor_key)?;
    Ok(actor.perception.max(1))
}

pub fn sight_map(
    world: &World,
    actor_key: ActorKey,
) -> WorldResult<[[bool; FLOOR_ROWS]; FLOOR_COLS]> {
    let sight_range = sight_range(world, actor_key)? as usize;
    let actor_coord = world.actor_coord(actor_key)?;
    let opacity_map = &world.tile_map().opacity_map(actor_coord.floor)?;
    l1_fov(actor_coord.coord, opacity_map, sight_range)
}

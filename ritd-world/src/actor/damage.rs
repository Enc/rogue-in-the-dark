use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Damage {
    pub amount: u8,
    pub kind: DamageKind,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum DamageKind {
    Physical(PhysicalDamage),
    Elemental(Element),
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum PhysicalDamage {
    Slashing,
    Piercing,
    Bludgeoning,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Element {
    Fire,
    Electricity,
    Cold,
}

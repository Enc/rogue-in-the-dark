use crate::item::{Item, Potion, Wand, Weapon, Wearable};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum ActorKind {
    Player,
    Bat,
    Cultist,
    Rat,
    Zombie,
}

impl ActorKind {
    const PLAYER_STARTING_STAT: u8 = 6;

    pub const fn strength(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Zombie => 8,
            ActorKind::Cultist => 5,
            ActorKind::Bat | ActorKind::Rat => 4,
        }
    }

    pub const fn constitution(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Zombie => 8,
            ActorKind::Cultist => 5,
            ActorKind::Bat | ActorKind::Rat => 3,
        }
    }

    pub const fn dexterity(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Zombie => 4,
            ActorKind::Cultist => 5,
            ActorKind::Bat | ActorKind::Rat => 5,
        }
    }

    pub const fn perception(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Rat => 6,
            ActorKind::Zombie => 4,
            ActorKind::Bat | ActorKind::Cultist => 5,
        }
    }

    pub const fn intelligence(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Rat | ActorKind::Zombie => 2,
            ActorKind::Bat => 4,
            ActorKind::Cultist => 6,
        }
    }

    pub const fn flying(&self) -> bool {
        matches!(self, ActorKind::Bat)
    }

    pub const fn equipment(self) -> &'static [(Item, f64)] {
        // const FIREARM: Firearm = Firearm::Shotgun;
        // const AMMO: u8 = FIREARM.ammo_capacity();
        // const FIREARM_ITEM: Item = Item::Firearm {
        //     firearm: FIREARM,
        //     ammo: AMMO,
        // };
        match self {
            ActorKind::Player => &[
                // (Item::Potion(Potion::Health), 1.0),
                // (Item::Scroll(Scroll::Teleportation), 1.0),
                // (Item::Wand(Wand::FireBolt), 1.0),
                // (Item::Wand(Wand::Lightning), 1.0),
                // (FIREARM_ITEM, 1.0),
            ],
            ActorKind::Cultist => &[
                (Item::Potion(Potion::Health), 0.3),
                (Item::Wand(Wand::Lightning), 0.3),
            ],
            ActorKind::Zombie => &[(Item::Wand(Wand::FireBolt), 0.5)],
            _ => &[],
        }
    }

    pub const fn armor(self) -> Option<Wearable> {
        match self {
            ActorKind::Cultist => Some(Wearable::Robe),
            _ => None,
        }
    }

    pub const fn weapon(self) -> Option<Weapon> {
        match self {
            // ActorKind::Player => Some(Weapon::Axe),
            ActorKind::Cultist => Some(Weapon::Dagger),
            _ => None,
        }
    }
}

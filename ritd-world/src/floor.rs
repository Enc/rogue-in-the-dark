mod room;

pub use room::{Doors, Room, RoomPlan, RoomType};

use crate::world::Coord;
use serde::{Deserialize, Serialize};

// pub const CRYPT: usize = 0;
pub const CELLAR: usize = 0;
pub const DAY_FLOOR: usize = 1;
pub const NIGHT_FLOOR: usize = 2;
// pub const ATTIC: usize = 4;

#[derive(Clone, Copy, Debug)]
pub enum Floor {
    // Attic,
    // NightFloor,
    DayFloor,
    // Cellar,
    // Crypt,
}

impl From<Floor> for usize {
    fn from(floor: Floor) -> usize {
        match floor {
            // Floor::Crypt => CRYPT,
            // Floor::Cellar => CELLAR,
            Floor::DayFloor => DAY_FLOOR,
            // Floor::NightFloor => NIGHT_FLOOR,
            // Floor::Attic => ATTIC,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FloorPlan {
    pub rooms: Vec<Room>,
    pub landing: Option<Coord>,
    pub stairs: Option<Coord>,
}

impl FloorPlan {
    pub fn find_rooms(&self, coord: Coord) -> impl Iterator<Item = &Room> {
        self.rooms
            .iter()
            .filter(move |room| room.plan.contains(coord))
    }
}

use crate::actor::ActorKind;
use crate::floor::Floor;
use crate::furniture::Furniture;
use crate::item::{Item, Potion, Wand, Weapon, Wearable};
use crate::tile::{FloorKind, WallKind};

use serde::{Deserialize, Serialize};

// pub enum RoomType {
//     // ATTIC
//     ServantBedroom, // Small bedroom with twin bed
//     Storage, // Crates
//     // NIGHT FLOOR
//     MasterBedroom, // Large bedroom with full size bed, ensuite bathroom
//     Bedroom, // Bedroom with twin bed
//     Bathroom,
//     ChildrenBedroom, // Small bedroom with small bed and toys
//     Library, // Bookshelves
//     Study, // Table
//     // DAY FLOOR
//     Kitchen,
//     Pantry,
//     DiningRoom,
//     // CELLAR
//     // CRYPT
//     Cell,
//     // IRON ROOMS
//     // BRASS ROOMS
//     AlchemicLab,
//     SecretLibrary,
//     // SILVER ROOMS
//     // MIX
//     Landing,
//     Stairs,
// }

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum RoomType {
    AlchemicLab,
    Bathroom,
    Bedroom,
    Cell,
    Corridor,
    Dining,
    Drawing,
    Hall,
    Kitchen,
    Landing,
    Library,
    Pantry,
    Stairs,
    Storage,
    Study,
    Temple,
}

impl RoomType {
    pub const fn floor(self) -> FloorKind {
        match self {
            RoomType::Corridor
            | RoomType::Dining
            | RoomType::Hall
            | RoomType::Pantry
            | RoomType::Stairs
            | RoomType::Storage
            | RoomType::Landing => FloorKind::Plank,
            RoomType::Bedroom | RoomType::Library | RoomType::Study | RoomType::Drawing => {
                FloorKind::Carpet
            }
            RoomType::Bathroom | RoomType::Kitchen => FloorKind::Tile,
            RoomType::Cell | RoomType::AlchemicLab | RoomType::Temple => FloorKind::Stone,
        }
    }

    pub const fn wall(self) -> WallKind {
        match self {
            _ => WallKind::Brick,
            // RoomType::Plain
            // | RoomType::Vaults
            // | RoomType::Chasm
            // | RoomType::Stairs
            // | RoomType::Landing
            // | RoomType::Temple => WallKind::Stone,
            // RoomType::Barracks => WallKind::Brick,
        }
    }

    // pub const fn furniture(self) -> &'static [Furniture] {
    //     match self {
    //         RoomType::Drawing => &[Furniture::Armchair],
    //         RoomType::Kitchen => &[Furniture::Table, Furniture::Stove { searched: false }],
    //         RoomType::Library => &[Furniture::Library { searched: false }],
    //         RoomType::Storage => &[Furniture::Crate { open: false }],
    //         RoomType::Bedroom => &[Furniture::Bed],
    //         RoomType::Study => &[Furniture::Chair],
    //         RoomType::Temple => &[Furniture::Altar { used: false }],
    //         _ => &[],
    //     }
    // }

    pub const fn mob_kind(self) -> &'static [ActorKind] {
        match self {
            // RoomType::Storage => &[ActorKind::Rat],
            // RoomType::Bedroom => &[ActorKind::Zombie],
            RoomType::Library => &[ActorKind::Rat],
            RoomType::Temple => &[ActorKind::Cultist],
            RoomType::AlchemicLab => &[ActorKind::Zombie],
            // RoomType::Cell => &[ActorKind::Bat],
            _ => &[],
        }
    }

    pub const fn item_kind(self) -> &'static [Item] {
        match self {
            // RoomType::Storage => &[Item::Weapon(Weapon::Hammer)],
            RoomType::Study => &[
                // Item::Necronomicon,
                Item::Wand(Wand::FireBolt),
                Item::Wand(Wand::Lightning),
            ],
            RoomType::Kitchen => &[Item::Weapon(Weapon::Knife)],
            RoomType::Bathroom => &[Item::Potion(Potion::Health)],
            RoomType::Bedroom => &[Item::Wearable(Wearable::BulletproofVest)],
            // RoomType::Library => &[Item::Scroll(Scroll::Teleportation)],
            RoomType::AlchemicLab => &[Item::Potion(Potion::Health)],
            // RoomType::Cell => &[Item::Necronomicon],
            // RoomType::Plain => &[ItemKind::Wand(Wand::Lightning)],
            // RoomType::Vaults => &[ItemKind::Potion(Potion::Health)],
            _ => &[],
        }
    }

    pub const fn public_rooms(floor: Floor) -> &'static [RoomType] {
        match floor {
            // Floor::Crypt => &[RoomType::Storage],
            // Floor::Cellar => &[RoomType::Kitchen, RoomType::Pantry],
            // Floor::DayFloor => &[RoomType::Dining, RoomType::Drawing],
            // Floor::NightFloor => &[RoomType::Library, RoomType::Study],
            // Floor::Attic => &[RoomType::Storage],
            Floor::DayFloor => &[
                RoomType::Kitchen,
                // RoomType::Pantry,
                RoomType::Dining,
                // RoomType::Drawing,
                RoomType::Library,
                // RoomType::Study,
            ],
        }
    }

    pub const fn private_rooms(floor: Floor) -> &'static [RoomType] {
        match floor {
            // Floor::Crypt => &[RoomType::Storage],
            // Floor::Cellar => &[RoomType::Storage],
            // Floor::DayFloor => &[RoomType::Bathroom],
            // Floor::NightFloor => &[RoomType::Bedroom],
            // Floor::Attic => &[RoomType::Storage],
            Floor::DayFloor => &[RoomType::Bedroom, RoomType::Storage],
            // Floor::DayFloor => &[RoomType::Storage, RoomType::Bathroom, RoomType::Bedroom],
        }
    }

    pub const fn secret_rooms() -> &'static [RoomType] {
        // &[RoomType::AlchemicLab, RoomType::Cell, RoomType::Temple]
        &[RoomType::AlchemicLab, RoomType::Temple]
    }
}

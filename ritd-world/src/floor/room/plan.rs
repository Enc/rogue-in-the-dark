use crate::world::{Coord, CoordIndex, Dir};
use rand::Rng;
// use rand::distributions::{Bernoulli, Distribution};
use rand::prelude::*;
use serde::{Deserialize, Serialize};

pub type Doors = Vec<(Dir, CoordIndex)>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RoomPlan {
    pub north: CoordIndex,
    pub south: CoordIndex,
    pub east: CoordIndex,
    pub west: CoordIndex,
    pub doors: Doors,
}

impl RoomPlan {
    pub const ROOM_MIN_SIZE: CoordIndex = 5;
    pub const ROOM_MAX_SIZE: CoordIndex = 2 * Self::ROOM_MIN_SIZE + 1;

    pub fn size(&self) -> CoordIndex {
        (self.south - self.north) * (self.east - self.west)
    }

    pub fn nw_corner(&self) -> Coord {
        Coord::new(self.north, self.west).expect("TODO: manage error value!")
    }

    // Returns the collection of `Coord`s belonging to the rooom.
    pub fn coords(&self) -> Vec<Coord> {
        let height = self.south - self.north + 1;
        let width = self.east - self.west + 1;
        let mut coords = Vec::with_capacity(height * width);

        for row in self.north..=self.south {
            for col in self.west..=self.east {
                coords.push(Coord::new(row, col).expect("TODO: manage error value"));
            }
        }

        coords
    }

    pub fn center(&self) -> Coord {
        Coord::new((self.south + self.north) / 2, (self.east + self.west) / 2)
            .expect("TODO: manage error value")
    }

    pub fn inside_coords(&self) -> Vec<Coord> {
        let height = self.south - self.north - 1;
        let width = self.east - self.west - 1;
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 1)..self.south {
            for col in (self.west + 1)..self.east {
                coords.push(Coord::new(row, col).expect("TODO: manage error value"));
            }
        }

        coords
    }

    pub fn inner_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north - 3).max(0);
        let width = (self.east - self.west - 3).max(0);
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 2)..=(self.south - 2) {
            for col in (self.west + 2)..=(self.east - 2) {
                coords.push(Coord::new(row, col).expect("TODO: manage error value"));
            }
        }

        coords
    }

    // pub fn border_coords_without_doors(&self) -> Vec<Coord> {
    //     let height = (self.south - self.north - 1) as usize;
    //     let width = (self.east - self.west - 1) as usize;
    //     let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

    //     for row in (self.north + 2)..=(self.south - 2) {
    //         if !self.doors.contains(&(Dir::West, row)) {
    //             coords.push(Coord {
    //                 row,
    //                 col: self.west + 1,
    //             });
    //         }
    //         if !self.doors.contains(&(Dir::East, row)) {
    //             coords.push(Coord {
    //                 row,
    //                 col: self.east - 1,
    //             });
    //         }
    //     }
    //     for col in (self.west + 2)..=(self.east - 2) {
    //         if !self.doors.contains(&(Dir::North, col)) {
    //             coords.push(Coord {
    //                 row: self.north + 1,
    //                 col,
    //             });
    //         }
    //         if !self.doors.contains(&(Dir::South, col)) {
    //             coords.push(Coord {
    //                 row: self.south - 1,
    //                 col,
    //             });
    //         }
    //     }
    //     if !self.doors.contains(&(Dir::North, self.west + 1))
    //         && !self.doors.contains(&(Dir::West, self.north + 1))
    //     {
    //         coords.push(Coord {
    //             row: self.north + 1,
    //             col: self.west + 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::North, self.east - 1))
    //         && !self.doors.contains(&(Dir::East, self.north + 1))
    //     {
    //         coords.push(Coord {
    //             row: self.north + 1,
    //             col: self.east - 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::South, self.west + 1))
    //         && !self.doors.contains(&(Dir::West, self.south - 1))
    //     {
    //         coords.push(Coord {
    //             row: self.south - 1,
    //             col: self.west + 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::South, self.east - 1))
    //         && !self.doors.contains(&(Dir::East, self.south - 1))
    //     {
    //         coords.push(Coord {
    //             row: self.south - 1,
    //             col: self.east - 1,
    //         });
    //     }

    //     coords
    // }

    pub fn border_coords(&self) -> Vec<Coord> {
        let height = self.south - self.north - 1;
        let width = self.east - self.west - 1;
        let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

        for row in self.north + 1..self.south {
            coords.push(Coord::new(row, self.west + 1).expect("TODO: manage error value"));
            coords.push(Coord::new(row, self.east - 1).expect("TODO: manage error value"));
        }
        for col in (self.west + 2)..self.east - 1 {
            coords.push(Coord::new(self.north + 1, col).expect("TODO: manage error value"));
            coords.push(Coord::new(self.south - 1, col).expect("TODO: manage error value"));
        }

        coords
    }

    pub fn wall_coords(&self) -> Vec<Coord> {
        let height = self.south - self.north + 1;
        let width = self.east - self.west + 1;
        let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

        for row in self.north..=self.south {
            if !self.doors.contains(&(Dir::West, row)) {
                coords.push(Coord::new(row, self.west).expect("TODO: manage error value"));
            }
            if !self.doors.contains(&(Dir::East, row)) {
                coords.push(Coord::new(row, self.east).expect("TODO: manage error value"));
            }
        }
        for col in (self.west + 1)..self.east {
            if !self.doors.contains(&(Dir::North, col)) {
                coords.push(Coord::new(self.north, col).expect("TODO: manage error value"));
            }
            if !self.doors.contains(&(Dir::South, col)) {
                coords.push(Coord::new(self.south, col).expect("TODO: manage error value"));
            }
        }

        coords
    }

    pub fn floor_coords(&self) -> Vec<Coord> {
        let height = self.south - self.north - 3;
        let width = self.east - self.west - 3;
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 1)..self.south {
            for col in (self.west + 1)..self.east {
                coords.push(Coord::new(row, col).expect("TODO: manage error value"));
            }
        }

        coords
    }

    pub fn contains(&self, coord: Coord) -> bool {
        self.north <= coord.row()
            && coord.row() <= self.south
            && self.west <= coord.col()
            && coord.col() <= self.east
    }

    pub fn borders(&self, coord: Coord) -> bool {
        self.north - 1 <= coord.row()
            && coord.row() <= self.south + 1
            && self.west - 1 <= coord.col()
            && coord.col() <= self.east + 1
    }

    pub fn door_coord(&self, door_side: Dir, index: CoordIndex) -> Coord {
        match door_side {
            Dir::North => Coord::new(self.north, index).expect("TODO: manage error value"),
            Dir::South => Coord::new(self.south, index).expect("TODO: manage error value"),
            Dir::East => Coord::new(index, self.east).expect("TODO: manage error value"),
            Dir::West => Coord::new(index, self.west).expect("TODO: manage error value"),
        }
    }

    pub fn add_door(&mut self, coord: Coord) {
        if coord.row() == self.north
            && self.west < coord.col()
            && coord.col() < self.east
            && !self.doors.contains(&(Dir::North, coord.col()))
        {
            self.doors.push((Dir::North, coord.col()));
        }
        if coord.row() == self.south
            && self.west < coord.col()
            && coord.col() < self.east
            && !self.doors.contains(&(Dir::South, coord.col()))
        {
            self.doors.push((Dir::South, coord.col()));
        }
        if coord.col() == self.west
            && self.north < coord.row()
            && coord.row() < self.south
            && !self.doors.contains(&(Dir::West, coord.row()))
        {
            self.doors.push((Dir::West, coord.row()));
        }
        if coord.col() == self.east
            && self.north < coord.row()
            && coord.row() < self.south
            && !self.doors.contains(&(Dir::East, coord.row()))
        {
            self.doors.push((Dir::East, coord.row()));
        }
    }

    pub fn intersects(&self, other: &RoomPlan) -> bool {
        let delta_v = (other.south - other.north) / 2 + 1;
        let delta_h = (other.east - other.west) / 2 + 1;
        let plan = RoomPlan {
            north: self.north - delta_v,
            south: self.south + delta_v,
            west: self.west - delta_h,
            east: self.east + delta_h,
            doors: Vec::new(),
        };
        plan.contains(other.center())
    }

    pub fn split<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self)> {
        let room_length_h = (self.east - self.west + 1) as u32;
        let room_length_v = (self.south - self.north + 1) as u32;

        if rng.gen_ratio(room_length_v, room_length_h + room_length_v) {
            self.split_vertical(rng)
                .or_else(|| self.split_horizontal(rng))
        } else {
            self.split_horizontal(rng)
                .or_else(|| self.split_vertical(rng))
        }
    }

    fn split_horizontal<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self)> {
        let wall = (self.west + Self::ROOM_MIN_SIZE + 1..self.east - Self::ROOM_MIN_SIZE)
            .filter(|&col| col % 2 == 0)
            .choose(rng)?;

        let mut west_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::West => true,
                Dir::East => false,
                _ => index < wall,
            })
            .cloned()
            .collect();
        let mut east_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::West => false,
                Dir::East => true,
                _ => index >= wall,
            })
            .cloned()
            .collect();

        let door = rng.gen_range(self.north + 1..self.south - 1) / 2 * 2 + 1; // enforce parity
        west_doors.push((Dir::East, door));
        east_doors.push((Dir::West, door));

        let west_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: wall,
            west: self.west,
            doors: west_doors,
        };

        let east_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: self.east,
            west: wall,
            doors: east_doors,
        };

        Some((west_room, east_room))
    }

    fn split_vertical<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self)> {
        let wall = (self.north + Self::ROOM_MIN_SIZE + 1..self.south - Self::ROOM_MIN_SIZE)
            .filter(|&row| row % 2 == 0)
            .choose(rng)?;

        let mut north_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::North => true,
                Dir::South => false,
                _ => index < wall,
            })
            .cloned()
            .collect();
        let mut south_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::North => false,
                Dir::South => true,
                _ => index >= wall,
            })
            .cloned()
            .collect();

        let door = rng.gen_range(self.west + 1..self.east - 1) / 2 * 2 + 1; // enforce parity
        north_doors.push((Dir::South, door));
        south_doors.push((Dir::North, door));

        let north_room = RoomPlan {
            north: self.north,
            south: wall,
            east: self.east,
            west: self.west,
            doors: north_doors,
        };

        let south_room = RoomPlan {
            north: wall,
            south: self.south,
            east: self.east,
            west: self.west,
            doors: south_doors,
        };

        Some((north_room, south_room))
    }

    pub fn split_corridor<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self, Self)> {
        let room_length_h = (self.east - self.west + 1) as u32;
        let room_length_v = (self.south - self.north + 1) as u32;

        if room_length_v < Self::ROOM_MAX_SIZE as u32 || room_length_h < Self::ROOM_MAX_SIZE as u32
        {
            None
        } else if rng.gen_ratio(room_length_v, room_length_h + room_length_v) {
            self.split_vertical_corridor(rng)
            // .or_else(|| self.split_horizontal_corridor(rng))
        } else {
            self.split_horizontal_corridor(rng)
            // .or_else(|| self.split_vertical_corridor(rng))
        }
    }

    fn split_horizontal_corridor<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self, Self)> {
        let wall = (self.west + Self::ROOM_MAX_SIZE + 1..self.east - Self::ROOM_MAX_SIZE)
            .filter(|&col| col % 2 == 1)
            .choose(rng)?;

        let mut west_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::West => true,
                Dir::East => false,
                _ => index < wall,
            })
            .cloned()
            .collect();
        let mut east_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::West => false,
                Dir::East => true,
                _ => index > wall,
            })
            .cloned()
            .collect();

        let corridor_length = self.south - self.north + 1;
        let corridor_west_doors = (self.north + 1..self.south - 1)
            .filter(|&row| row % 2 == 1) // enforce parity
            .choose_multiple(rng, 1 + (corridor_length / Self::ROOM_MAX_SIZE));
        assert!(!corridor_west_doors.is_empty());
        west_doors.extend(corridor_west_doors.iter().map(|&row| (Dir::East, row)));
        let corridor_east_doors = (self.north + 1..self.south - 1)
            .filter(|&row| row % 2 == 1) // enforce parity
            .choose_multiple(rng, 1 + (corridor_length / Self::ROOM_MAX_SIZE));
        assert!(!corridor_east_doors.is_empty());
        east_doors.extend(corridor_east_doors.iter().map(|&row| (Dir::West, row)));

        let corridor = RoomPlan {
            north: self.north,
            south: self.south,
            east: wall + 1,
            west: wall - 1,
            doors: Vec::default(),
        };

        let west_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: wall - 1,
            west: self.west,
            doors: west_doors,
        };

        let east_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: self.east,
            west: wall + 1,
            doors: east_doors,
        };

        Some((west_room, east_room, corridor))
    }

    fn split_vertical_corridor<R: Rng>(&self, rng: &mut R) -> Option<(Self, Self, Self)> {
        let wall = (self.north + Self::ROOM_MAX_SIZE + 1..self.south - Self::ROOM_MAX_SIZE)
            .filter(|&row| row % 2 == 1)
            .choose(rng)?;

        let mut north_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::North => true,
                Dir::South => false,
                _ => index < wall,
            })
            .cloned()
            .collect();
        let mut south_doors: Doors = self
            .doors
            .iter()
            .filter(|&&(dir, index)| match dir {
                Dir::North => false,
                Dir::South => true,
                _ => index > wall,
            })
            .cloned()
            .collect();

        let corridor_length = self.east - self.west + 1;
        let corridor_north_doors = (self.west + 1..self.east - 1)
            .filter(|&col| col % 2 == 1) // enforce parity
            .choose_multiple(rng, 1 + (corridor_length / Self::ROOM_MAX_SIZE));
        assert!(!corridor_north_doors.is_empty());
        north_doors.extend(corridor_north_doors.iter().map(|&col| (Dir::South, col)));
        let corridor_south_doors = (self.west + 1..self.east - 1)
            .filter(|&col| col % 2 == 1) // enforce parity
            .choose_multiple(rng, 1 + (corridor_length / Self::ROOM_MAX_SIZE));
        assert!(!corridor_south_doors.is_empty());
        south_doors.extend(corridor_south_doors.iter().map(|&col| (Dir::North, col)));

        let corridor = RoomPlan {
            north: wall - 1,
            south: wall + 1,
            east: self.east,
            west: self.west,
            doors: Vec::default(),
        };

        let north_room = RoomPlan {
            north: self.north,
            south: wall - 1,
            east: self.east,
            west: self.west,
            doors: north_doors,
        };

        let south_room = RoomPlan {
            north: wall + 1,
            south: self.south,
            east: self.east,
            west: self.west,
            doors: south_doors,
        };

        Some((north_room, south_room, corridor))
    }
}

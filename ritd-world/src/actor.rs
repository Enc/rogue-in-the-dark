mod damage;
mod kind;

pub use damage::{Damage, DamageKind, Element, PhysicalDamage};
pub use kind::ActorKind;

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Alignment {
    Player,
    Dungeon,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Stat {
    Con,
    Dex,
    Int,
    Per,
    Str,
}

pub const STATS: [Stat; 5] = [Stat::Con, Stat::Dex, Stat::Int, Stat::Per, Stat::Str];

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Actor {
    alignment: Alignment,
    kind: ActorKind,
    pub health_points: u8,
    pub constitution: u8,
    pub strength: u8,
    pub dexterity: u8,
    pub perception: u8,
    pub intelligence: u8,
    pub sanity: u8,
    pub burning: u8,
    pub bleeding: u8,
}

impl Actor {
    pub const SANITY: u8 = 10;

    pub fn new(kind: ActorKind, alignment: Alignment) -> Self {
        let mut actor = Actor {
            alignment,
            kind,
            health_points: 0,
            constitution: kind.constitution(),
            strength: kind.strength(),
            dexterity: kind.dexterity(),
            perception: kind.perception(),
            intelligence: kind.intelligence(),
            sanity: Self::SANITY,
            burning: 0,
            bleeding: 0,
        };

        actor.health_points = actor.base_max_health();
        actor
    }

    pub fn alignment(&self) -> Alignment {
        self.alignment
    }

    pub fn kind(&self) -> ActorKind {
        self.kind
    }

    pub fn base_max_health(&self) -> u8 {
        self.constitution * 3
    }
}

use crate::{item::Key, Furniture};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum FloorKind {
    Carpet,
    Plank,
    Stone,
    Tile,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum WallKind {
    Brick,
    Stone,
    Molded,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum TileKind {
    Door(DoorState),
    StairsDown,
    StairsUp,
    Floor(FloorKind),
    Wall(WallKind),
    Window,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum DoorState {
    Open,
    Closed,
    Locked(Key),
}

impl TileKind {
    pub const fn impassable(self) -> bool {
        matches!(
            self,
            TileKind::Wall(_)
                | TileKind::Window
                | TileKind::Door(DoorState::Closed | DoorState::Locked(_))
        )
    }

    pub const fn steppable(self) -> bool {
        matches!(
            self,
            TileKind::Floor(_)
                | TileKind::StairsDown
                | TileKind::StairsUp
                | TileKind::Door(DoorState::Open)
        )
    }

    pub const fn opaque(self) -> bool {
        matches!(
            self,
            TileKind::Wall(_)
                | TileKind::Window
                | TileKind::Door(DoorState::Closed | DoorState::Locked(_))
        )
    }

    pub const fn flammable(self) -> bool {
        matches!(self, TileKind::Door(_))
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Tile {
    pub kind: TileKind,
    pub fire: u8,
    pub furniture: Option<Furniture>,
}

impl Tile {
    const FUEL: u8 = 10;

    pub fn new(kind: TileKind) -> Self {
        Tile {
            kind,
            fire: 0,
            furniture: None,
        }
    }

    pub fn impassable(&self) -> bool {
        self.kind.impassable() || self.furniture.map(Furniture::impassable).unwrap_or(false)
    }

    pub fn steppable(&self) -> bool {
        self.kind.steppable() && self.furniture.map(Furniture::steppable).unwrap_or(true)
    }

    pub fn opaque(&self) -> bool {
        self.kind.opaque() || self.furniture.map(Furniture::opaque).unwrap_or(false)
        // || self.fire > 0
    }

    pub fn flammable(&self) -> bool {
        self.kind.flammable()
            || self
                .furniture
                .map(|furniture| furniture.flammable())
                .unwrap_or(false)
    }

    pub fn set_on_fire(&mut self) {
        if self.flammable() {
            self.fire = Self::FUEL;
        }
        if self
            .furniture
            .map(|furniture| furniture.flammable())
            .unwrap_or(false)
        {
            self.furniture = None;
        }
    }

    pub fn burn(&mut self) {
        if self
            .furniture
            .map(|furniture| furniture.flammable())
            .unwrap_or(false)
        {
            self.furniture = None;
        }
        self.fire = self.fire.saturating_sub(1);
    }
}

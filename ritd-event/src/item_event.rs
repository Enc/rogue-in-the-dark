use rand::Rng;
use ritd_world::{roll_item, Dir, FloorCoord, ItemCoord, ItemKey, World, WorldError, WorldResult};

use crate::Event;

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum ItemEvent {
    Burn,
    Disappear,
    Drop(FloorCoord),
    Fly { dir: Dir, strength: u8 },
    Fall,
    PotionShatter,
}

impl ItemEvent {
    pub fn is_applicable(&self, item_key: ItemKey, world: &World) -> WorldResult<()> {
        match *self {
            ItemEvent::Drop(coord) => {
                let _ = roll_item(world, coord)?;
                Ok(())
            }
            ItemEvent::Fall => {
                let coord = world
                    .item_location(item_key)?
                    .ok_or(WorldError::MissingItem(item_key))?;
                if let ItemCoord::Fly(_) = coord {
                    Ok(())
                } else {
                    // TO_FIX: wrong error
                    Err(WorldError::MissingItem(item_key))
                }
            }
            ItemEvent::Fly { dir, strength } => {
                let coord = world.flying_item_coord(item_key)?;
                let coord_dir = (coord + dir)?;
                if strength == 0 || world.tile_map().is_impassable(coord_dir)? {
                    // self.fall_item(item_key)?;
                    Err(WorldError::ImpassableTerrain(coord))
                } else {
                    Ok(())
                }
            }
            _ => Ok(()),
        }
    }

    // PROBLEM: How to change Game::state?
    pub fn apply<R: Rng>(
        &self,
        item_key: ItemKey,
        world: &mut World,
        _rng: &mut R,
    ) -> WorldResult<Vec<Event>> {
        match *self {
            ItemEvent::Drop(coord) => {
                let drop_coord = roll_item(world, coord)?;
                world.place_item(item_key, ItemCoord::Lay(drop_coord))?;
                Ok(Vec::new())
            }
            ItemEvent::Fall => {
                let coord = world
                    .item_location(item_key)?
                    .ok_or(WorldError::MissingItem(item_key))?;
                if let ItemCoord::Fly(coord) = coord {
                    let drop_coord = roll_item(world, coord)?;
                    world.move_item(item_key, ItemCoord::Lay(drop_coord))?;
                }
                Ok(Vec::new())
            }
            ItemEvent::Fly { dir, strength } => {
                let coord = world.flying_item_coord(item_key)?;
                let fly_coord = (coord + dir)?;
                world.move_item(item_key, ItemCoord::Fly(fly_coord))?;
                let fly_coord_dir = (fly_coord + dir)?;
                if let Some(actor_key) = world.actor_at(coord) {
                    let fall = Event::Item(item_key, ItemEvent::Fall);
                    let hit = Event::Hit {
                        actor_key,
                        item_key,
                    };
                    Ok(vec![hit, fall])
                } else if strength == 0 || world.tile_map().is_impassable(fly_coord_dir)? {
                    let fall = Event::Item(item_key, ItemEvent::Fall);
                    Ok(vec![fall])
                } else {
                    let fly = Event::Item(
                        item_key,
                        ItemEvent::Fly {
                            dir,
                            strength: strength - 1,
                        },
                    );
                    Ok(vec![fly])
                }
            }
            _ => Ok(Vec::new()),
        }
    }
}

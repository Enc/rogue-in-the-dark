use std::vec;

use rand::Rng;
use ritd_world::{
    aim_roll, bleed, burn, can_place, dodge_roll, fire, heal, increase_stat, is_insane,
    random_actor_coord, reduce_sanity, roll_item, take_damage, Dir, FloorCoord, Furniture, Item,
    Potion, Scroll, World, WorldError, WorldResult, FLOOR_ROWS,
};
use ritd_world::{ActorKey, ItemKey};
use ritd_world::{Damage, Stat};

use crate::{Event, ItemEvent, TileEvent};

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum ActorEvent {
    // AcquireEvidence,
    AscendStairs,
    Bleed,
    Blessed(Stat),
    Burn,
    CastFireBolt,
    CastLightning,
    Consume(ItemKey),
    DescendStairs,
    Die,
    Examine(FloorCoord),
    Fall,
    Fire(Dir),
    Fulminate,
    Heal,
    Insane,
    Jump,
    LoseSanity,
    Quaff(ItemKey),
    Read(ItemKey),
    Remove,
    // Scream,
    SetOnFire,
    Shoot(Dir),
    Spawn(FloorCoord),
    Step(Dir),
    TakeDamage(Damage),
    Teleport(FloorCoord),
    TimeTick,
    Upgrade(Stat),
    Wait,
    Wear(ItemKey),
    Unwear,
    Wield(ItemKey),
    Unwield,
}

impl ActorEvent {
    pub fn is_applicable(&self, actor_key: ActorKey, world: &World) -> WorldResult<()> {
        match *self {
            ActorEvent::Examine(coord) => {
                if let Some(furniture) = world.tile_map().furniture(coord)? {
                    match furniture {
                        Furniture::Library { searched } | Furniture::Stove { searched } => {
                            if searched {
                                Err(WorldError::MissingTile(coord))
                            } else {
                                Ok(())
                            }
                        }
                        // TODO: Fix WorldError
                        _ => Err(WorldError::MissingTile(coord)),
                    }
                } else {
                    // TODO: Fix WorldError
                    Err(WorldError::MissingTile(coord))
                }
            }
            ActorEvent::Fire(_) => {
                if let Some(item_key) = world.equipped_weapon(actor_key)? {
                    if let Item::Firearm { ammo, .. } = world.item(item_key)? {
                        if *ammo > 0 {
                            Ok(())
                        } else {
                            Err(WorldError::OutOfAmmo(item_key))
                        }
                    } else {
                        Err(WorldError::NotAFirearm(item_key))
                    }
                } else {
                    Err(WorldError::NoFirearmEquipped(actor_key))
                }
            }
            ActorEvent::Quaff(item_key) => {
                if !world.has_item(actor_key, item_key)? {
                    return Err(WorldError::NotInInventory {
                        actor_key,
                        item_key,
                    });
                }
                let item = world.item(item_key)?;
                if let Item::Potion(_) = item {
                    Ok(())
                } else {
                    Err(WorldError::CannotQuaff(item_key))
                }
            }
            ActorEvent::Read(item_key) => {
                if !world.has_item(actor_key, item_key)? {
                    return Err(WorldError::NotInInventory {
                        actor_key,
                        item_key,
                    });
                }
                let item = world.item(item_key)?;
                if let Item::Scroll(_) | Item::Necronomicon = item {
                    Ok(())
                } else {
                    Err(WorldError::CannotRead(item_key))
                }
            }
            ActorEvent::Step(dir) => {
                let coord = (world.actor_coord(actor_key)? + dir)?;
                let actor = world.actor(actor_key)?;
                can_place(world, actor, coord)?;
                Ok(())
            }
            ActorEvent::Unwear => {
                if world.equipped_armor(actor_key)?.is_some() {
                    Ok(())
                } else {
                    Err(WorldError::NoArmorEquipped(actor_key))
                }
            }
            ActorEvent::Unwield => {
                if world.equipped_weapon(actor_key)?.is_some() {
                    Ok(())
                } else {
                    Err(WorldError::NoWeaponEquipped(actor_key))
                }
            }
            ActorEvent::Wear(item_key) => {
                if world.has_item(actor_key, item_key)? {
                    if let Item::Wearable(_) = *world.item(item_key)? {
                        Ok(())
                    } else {
                        Err(WorldError::NotAnArmor(item_key))
                    }
                } else {
                    Err(WorldError::NotInInventory {
                        actor_key,
                        item_key,
                    })
                }
            }
            ActorEvent::Wield(item_key) => {
                if world.has_item(actor_key, item_key)? {
                    if let Item::Weapon(_) | Item::Firearm { .. } = *world.item(item_key)? {
                        Ok(())
                    } else {
                        Err(WorldError::NotAWeapon(item_key))
                    }
                } else {
                    Err(WorldError::NotInInventory {
                        actor_key,
                        item_key,
                    })
                }
            }
            _ => Ok(()),
        }
    }

    // PROBLEM: How to change Game::state?
    pub fn apply<R: Rng>(
        &self,
        actor_key: ActorKey,
        world: &mut World,
        player: ActorKey,
        rng: &mut R,
    ) -> WorldResult<Vec<Event>> {
        match *self {
            ActorEvent::Bleed => {
                bleed(world, actor_key)?;
                let actor = world.actor(actor_key)?;
                if actor.health_points == 0 {
                    Ok(vec![Event::Actor(actor_key, ActorEvent::Die)])
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::Burn => {
                burn(world, actor_key)?;
                let actor = world.actor(actor_key)?;
                if actor.health_points == 0 {
                    Ok(vec![Event::Actor(actor_key, ActorEvent::Die)])
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::Consume(item_key) => {
                world.remove_from_inventory(actor_key, item_key)?;
                Ok(vec![Event::DeleteItem(item_key)])
            }
            ActorEvent::Die => {
                // If actor_key has an inventory, drop all the items
                world.unequip_armor_from_inventory(actor_key).ok();
                world.unequip_weapon_from_inventory(actor_key).ok();
                let mut consequences = Vec::new();
                // let unwield = Event::Actor(actor_key, ActorEvent::Unwield);
                // let unwear = Event::Actor(actor_key, ActorEvent::Unwear);
                // let mut consequences = vec![unwield, unwear];
                if let Some(inventory) = world.inventory(actor_key)? {
                    for &item_key in inventory {
                        consequences.push(Event::DropItem {
                            actor_key,
                            item_key,
                        });
                    }
                }

                if actor_key == player {
                    consequences.push(Event::Defeat);
                } else {
                    consequences.push(Event::Actor(actor_key, ActorEvent::Remove));
                }
                Ok(consequences)
            }
            ActorEvent::Examine(coord) => {
                if let Some(furniture) = world.tile_map().furniture(coord)? {
                    match furniture {
                        Furniture::Library { searched } if !searched => {
                            world.remove_furniture(coord)?;
                            let ft_library = Furniture::Library { searched: true };
                            world.add_furniture(coord, ft_library)?;

                            const EVIDENCE_PROB: f64 = 0.2;
                            const SANITY_PROB: f64 = EVIDENCE_PROB + 0.2;

                            let rand_val = rng.gen_range(0.0..1.0);
                            if rand_val < EVIDENCE_PROB {
                                Ok(vec![Event::RevealEvidence])
                            } else if rand_val < SANITY_PROB {
                                Ok(vec![Event::Actor(actor_key, ActorEvent::LoseSanity)])
                            } else {
                                let actor_coord = world.actor_coord(actor_key)?;
                                let scroll = Item::Scroll(Scroll::Teleportation);
                                let scroll_coord = roll_item(world, actor_coord)?;
                                let scroll_key = world.add_item(scroll)?;
                                Ok(vec![Event::Item(scroll_key, ItemEvent::Drop(scroll_coord))])
                            }
                        }
                        Furniture::Stove { searched } if !searched => {
                            world.remove_furniture(coord)?;
                            let ft_stove = Furniture::Stove { searched: true };
                            world.add_furniture(coord, ft_stove)?;

                            const FOOD_PROB: f64 = 0.8;

                            let rand_val = rng.gen_range(0.0..1.0);
                            if rand_val < FOOD_PROB {
                                Ok(vec![Event::Actor(actor_key, ActorEvent::Heal)])
                            } else {
                                Ok(vec![Event::Actor(actor_key, ActorEvent::LoseSanity)])
                            }
                        }
                        _ => Ok(Vec::new()),
                    }
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::Fire(dir) => {
                fire(world, actor_key)?;
                let mut events = Vec::new();
                let actor_coord = world.actor_coord(actor_key)?;
                let mut bullet_coord = (actor_coord + dir)?;
                while !world.tile_map().is_impassable(bullet_coord)? {
                    if let Some(target_key) = world.actor_at(bullet_coord) {
                        let aim = aim_roll(world, actor_key, rng)?;
                        let dodge = dodge_roll(world, actor_key, rng)?;
                        if aim >= dodge {
                            events.push(Event::ShootHit {
                                actor_key,
                                target_key,
                            });
                        } else {
                            events.push(Event::ShootMiss {
                                actor_key,
                                target_key,
                            });
                        }
                        break;
                    }
                    events.push(Event::Tile(bullet_coord, TileEvent::Shoot));
                    bullet_coord = (bullet_coord + dir)?;
                }
                Ok(events)
            }
            ActorEvent::Heal => {
                heal(world, actor_key)?;
                Ok(Vec::new())
            }
            ActorEvent::LoseSanity => {
                reduce_sanity(world, actor_key)?;
                if is_insane(world, actor_key)? {
                    let insane = Event::Actor(actor_key, ActorEvent::Insane);
                    let die = Event::Actor(actor_key, ActorEvent::Die);
                    Ok(vec![insane, die])
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::Quaff(item_key) => {
                let item = world.item(item_key)?;
                let mut consequences = Vec::new();
                if let Item::Potion(potion) = item {
                    match potion {
                        Potion::Health => {
                            consequences.push(Event::Actor(actor_key, ActorEvent::Heal))
                        }
                    }
                    consequences.push(Event::Actor(actor_key, ActorEvent::Consume(item_key)));
                    Ok(consequences)
                } else {
                    Err(WorldError::CannotQuaff(item_key))
                }
            }
            ActorEvent::Read(item_key) => {
                let item = world.item(item_key)?;
                let mut consequences = Vec::new();
                match item {
                    Item::Necronomicon => Ok(vec![
                        Event::Necronomicon,
                        Event::RevealEvidence,
                        Event::Actor(actor_key, ActorEvent::Consume(item_key)),
                    ]),
                    Item::Scroll(scroll) => {
                        match scroll {
                            Scroll::Teleportation => {
                                let actor = world.actor(actor_key)?;
                                let coord = random_actor_coord(world, actor, rng)?;
                                consequences
                                    .push(Event::Actor(actor_key, ActorEvent::Teleport(coord)));
                            }
                        }
                        consequences.push(Event::Actor(actor_key, ActorEvent::Consume(item_key)));
                        Ok(consequences)
                    }
                    _ => Err(WorldError::NonReadable(item_key)),
                }
            }
            ActorEvent::Remove => {
                let _ = world.remove_actor(actor_key)?;
                Ok(Vec::new())
            }
            ActorEvent::SetOnFire => {
                world.set_actor_on_fire(actor_key)?;
                Ok(Vec::new())
            }
            // ActorEvent::Spawn(coord) => {
            //     let _ = world.act place_actor(actor_key, coord)?;
            //     Ok(Vec::new())
            // }
            ActorEvent::Step(dir) => {
                let _ = world.step_actor(actor_key, dir)?;
                if world.actor_coord(actor_key)?.coord.row() == FLOOR_ROWS - 1 {
                    // Player escaped mansion
                    Ok(vec![Event::Victory])
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::TakeDamage(damage) => {
                take_damage(world, actor_key, damage)?;
                let actor = world.actor(actor_key)?;
                if actor.health_points == 0 {
                    Ok(vec![Event::Actor(actor_key, ActorEvent::Die)])
                } else {
                    Ok(Vec::new())
                }
            }
            ActorEvent::Teleport(coord) => {
                let _ = world.move_actor(actor_key, coord)?;
                Ok(Vec::new())
            }
            ActorEvent::TimeTick => {
                let mut consequences = Vec::new();

                if let Ok(coord) = world.actor_coord(actor_key) {
                    if world.tile_map().tile(coord)?.fire > 0 {
                        consequences.push(Event::Actor(actor_key, ActorEvent::SetOnFire));
                    }
                }

                let actor = world.actor(actor_key)?;
                if actor.bleeding > 0 {
                    consequences.push(Event::Actor(actor_key, ActorEvent::Bleed));
                }
                if actor.burning > 0 {
                    consequences.push(Event::Actor(actor_key, ActorEvent::Burn));
                }

                Ok(consequences)
            }
            ActorEvent::Unwear => {
                world.unequip_armor_from_inventory(actor_key)?;
                Ok(Vec::new())
            }
            ActorEvent::Unwield => {
                world.unequip_weapon_from_inventory(actor_key)?;
                Ok(Vec::new())
            }
            ActorEvent::Upgrade(stat) => {
                increase_stat(world, actor_key, stat)?;
                Ok(Vec::new())
            }
            ActorEvent::Wear(item_key) => {
                world.equip_armor_from_inventory(actor_key, item_key)?;
                Ok(Vec::new())
            }
            ActorEvent::Wield(item_key) => {
                world.equip_weapon_from_inventory(actor_key, item_key)?;
                Ok(Vec::new())
            }
            _ => Ok(Vec::new()),
        }
    }
}

use rand::Rng;
use ritd_world::{
    Actor, ActorKind, Alignment, Dir, DoorState, FloorCoord, Furniture, Item, Tile, TileKind,
    Weapon, World, WorldError, WorldResult,
};

use crate::{Event, ItemEvent};

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum TileEvent {
    Burn,
    Close,
    Firebolt(Dir),
    Lightning,
    Open,
    Shoot,
}

impl TileEvent {
    pub fn is_applicable(&self, coord: FloorCoord, world: &World) -> WorldResult<()> {
        match self {
            TileEvent::Close => {
                // TODO: cannot close door if actor/item/furniture is in the way.
                match world.tile_map().tile(coord)?.kind {
                    TileKind::Door(DoorState::Open) => Ok(()),
                    _ => Err(WorldError::Uninteracting(coord)),
                }
            }
            TileEvent::Open => {
                if let Some(furniture) = world.tile_map().furniture(coord)? {
                    match furniture {
                        Furniture::Crate { open } if !open => Ok(()),
                        _ => Err(WorldError::Uninteracting(coord)),
                    }
                } else if let TileKind::Door(DoorState::Closed) = world.tile_map().tile(coord)?.kind
                {
                    Ok(())
                } else {
                    Err(WorldError::Uninteracting(coord))
                }
            }
            _ => Ok(()),
        }
    }

    // PROBLEM: How to change Game::state?
    pub fn apply<R: Rng>(
        &self,
        coord: FloorCoord,
        world: &mut World,
        rng: &mut R,
    ) -> WorldResult<Vec<Event>> {
        match *self {
            TileEvent::Close => {
                // TODO: cannot close door if actor/item/furniture is in the way.
                match world.tile_map().tile(coord)?.kind {
                    TileKind::Door(DoorState::Open) => {
                        let door = TileKind::Door(DoorState::Closed);
                        world.place_tile(coord, Tile::new(door))?;
                        Ok(Vec::new())
                    }
                    _ => Err(WorldError::Uninteracting(coord)),
                }
            }
            TileEvent::Open => {
                if let Some(furniture) = world.tile_map().furniture(coord)? {
                    match furniture {
                        Furniture::Crate { open } if !open => {
                            world.remove_furniture(coord)?;
                            let ft_crate = Furniture::Crate { open: true };
                            world.add_furniture(coord, ft_crate)?;

                            const INFESTED_PROB: f64 = 0.333;
                            const EVIDENCE_PROB: f64 = INFESTED_PROB + 0.333;
                            let rand_val = rng.gen_range(0.0..1.0);
                            if rand_val < INFESTED_PROB {
                                // let spawn =
                                //     if world.evidence() < rng.gen_range(0..World::TOTAL_EVIDENCE) {
                                //         ActorKind::Rat
                                //     } else {
                                //         ActorKind::Zombie
                                //     };
                                // TODO: make spawn into an Event
                                // let actor = Actor::new(spawn, Alignment::Dungeon);
                                let actor = Actor::new(ActorKind::Rat, Alignment::Dungeon);
                                let _ = world.insert_actor(actor, coord)?;
                                Ok(Vec::new())
                            } else if rand_val < EVIDENCE_PROB {
                                Ok(vec![Event::RevealEvidence])
                            } else {
                                let hammer = Item::Weapon(Weapon::Hammer);
                                let hammer_key = world.add_item(hammer)?;
                                Ok(vec![Event::Item(hammer_key, ItemEvent::Drop(coord))])
                            }
                        }
                        _ => Ok(Vec::new()),
                    }
                } else if let TileKind::Door(DoorState::Closed) = world.tile_map().tile(coord)?.kind
                {
                    let door = TileKind::Door(DoorState::Open);
                    world.place_tile(coord, Tile::new(door))?;
                    Ok(Vec::new())
                } else {
                    Err(WorldError::Uninteracting(coord))
                }
            }
            _ => Ok(Vec::new()),
        }
    }
}

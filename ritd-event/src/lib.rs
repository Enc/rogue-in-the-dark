use std::vec;

use rand::Rng;
use ritd_world::{
    damage, defense, dodge_roll, hit_roll, roll_item, shot_damage_roll, DoorState, Furniture, Item,
    ItemCoord, Stat, Tile, TileKind, Wand, World, WorldError, WorldResult,
};
use ritd_world::{ActorKey, ItemKey};
use ritd_world::{Dir, FloorCoord};

mod actor_event;
mod item_event;
mod tile_event;

pub use actor_event::ActorEvent;
pub use item_event::ItemEvent;
use serde::{Deserialize, Serialize};
pub use tile_event::TileEvent;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum State {
    Running,
    Paused,
    Corrupted,
    Victory,
    Defeat,
}

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum Event {
    Actor(ActorKey, ActorEvent),
    DeleteItem(ItemKey),
    DropItem {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Escape,
    Fireblast,
    Hit {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Item(ItemKey, ItemEvent),
    MeleeAttack {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    MeleeHit {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    MeleeMiss {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    Necronomicon,
    Pause,
    Pick {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Pray {
        actor_key: ActorKey,
        coord: FloorCoord,
        stat: Stat,
    },
    Pull {
        actor_key: ActorKey,
        dir: Dir,
    },
    Push {
        actor_key: ActorKey,
        dir: Dir,
    },
    Read {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    RevealEvidence,
    ShootHit {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    ShootMiss {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    Throw {
        actor_key: ActorKey,
        item_key: ItemKey,
        dir: Dir,
    },
    Tile(FloorCoord, TileEvent),
    TimeTick,
    Unlock {
        actor_key: ActorKey,
        coord: FloorCoord,
    },
    Victory,
    Zap {
        actor_key: ActorKey,
        wand_key: ItemKey,
        dir: Dir,
    },
    Defeat,
}

impl Event {
    pub fn is_applicable(&self, world: &World) -> WorldResult<()> {
        match *self {
            Event::Actor(actor_key, actor_event) => actor_event.is_applicable(actor_key, world),
            Event::Item(item_key, item_event) => item_event.is_applicable(item_key, world),
            Event::Tile(coord, tile_event) => tile_event.is_applicable(coord, world),
            Event::DeleteItem(item_key) => {
                world.item_exist(item_key)?;
                Ok(())
            }
            Event::DropItem {
                actor_key,
                item_key,
            } => {
                if !world.has_item(actor_key, item_key)? {
                    Err(WorldError::MissingItem(item_key))
                } else {
                    if let Some(armor_key) = world.equipped_armor(actor_key)? {
                        if armor_key == item_key {
                            return Err(WorldError::Worn {
                                actor_key,
                                item_key,
                            });
                        }
                    }
                    if let Some(weapon_key) = world.equipped_weapon(actor_key)? {
                        if weapon_key == item_key {
                            return Err(WorldError::Wield {
                                actor_key,
                                item_key,
                            });
                        }
                    }
                    let coord = world.actor_coord(actor_key)?;
                    let _ = roll_item(world, coord)?;
                    Ok(())
                }
            }
            Event::MeleeAttack {
                actor_key,
                target_key,
            } => {
                let actor_coord = world.actor_coord(actor_key)?;
                let target_coord = world.actor_coord(target_key)?;
                if FloorCoord::distance(actor_coord, target_coord).unwrap_or(usize::MAX) > 1 {
                    Err(WorldError::OutOfRange {
                        actor_key,
                        target_key,
                    })
                } else {
                    Ok(())
                }
            }
            Event::Pick {
                actor_key,
                item_key,
            } => {
                let actor_coord = world.actor_coord(actor_key)?;
                let item_coord = world.item_coord(item_key)?;
                if actor_coord != item_coord {
                    Err(WorldError::MissingItem(item_key))
                } else if world.is_inventory_full(actor_key)? {
                    Err(WorldError::InventoryFull(actor_key))
                } else {
                    Ok(())
                }
            }
            Event::Pull { actor_key, dir } => {
                let coord = world.actor_coord(actor_key)?;
                let coord_dir = (coord - dir)?;
                if let Some(furniture) = world.tile_map().furniture(coord_dir)? {
                    if !furniture.is_movable() {
                        Err(WorldError::Furniture(coord_dir, furniture))
                    } else {
                        let step = Event::Actor(actor_key, ActorEvent::Step(dir));
                        step.is_applicable(world)?;
                        Ok(())
                    }
                } else {
                    Err(WorldError::NoFurniture(
                        (coord - dir).expect("TODO: manage error"),
                    ))
                }
            }
            Event::Push { actor_key, dir } => {
                let coord = world.actor_coord(actor_key)?;
                let coord_dir = (coord + dir)?;
                if let Some(furniture) = world.tile_map().furniture(coord_dir)? {
                    let coord_dir_dir = (coord_dir + dir)?;
                    if !furniture.is_movable() {
                        Err(WorldError::Furniture(coord_dir, furniture))
                    } else if world.is_steppable(coord_dir_dir)?
                        && world.actor_at(coord_dir_dir).is_none()
                    {
                        Ok(())
                    } else {
                        Err(WorldError::NotSteppable(coord_dir_dir))
                    }
                } else {
                    Err(WorldError::NoFurniture(coord_dir))
                }
            }
            Event::Throw {
                actor_key,
                item_key,
                ..
            } => {
                if !world.has_item(actor_key, item_key)? {
                    Err(WorldError::MissingItem(item_key))
                } else {
                    if let Some(armor_key) = world.equipped_armor(actor_key)? {
                        if armor_key == item_key {
                            return Err(WorldError::Worn {
                                actor_key,
                                item_key,
                            });
                        }
                    }
                    if let Some(weapon_key) = world.equipped_weapon(actor_key)? {
                        if weapon_key == item_key {
                            return Err(WorldError::Wield {
                                actor_key,
                                item_key,
                            });
                        }
                    }
                    let _ = world.actor_coord(actor_key)?;
                    Ok(())
                }
            }
            Event::Unlock { actor_key, coord } => {
                let actor_coord = world.actor_coord(actor_key)?;
                if FloorCoord::distance(actor_coord, coord).ok_or(WorldError::NotSameFloor)? > 1 {
                    // Suitable error
                    todo!();
                } else if let TileKind::Door(DoorState::Locked(key)) =
                    world.tile_map().tile(coord)?.kind
                {
                    let is_key = |item: &Item| {
                        if let Item::Key(keyhole) = item {
                            *keyhole == key
                        } else {
                            false
                        }
                    };

                    let _ = *world
                        .inventory(actor_key)?
                        .ok_or(WorldError::NoInventory(actor_key))?
                        .iter()
                        .find(|item_key| is_key(world.item(**item_key).expect("item")))
                        .ok_or(WorldError::NoKey)?;
                    Ok(())
                } else {
                    Err(WorldError::Uninteracting(coord))
                }
            }
            Event::Zap {
                actor_key,
                wand_key,
                dir: _,
            } => {
                if !world.has_item(actor_key, wand_key)? {
                    return Err(WorldError::NotInInventory {
                        actor_key,
                        item_key: wand_key,
                    });
                }
                let item = world.item(wand_key)?;
                if let Item::Wand(_) = item {
                    Ok(())
                } else {
                    Err(WorldError::CannotZap(wand_key))
                }
            }
            _ => Ok(()),
        }
    }

    // PROBLEM: How to change Game::state?
    pub fn apply<R: Rng>(
        &self,
        world: &mut World,
        state: &mut State,
        player: ActorKey,
        rng: &mut R,
    ) -> WorldResult<Vec<Event>> {
        match *self {
            Event::Actor(actor_key, actor_event) => {
                actor_event.apply(actor_key, world, player, rng)
            }
            Event::Item(item_key, item_event) => item_event.apply(item_key, world, rng),
            Event::Tile(coord, tile_event) => tile_event.apply(coord, world, rng),
            Event::Defeat => {
                *state = State::Defeat;
                Ok(vec![])
            }
            Event::DeleteItem(item_key) => {
                world.delete_item(item_key)?;
                Ok(Vec::new())
            }
            Event::DropItem {
                actor_key,
                item_key,
            } => {
                // TO_FIX: Fails for wielded items!
                world.remove_from_inventory(actor_key, item_key)?;
                let coord = world.actor_coord(actor_key)?;
                let drop_coord = roll_item(world, coord)?;
                world.place_item(item_key, ItemCoord::Lay(drop_coord))?;
                Ok(Vec::new())
                // Ok(vec![Event::Item(item_key, ItemEvent::Drop(coord))])
            }
            Event::MeleeAttack {
                actor_key,
                target_key,
            } => {
                let hit_roll = hit_roll(world, actor_key, rng)?;
                let dodge_roll = dodge_roll(world, actor_key, rng)?;
                if hit_roll >= dodge_roll {
                    Ok(vec![Event::MeleeHit {
                        actor_key,
                        target_key,
                    }])
                } else {
                    Ok(vec![Event::MeleeMiss {
                        actor_key,
                        target_key,
                    }])
                }
            }
            Event::MeleeHit {
                actor_key,
                target_key,
            } => {
                let mut damage = damage(world, actor_key, rng)?;
                let defense = defense(world, target_key, damage.kind, rng)?;
                damage.amount = damage.amount.saturating_sub(defense);
                Ok(vec![Event::Actor(
                    target_key,
                    ActorEvent::TakeDamage(damage),
                )])
            }
            Event::Pause => {
                *state = State::Paused;
                Ok(vec![])
            }
            Event::Pick {
                actor_key,
                item_key,
            } => {
                let _ = world.remove_item(item_key)?;
                world
                    .add_to_inventory(actor_key, item_key)
                    .expect("add item to actor's inventory");
                Ok(Vec::new())
            }
            Event::Pray {
                actor_key,
                coord,
                stat,
            } => {
                if let Some(Furniture::Altar { used }) = world.tile_map().furniture(coord)? {
                    if used {
                        Err(WorldError::UsedAltar(coord))
                    } else {
                        let altar = Furniture::Altar { used: true };
                        world.remove_furniture(coord)?;
                        world.add_furniture(coord, altar)?;
                        let lose_sanity = Event::Actor(actor_key, ActorEvent::LoseSanity);
                        let blessed = Event::Actor(actor_key, ActorEvent::Blessed(stat));
                        let upgrade = Event::Actor(actor_key, ActorEvent::Upgrade(stat));
                        Ok(vec![blessed, upgrade, lose_sanity])
                        // self.broadcast(Event::Actor(actor_key, ActorEvent::Upgrade(stat)))?;
                    }
                } else {
                    Err(WorldError::NoAltar(coord))
                }
            }
            Event::Pull { actor_key, dir } => {
                let coord = world.actor_coord(actor_key)?;
                let coord_dir = (coord - dir)?;
                world.move_furniture(coord_dir, dir)?;
                // Swap place with items
                if let Some(item_key) = world.item_at(ItemCoord::Lay(coord)) {
                    // TODO: use an event for this, e.g., DropItem
                    let _ = world.move_item(item_key, ItemCoord::Lay(coord_dir))?;
                }
                let step = Event::Actor(actor_key, ActorEvent::Step(dir));
                Ok(vec![step])
            }
            Event::Push { actor_key, dir } => {
                let coord = world.actor_coord(actor_key)?;
                let coord_dir = (coord + dir)?;
                world.move_furniture(coord_dir, dir)?;
                // Swap place with items
                let coord_dir_dir = (coord_dir + dir)?;
                if let Some(item_key) = world.item_at(ItemCoord::Lay(coord_dir_dir)) {
                    // TODO: use an event for this, e.g., DropItem
                    let _ = world.move_item(item_key, ItemCoord::Lay(coord_dir))?;
                }
                let step = Event::Actor(actor_key, ActorEvent::Step(dir));
                Ok(vec![step])
            }
            Event::RevealEvidence => {
                world.reveal_evidence();
                if world.evidence() >= World::TOTAL_EVIDENCE {
                    // self.state = State::Victory;
                    // self.broadcast(Event::Victory)?;
                }
                Ok(Vec::new())
            }
            Event::ShootHit {
                actor_key,
                target_key,
            } => {
                let mut damage = shot_damage_roll(world, actor_key, rng)?;
                let defense = defense(world, target_key, damage.kind, rng)?;
                damage.amount = damage.amount.saturating_sub(defense);
                Ok(vec![Event::Actor(
                    target_key,
                    ActorEvent::TakeDamage(damage),
                )])
            }
            Event::Throw {
                actor_key,
                item_key,
                dir,
            } => {
                world.remove_from_inventory(actor_key, item_key)?;
                let coord = world.actor_coord(actor_key)?;
                world.place_item(item_key, ItemCoord::Fly(coord))?;
                let actor = world.actor(actor_key)?;
                let strength = actor.strength;
                Ok(vec![Event::Item(
                    item_key,
                    ItemEvent::Fly { dir, strength },
                )])
            }
            Event::TimeTick => {
                world.time_tick()?;
                // self.expand_fire()?;

                // TODO: spawning
                // // Spawn new mobs
                // let spawn_prob = 0.01 * world.evidence() as f64 / World::TOTAL_EVIDENCE as f64;
                // if rng.gen_bool(spawn_prob) {
                //     let coord = if rng.gen() {
                //         Coord::new(
                //             if rng.gen() { 1 } else { FLOOR_ROWS - 2 } as CoordIndex,
                //             (rng.gen_range(0..FLOOR_COLS - 1) / 2 * 2 + 1) as CoordIndex,
                //         )
                //     } else {
                //         Coord::new(
                //             (rng.gen_range(0..FLOOR_ROWS - 1) / 2 * 2 + 1) as CoordIndex,
                //             if rng.gen() { 1 } else { FLOOR_COLS - 2 } as CoordIndex,
                //         )
                //     }?;
                //     // TODO: select floor in some sensible way
                //     let floor_coord = FloorCoord { coord, floor: 0 };
                //     // Try to spawn in selected coord, but just give up if it raises an error.
                //     self.spawn(floor_coord, ActorKind::Cultist).ok();
                // }

                let consequences = world
                    .actors()
                    .keys()
                    .map(|actor_key| Event::Actor(actor_key, ActorEvent::TimeTick))
                    .collect::<Vec<Event>>();
                Ok(consequences)
            }
            Event::Unlock { actor_key, coord } => match world.tile_map().tile(coord)?.kind {
                TileKind::Door(DoorState::Locked(key)) => {
                    let is_key = |item: &Item| {
                        if let Item::Key(keyhole) = item {
                            *keyhole == key
                        } else {
                            false
                        }
                    };

                    let item_key = *world
                        .inventory(actor_key)?
                        .ok_or(WorldError::NoInventory(actor_key))?
                        .iter()
                        .find(|item_key| is_key(world.item(**item_key).expect("item")))
                        .ok_or(WorldError::NoKey)?;
                    let door = TileKind::Door(DoorState::Open);
                    world.place_tile(coord, Tile::new(door))?;
                    Ok(vec![Event::Actor(actor_key, ActorEvent::Consume(item_key))])
                }
                _ => Err(WorldError::Uninteracting(coord)),
            },
            Event::Victory => {
                *state = State::Victory;
                Ok(vec![])
            }
            Event::Zap {
                actor_key,
                wand_key,
                dir: _,
            } => {
                let mut consequences = Vec::new();
                let item = world.item(wand_key)?;

                if let Item::Wand(wand) = item {
                    // let actor = self.world.actor(actor_key)?;
                    match wand {
                        Wand::Lightning => {
                            consequences.push(Event::Actor(actor_key, ActorEvent::CastLightning))
                        }
                        Wand::FireBolt => {
                            consequences.push(Event::Actor(actor_key, ActorEvent::CastFireBolt))
                        }
                    }
                    consequences.push(Event::Actor(actor_key, ActorEvent::Consume(wand_key)));
                    Ok(consequences)
                    // TODO: wand charges
                } else {
                    Err(WorldError::UnusableItem(wand_key))
                }
            }
            _ => Ok(Vec::new()),
        }
    }
}

use crate::dictionary::*;
use ritd_game::ritd_ai::*;
use ritd_game::ritd_world::*;

pub fn status(world_view: &WorldView) -> String {
    let player_key = world_view.pov_key();
    let player_coord = world_view.actor_coord(player_key).expect("player's coord");
    if let Ok(Some(item_key)) = world_view.item_at(player_coord) {
        let item = world_view
            .item(item_key)
            .expect("an item")
            .expect("see item");
        format!("There is a {} at your feet.", item_name(item))
    // TODO: status message for furniture?
    // } else if let Some(feature) = tile.feature() {
    //     String::from(match feature {
    //         Furniture::Blood { .. } => "You are standing in a puddle of blood.",
    //         Furniture::Door { open } if open => "You are standing under a door.",
    //         Furniture::Flooded => "You are knee-deep in slimy stagnant water.",
    //         Furniture::Vegetation { .. } => "You are standing on a patch of grass.",
    //         Furniture::Fire { .. } => "You are enveloped by flames!",
    //         _ => "You are standing on something???",
    //     })
    } else {
        let tile = world_view
            .see_tile(player_coord)
            .expect("tile")
            .expect("see tile");
        String::from(tile_description(*tile))
    }
    // } else {
    //     String::from("YOU ARE DEAD!!!")
    // }
}

fn tile_description(tile: Tile) -> &'static str {
    match tile.kind {
        TileKind::Floor(floor_kind) => match floor_kind {
            FloorKind::Plank => "You are standing on wooden planks.",
            FloorKind::Tile => "You are standing on a tiled floor.",
            FloorKind::Carpet => "You are standing on an old, moldy carpet.",
            FloorKind::Stone => "You are standing on a stone floor.",
        },
        TileKind::StairsDown => "You are standing on a staircase leading downward.",
        TileKind::StairsUp => "You are standing on a staircase leading upward.",
        _ => "You are standing on something???",
    }
}

pub fn action_outcome(_world_view: &WorldView, action: Action, outcome: WorldError) -> String {
    match action {
        // TODO: step error message
        // Action::Step { .. } => {
        //     if let GameError::World(WorldError::NotSteppable(coord)) = outcome {
        //         let tile = world_view.see_tile(coord).expect("tile").expect("see tile");
        //         if !tile.kind().steppable() {
        //             return format!("You cannot step into a {}.", tilekind_name(tile.kind()));
        //         } else if let Some(feature) = tile.feature() {
        //             if !feature.steppable() {
        //                 return format!("You cannot step into a {}.", furniture_name(feature));
        //             }
        //         }
        //     }

        //     format!("{:?} fails because {:?}", action, outcome)
        // }
        Action::Pick => match outcome {
            WorldError::NoItem(..) => String::from("There is nothing to pick up here."),
            // WorldError::Actor {
            //     actor_error: ActorError::InventoryFull,
            //     ..
            // } => String::from("You are fully loaded and cannot carry any further item."),
            _ => format!("{:?} fails because {:?}", action, outcome),
        },
        _ => format!("{:?} fails because {:?}", action, outcome),
    }
}

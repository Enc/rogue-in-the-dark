use crossterm::style::Color;
use ritd_game::ritd_world::*;

// pub const DUNGEON_INTRO: &str = "You have entered the Bottomless Dungeon, you fool!";

// pub const JUMP_CONFIRM: &str =
//     "Are you sure you want to jump down? You will get injured in the fall and might even die!";
// pub const YES: &str = "YES";
// pub const NO: &str = "NO";

// pub fn dir_name(dir: Dir) -> &'static str {
//     match dir {
//         Dir::North => "North",
//         Dir::West => "West",
//         Dir::South => "South",
//         Dir::East => "East",
//     }
// }

// pub fn stat_name(stat: Stat) -> &'static str {
//     match stat {
//         Stat::Con => "Constitution",
//         Stat::Dex => "Dexterity",
//         Stat::Int => "Intelligence",
//         Stat::Per => "Perception",
//         Stat::Str => "Strength",
//     }
// }

// pub fn furniture_background_color(furniture: &Furniture) -> Color {
//     match furniture {
//         Furniture::Altar { used: false } => Color::Yellow,
//         // Furniture::Blood => Color::DarkRed,
//         // Furniture::Fire { .. } => Color::Red,
//         // Furniture::Flooded => Color::Blue,
//         // Furniture::Vegetation { .. } => Color::Green,
//         _ => todo!(),
//     }
// }

pub fn tile_color(tile: Tile) -> Color {
    if tile.fire > 0 {
        Color::Red
    } else {
        tile_kind_color(tile.kind)
    }
}

pub fn tile_kind_color(kind: TileKind) -> Color {
    match kind {
        TileKind::Door(door_state) => match door_state {
            DoorState::Closed => Color::DarkYellow,
            DoorState::Open => Color::DarkYellow,
            DoorState::Locked(key) => match key {
                Key::Iron => Color::DarkGrey,
                Key::Brass => Color::Yellow,
                Key::Silver => Color::DarkGrey,
            },
        },
        TileKind::Floor(floor_kind) => match floor_kind {
            FloorKind::Carpet => Color::Green,
            FloorKind::Plank => Color::Yellow,
            FloorKind::Stone => Color::DarkGrey,
            FloorKind::Tile => Color::Blue,
        },
        TileKind::Wall(wall_kind) => match wall_kind {
            WallKind::Stone => Color::DarkGrey,
            WallKind::Brick => Color::DarkRed,
            WallKind::Molded => Color::DarkGreen,
        },
        TileKind::StairsDown | TileKind::StairsUp => Color::Grey,
        TileKind::Window => Color::Blue,
        // TileKind::Chasm => Color::Grey,
    }
}

pub fn tile_kind_inverse(kind: TileKind) -> bool {
    matches!(
        kind,
        TileKind::Door(DoorState::Closed | DoorState::Locked(_))
    )
}

pub fn furniture_symb(furniture: Furniture) -> char {
    match furniture {
        Furniture::Altar { .. } => '±',
        Furniture::Armchair => 'Ω',
        Furniture::Bed => 'Θ',
        Furniture::Chair => '╥',
        Furniture::Library { .. } => '≡',
        // Furniture::Library { searched: false } => '⊞',
        Furniture::Crate { .. } => 'X',
        // Furniture::Crate { open: false } => '▤',
        // Furniture::Fire { .. } => '^',
        // Furniture::Vegetation { trampled: true } => '"',
        // Furniture::Vegetation { trampled: false } => '&',
        Furniture::Stove { .. } => 'Φ',
        Furniture::Table => '╤',
    }
}

pub fn furniture_color(furniture: Furniture) -> Color {
    match furniture {
        Furniture::Altar { used: false } | Furniture::Stove { searched: false } => Color::Black,
        Furniture::Crate { open: false } | Furniture::Library { searched: false } => Color::DarkRed,
        _ => Color::DarkGrey,
    }
}

pub fn furniture_inverse(kind: Furniture) -> bool {
    matches!(kind, Furniture::Crate { .. } | Furniture::Library { .. })
}

pub fn tile_symb(tile: Tile) -> char {
    if tile.fire > 0 {
        '^'
    } else {
        tile_kind_symb(tile.kind)
    }
}

pub fn tile_kind_symb(kind: TileKind) -> char {
    match kind {
        TileKind::Door(DoorState::Open) => '∩',
        TileKind::Door(DoorState::Closed) => '┼',
        TileKind::Door(DoorState::Locked(_)) => '╪',
        TileKind::Floor(_) => '·',
        TileKind::Wall(_) => '#',
        // TileKind::Chasm => '░',
        TileKind::StairsDown => '>',
        TileKind::StairsUp => '<',
        TileKind::Window => '"', // ⌸
    }
}

pub fn box_wall_symbol(count: u8) -> char {
    match count {
        0 => '○',
        1 => '╜',
        2 => '╒',
        3 => '╚',
        4 => '╖',
        5 => '║',
        6 => '╔',
        7 => '╠',
        8 => '╕',
        9 => '╝',
        10 => '═',
        11 => '╩',
        12 => '╗',
        13 => '╣',
        14 => '╦',
        15 => '╬',
        _ => unreachable!("max count is 1 + 2 + 4 + 8 = 15"),
    }
}

pub fn box_window_symbol(count: u8) -> char {
    match count {
        0 => '○',
        1 => '╜',
        2 => '╒',
        3 => '╚',
        4 => '╖',
        5 => '│',
        6 => '╔',
        7 => '╠',
        8 => '╕',
        9 => '╝',
        10 => '─',
        11 => '╩',
        12 => '╗',
        13 => '╣',
        14 => '╦',
        15 => '╬',
        _ => unreachable!("max count is 1 + 2 + 4 + 8 = 15"),
    }
}

// pub fn furniture_name(furniture: Furniture) -> &'static str {
//     match furniture {
//         Furniture::Armchair => "armchair",
//         Furniture::Altar { .. } => "altar",
//         // Furniture::Blood => "blood",
//         Furniture::Bed => "bed",
//         Furniture::Chair => "chair",
//         // Furniture::Fire { .. } => "fire",
//         // Furniture::Flooded => "flooded",
//         // Furniture::Vegetation { trampled: true } => "trampled vegetation",
//         // Furniture::Vegetation { trampled: false } => "dense vegetation",
//         Furniture::Crate { open: true } => "open crate",
//         Furniture::Crate { open: false } => "closed crate",
//         Furniture::Library => "library",
//         Furniture::Table => "table",
//     }
// }

// pub fn tile_name(tile: Tile) -> &'static str {
//     match tile {
//         Tile::Door(DoorState::Open) => "open door",
//         Tile::Door(DoorState::Closed) => "closed door",
//         Tile::Door(DoorState::Locked(_)) => "locked door",
//         Tile::Floor(_) => "floor",
//         Tile::Wall(_) => "wall",
//         // Tile::Chasm => "chasm",
//         Tile::StairsDown => "downward stairs",
//         Tile::StairsUp => "upward stairs",
//     }
// }

pub fn actor_symb(actor: &Actor) -> char {
    match actor.kind() {
        ActorKind::Player => '@',
        ActorKind::Bat => 'B',
        ActorKind::Rat => 'R',
        ActorKind::Cultist => 'C',
        // ActorKind::Goblin => 'G',
        // ActorKind::Troll => 'T',
        ActorKind::Zombie => 'Z',
    }
}

pub fn actor_name(actor: &Actor) -> &'static str {
    match actor.kind() {
        ActorKind::Player => "rogue",
        ActorKind::Bat => "bat",
        ActorKind::Rat => "rat",
        ActorKind::Cultist => "cultist",
        // ActorKind::Goblin => "goblin",
        // ActorKind::Troll => "troll",
        ActorKind::Zombie => "zombie",
    }
}

pub fn item_symb(item: &Item) -> char {
    match item {
        Item::Firearm { firearm: _, .. } => '⌐',
        Item::Key(_) => '♀',
        Item::Wearable(_) => '[',
        Item::Potion(_) => '¡',
        Item::Scroll(_) => '♫',
        Item::Wand(_) => 'τ',
        Item::Weapon(_) => '↑',
        Item::Necronomicon => '⎅',
    }
}

pub fn item_name(item: &Item) -> &'static str {
    match item {
        Item::Firearm { firearm, .. } => match firearm {
            Firearm::Revolver => "revolver",
            Firearm::Shotgun => "shotgun",
        },
        Item::Key(key) => match key {
            Key::Iron => "iron key",
            Key::Brass => "brass key",
            Key::Silver => "silver key",
        },
        Item::Potion(potion) => potion_name(*potion),
        Item::Scroll(scroll) => match scroll {
            Scroll::Teleportation => "teleportation scroll",
        },
        Item::Wand(wand) => match wand {
            Wand::FireBolt => "firebolt wand",
            Wand::Lightning => "lightning wand",
        },
        Item::Wearable(armor) => match armor {
            Wearable::Robe => "robe",
            Wearable::BulletproofVest => "bulletproof vest",
        },
        Item::Weapon(weapon) => match weapon {
            Weapon::Axe => "axe",
            Weapon::Dagger => "dagger",
            Weapon::Knife => "knife",
            Weapon::Hammer => "hammer",
        },
        Item::Necronomicon => "Necronomicon",
    }
}

pub fn potion_name(potion: Potion) -> &'static str {
    match potion {
        Potion::Health => "health potion",
    }
}

pub fn item_inventory_descr(item: &Item) -> String {
    String::from(item_name(item))
        + &match item {
            Item::Firearm { firearm, ammo } => format!(" ({}/{})", ammo, firearm.ammo_capacity()),
            _ => String::new(),
        }
}

// pub fn item_description(item: &Item) -> &'static str {
//     match item {
//         Item::Potion(potion) => match potion {
//             Potion::Health => "A flask swirling with crimson liquid.",
//         },
//         Item::Scroll(scroll) => match scroll {
//             Scroll::AScroll => "A parchment scroll covered in dwarven runes.",
//         },
//         Item::Wand(wand) => match wand {
//             Wand::FireBolt => "A wooden wand.",
//             Wand::Lightning => "A metallic wand.",
//         },
//         Item::Wearable(wearable) => match wearable {
//             Wearable::Robe => "A cultist's robes.",
//             Wearable::BulletproofVest => "A bulletproof vest.",
//         },
//         Item::Weapon(weapon) => match weapon {
//             Weapon::Dagger => "A thin and pointy dagger, excellent for stabbing.",
//             Weapon::Knife => "A large butcher's knife.",
//             Weapon::Axe => "A large axe to chop wood, and other things as well.",
//             Weapon::Hammer => "A hammer, heavy enough to crush a man's head.",
//         },
//         Item::Necronomicon => "The fabled book of the dead!",
//     }
// }

pub fn damage_name(damage_kind: DamageKind) -> &'static str {
    match damage_kind {
        DamageKind::Physical(physical_damage) => match physical_damage {
            PhysicalDamage::Bludgeoning => "bludgeoning",
            PhysicalDamage::Piercing => "piercing",
            PhysicalDamage::Slashing => "slashing",
        },
        DamageKind::Elemental(elemental_damage) => match elemental_damage {
            Element::Electricity => "electricity",
            Element::Fire => "fire",
            Element::Cold => "cold",
        },
    }
}

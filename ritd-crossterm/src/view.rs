use crate::dictionary::*;
use crossterm::{
    cursor::MoveTo,
    event::{read, Event as CrossEvent, KeyCode},
    execute, queue,
    style::{Attribute, Color, Print, SetAttribute, SetBackgroundColor, SetForegroundColor},
    terminal::{BeginSynchronizedUpdate, Clear, ClearType, EndSynchronizedUpdate},
};
use ritd_event::*;
use ritd_game::ritd_ai::*;
use ritd_game::ritd_world::*;
use serde::{Deserialize, Serialize};
use serde_with::*;
use std::fmt::Write as _;
use std::io::{stdout, Write};

#[serde_as]
#[derive(Debug, Deserialize, Serialize)]
pub struct View {
    investigator: String,
    log: Vec<String>,
    #[serde_as(as = "Vec<[[_; FLOOR_ROWS]; FLOOR_COLS]>")]
    memory_map: Vec<[[Option<Tile>; FLOOR_ROWS]; FLOOR_COLS]>,
}

#[typetag::serde]
impl Player for View {
    fn decide(&mut self, world_view: &WorldView) -> Action {
        self.frame(world_view);

        loop {
            match read().expect("event") {
                CrossEvent::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Up => match self.dir_action(world_view, Dir::North) {
                            Some(action) => action,
                            None => continue,
                        },
                        KeyCode::Down => match self.dir_action(world_view, Dir::South) {
                            Some(action) => action,
                            None => continue,
                        },
                        KeyCode::Left => match self.dir_action(world_view, Dir::West) {
                            Some(action) => action,
                            None => continue,
                        },
                        KeyCode::Right => match self.dir_action(world_view, Dir::East) {
                            Some(action) => action,
                            None => continue,
                        },
                        KeyCode::Char(num) if num > '0' && num <= '9' => {
                            let num = num as usize - '0' as usize - 1;
                            let inventory = world_view.pov_inventory().expect("player's inventory");
                            if let Some(item_key) = inventory.iter().nth(num) {
                                if let Some(action) = self.item_query(world_view, *item_key) {
                                    action
                                } else {
                                    self.status(world_view);
                                    continue;
                                }
                            } else {
                                self.status(world_view);
                                continue;
                            }
                        }
                        KeyCode::Char('.') => Action::Wait,
                        KeyCode::Char('g') => Action::Pick,
                        KeyCode::Char('c') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Close(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('f') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Fire(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('o') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Open(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('u') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Unlock(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('p') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Pull(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('P') => {
                            if let Some(dir) = self.direction_query() {
                                Action::Push(dir)
                            } else {
                                continue;
                            }
                        }
                        KeyCode::Char('<') => Action::AscendStairs,
                        KeyCode::Char('>') => Action::DescendStairs,
                        KeyCode::Esc => Action::Pause,
                        _ => continue,
                    };
                }
                CrossEvent::Resize(_, _) => {
                    // TODO: check if screen too small.
                    queue!(stdout(), Clear(ClearType::All)).expect("clear screen");
                    self.frame(world_view);
                    continue;
                }
                _ => continue,
            }
        }
    }

    fn react(&mut self, world_view: &WorldView, event: &Event) {
        self.update_memory_map(world_view);

        const FRAME_TIME: std::time::Duration = std::time::Duration::from_millis(100);
        // self.view.render(world_view);
        self.log(world_view, event);
        match event {
            Event::Item(_, item_event) => match item_event {
                ItemEvent::Fly { .. } => {
                    self.frame(world_view);
                    std::thread::sleep(FRAME_TIME);
                }
                _ => {}
            },
            Event::Tile(coord, tile_event) => match tile_event {
                TileEvent::Burn => {
                    self.burn(*coord);
                }
                TileEvent::Lightning => {
                    self.lightning(*coord);
                    std::thread::sleep(FRAME_TIME);
                }
                TileEvent::Firebolt(dir) => {
                    self.firebolt(*coord, *dir);
                    std::thread::sleep(FRAME_TIME);
                }
                TileEvent::Shoot => {
                    self.shoot(*coord);
                    std::thread::sleep(FRAME_TIME);
                }
                _ => {}
            },
            Event::Actor(actor_key, actor_event) => match *actor_event {
                ActorEvent::Die => {
                    if world_view.pov_key() == *actor_key {
                        // Print frame to update log
                        // TODO: Needs better solution
                        self.frame(world_view);
                        self.end_screen();
                    }
                }
                _ => {}
            },
            Event::Fireblast => std::thread::sleep(FRAME_TIME),
            Event::Victory => {
                self.frame(world_view);
                self.end_screen();
            }
            _ => {}
        }
    }

    fn outcome(&mut self, world_view: &WorldView, action: Action, outcome: WorldResult<()>) {
        self.frame(world_view);
        if let Err(game_error) = outcome {
            self.action_outcome(world_view, action, game_error);
        } else {
            self.status(world_view);
        }
        // Do something?
    }
}

impl View {
    pub const STATUS_LN: u16 = 1;
    pub const INVENTORY_WIDTH: u16 = 20;
    pub const PLAYER_WIDTH: u16 = 20;
    pub const LOG_LNS: u16 = 3;
    pub const PADDING: u16 = 3;

    pub fn new(investigator: String) -> View {
        const INTRO_1: &str =
            "You ventured into the Mansion after hearing strange reports and wild rumors about it.";
        const INTRO_2: &str = "The heavy front door slams behind you. You are trapped inside!";
        const INTRO_3: &str =
            "Your only hope is to solve the mistery before ending up dead… or worse!";
        View {
            investigator,
            log: vec![INTRO_1.to_owned(), INTRO_2.to_owned(), INTRO_3.to_owned()],
            // TODO: fix ugly hack
            memory_map: vec![[[None; FLOOR_ROWS]; FLOOR_COLS]; FLOORS],
        }
    }

    fn update_memory_map(&mut self, world_view: &WorldView) {
        let actor_coord = world_view.pov_coord();
        let sight_map = world_view.sight_map();
        let floor = actor_coord.floor;
        let memory_map = &mut self.memory_map[floor];
        for row in 0..FLOOR_ROWS {
            for col in 0..FLOOR_COLS {
                let coord = Coord::new(row, col).expect("coords within bounds");
                if sight_map[coord] {
                    if let Ok(tile) = world_view.see_tile(FloorCoord { floor, coord }) {
                        memory_map[coord] = tile.cloned();
                    }
                }
            }
        }
    }

    fn dir_action(&self, world_view: &WorldView, dir: Dir) -> Option<Action> {
        let actor_key = world_view.pov_key();
        let player_coord = world_view.actor_coord(actor_key).expect("player's coord");
        let coord = (player_coord + dir).expect("TODO: handle error");
        if world_view
            .actor_at(coord)
            .expect("look for actors")
            .is_some()
        {
            Some(Action::MeleeAttack(dir))
        } else {
            let tile = world_view
                .see_tile(coord)
                .expect("tile")
                .expect("see adjacent tile");
            match tile.furniture {
                Some(Furniture::Crate { open }) => {
                    if open {
                        Some(Action::Push(dir))
                    } else {
                        Some(Action::Open(dir))
                    }
                }
                Some(Furniture::Altar { .. }) => match self.stat_query() {
                    Some(stat) => Some(Action::PrayAltar { dir, stat }),
                    _ => {
                        self.status(world_view);
                        None
                    }
                },
                Some(Furniture::Library { searched } | Furniture::Stove { searched }) => {
                    if searched {
                        Some(Action::Push(dir))
                    } else {
                        Some(Action::Examine(dir))
                    }
                }
                Some(_) => Some(Action::Push(dir)),
                // None => Some(Action::Step(dir)),
                _ => match tile.kind {
                    TileKind::Door(DoorState::Open) => Some(Action::Step(dir)),
                    TileKind::Door(DoorState::Closed) => Some(Action::Open(dir)),
                    TileKind::Door(DoorState::Locked(_)) => Some(Action::Unlock(dir)),
                    // TileKind::Chasm => match self.confirm_query(&crate::dictionary::JUMP_CONFIRM) {
                    //     Some(true) => Some(Action::Jump(dir)),
                    //     _ => {
                    //         self.status(world_view);
                    //         None
                    //     }
                    // },
                    _ => Some(Action::Step(dir)),
                },
            }
        }
    }

    fn frame(&self, world_view: &WorldView) {
        execute!(
            stdout(),
            BeginSynchronizedUpdate,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
        )
        .expect("begin sync update");
        self.print_log();
        self.print_player_header(world_view);
        self.print_life(world_view);
        self.print_sanity(world_view);
        self.print_evidence(world_view);
        self.print_stats(world_view);
        self.print_inventory(world_view);
        self.draw_world(world_view);
        execute!(stdout(), EndSynchronizedUpdate).expect("end sync update");
        stdout().flush().expect("flush to output stream");
    }

    fn draw_world(&self, view: &WorldView) {
        let mut stdout = stdout();
        for row in 0..FLOOR_ROWS {
            for col in 0..FLOOR_COLS {
                let coord = Coord::new(row, col).expect("coord within bounds");

                let (background_color, foreground_color, symbol) = self.draw_cell(view, coord);

                queue!(
                    stdout,
                    MoveTo(
                        col as u16 + Self::INVENTORY_WIDTH,
                        row as u16 + Self::STATUS_LN
                    ),
                    SetForegroundColor(foreground_color),
                    SetBackgroundColor(background_color),
                    Print(symbol),
                )
                .expect("print cell");
            }
        }
    }

    fn draw_cell(&self, view: &WorldView, coord: Coord) -> (Color, Color, char) {
        let foreground;
        let mut background;
        let symbol;

        let self_coord = view.actor_coord(view.pov_key()).expect("coord");
        let floor_coord = FloorCoord {
            coord,
            floor: self_coord.floor,
        };

        if let Some(tile) = view.see_tile(floor_coord).expect("seen tile") {
            background = Color::White;
            if let Some(actor_key) = view.actor_at(floor_coord).expect("get actor") {
                let actor = view.actor(actor_key).expect("actor").expect("actor");
                symbol = actor_symb(actor);
                foreground = Color::Black;
            } else if let Some(item_key) = view.flying_item_at(floor_coord).expect("get items") {
                let item = view.item(item_key).expect("item").expect("item");
                symbol = item_symb(item);
                foreground = Color::Black;
            } else if let Some(item_key) = view.item_at(floor_coord).expect("get items") {
                let item = view.item(item_key).expect("item").expect("item");
                symbol = item_symb(item);
                foreground = Color::Black;
            } else if let Some(furniture) = tile.furniture {
                symbol = furniture_symb(furniture);
                if furniture_inverse(furniture) {
                    background = furniture_color(furniture);
                    foreground = Color::White;
                } else {
                    background = Color::White;
                    foreground = furniture_color(furniture);
                }
            } else if let TileKind::Wall(_) = tile.kind {
                symbol = self.wall_symbol(floor_coord);
                foreground = tile_kind_color(tile.kind);
            } else if let TileKind::Window = tile.kind {
                symbol = self.window_symbol(floor_coord);
                foreground = tile_kind_color(tile.kind);
            } else {
                symbol = tile_symb(*tile);
                if tile_kind_inverse(tile.kind) {
                    background = tile_kind_color(tile.kind);
                    foreground = Color::White;
                } else {
                    background = Color::White;
                    foreground = tile_kind_color(tile.kind);
                }
            }
        } else if let Some(tile) = self.memory_map[floor_coord.floor][floor_coord.coord] {
            if let Some(furniture) = tile.furniture {
                symbol = furniture_symb(furniture);
                if furniture_inverse(furniture) {
                    background = furniture_color(furniture);
                    foreground = Color::Black;
                } else {
                    background = Color::Black;
                    foreground = furniture_color(furniture);
                }
            } else {
                if tile_kind_inverse(tile.kind) {
                    background = tile_kind_color(tile.kind);
                    foreground = Color::Black;
                } else {
                    background = Color::Black;
                    foreground = tile_kind_color(tile.kind);
                }
                if let TileKind::Wall(_) = tile.kind {
                    symbol = self.wall_symbol(floor_coord);
                } else if let TileKind::Window = tile.kind {
                    symbol = self.window_symbol(floor_coord);
                } else {
                    symbol = tile_symb(tile);
                }
            }
        } else {
            background = Color::Black;
            foreground = Color::DarkGrey;
            symbol = '░';
        }
        (background, foreground, symbol)
    }

    fn wall_symbol(&self, floor_coord: FloorCoord) -> char {
        let mut count: u8 = 0;
        for (idx, dir) in DIRS.into_iter().enumerate() {
            if let Ok(coord) = floor_coord + dir {
                if let Some(tile) = self.memory_map[coord.floor][coord.coord] {
                    if matches!(
                        tile.kind,
                        TileKind::Wall(_) | TileKind::Window | TileKind::Door(_)
                    ) {
                        count += 2_u8.pow(idx as u32);
                    }
                }
            }
        }
        box_wall_symbol(count)
        // match count {
        //     1 | 4 | 5 => '|',
        //     2 | 8 | 10 => '-',
        //     _ => '+',
        // }
    }

    fn window_symbol(&self, floor_coord: FloorCoord) -> char {
        let mut count: u8 = 0;
        for (idx, dir) in DIRS.into_iter().enumerate() {
            if let Ok(coord) = floor_coord + dir {
                if let Some(tile) = self.memory_map[coord.floor][coord.coord] {
                    if matches!(
                        tile.kind,
                        TileKind::Wall(_) | TileKind::Window | TileKind::Door(_)
                    ) {
                        count += 2_u8.pow(idx as u32);
                    }
                }
            }
        }
        box_window_symbol(count)
    }

    fn print_status(&self, status: &str) {
        execute!(
            stdout(),
            MoveTo(0, 0),
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            Clear(ClearType::CurrentLine),
            Print(status),
        )
        .expect("print status");
    }

    pub fn status(&self, world_view: &WorldView) {
        use crate::status::*;
        let status = status(world_view);
        self.print_status(&status);
    }

    pub fn action_outcome(&self, world_view: &WorldView, action: Action, outcome: WorldError) {
        use crate::status::*;
        let action_outcome = action_outcome(world_view, action, outcome);
        self.print_status(&action_outcome);
    }

    pub fn log(&mut self, world_view: &WorldView, event: &Event) {
        use crate::log::*;
        if let Some(entry) = log_event(world_view, event) {
            if log_new_line(event) {
                self.log.push(entry);
            } else if let Some(previous_entry) = self.log.last_mut() {
                write!(previous_entry, " {}", entry).expect("write");
            }
        }
    }

    pub fn print_log(&self) {
        let mut stdout = stdout();

        queue!(
            stdout,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
        )
        .expect("set log style");

        for (line, entry) in self
            .log
            .iter()
            .rev()
            .take(Self::LOG_LNS as usize)
            .rev()
            .enumerate()
        {
            queue!(
                stdout,
                MoveTo(0, line as u16 + Self::STATUS_LN + FLOOR_ROWS as u16),
                Clear(ClearType::CurrentLine),
                Print(entry),
            )
            .expect("print log entry");
        }
    }

    fn print_inventory(&self, world_view: &WorldView) {
        let mut stdout = stdout();

        queue!(
            stdout,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            MoveTo(
                Self::INVENTORY_WIDTH + FLOOR_COLS as u16 + Self::PADDING,
                Self::STATUS_LN + Self::PADDING,
            ),
            SetAttribute(Attribute::Bold),
            SetAttribute(Attribute::Underlined),
            Print("Inventory"),
            SetAttribute(Attribute::Reset),
        )
        .expect("print inventory header");

        for line in 0..10 {
            queue!(
                stdout,
                MoveTo(
                    Self::INVENTORY_WIDTH + FLOOR_COLS as u16 + Self::PADDING,
                    Self::STATUS_LN + 3 + line
                ),
                SetForegroundColor(Color::White),
                SetBackgroundColor(Color::Black),
                Clear(ClearType::UntilNewLine),
            )
            .expect("clear inventory");
        }

        let inventory = world_view.pov_inventory().expect("player's inventory");
        for (idx, item_key) in inventory.iter().enumerate() {
            let item = world_view.item(*item_key).expect("item").expect("see item");
            queue!(
                stdout,
                MoveTo(
                    Self::INVENTORY_WIDTH + FLOOR_COLS as u16 + 1,
                    Self::STATUS_LN + 3 + idx as u16
                ),
                SetForegroundColor(Color::White),
                SetBackgroundColor(Color::Black),
                Print(format!("{}) {}", idx + 1, item_inventory_descr(item))),
            )
            .expect("print inventory item");
        }
    }

    fn item_query(&self, world_view: &WorldView, item_key: ItemKey) -> Option<Action> {
        let item = world_view.item(item_key).expect("item").expect("see item");
        let query = format!(
            "{}: (t)hrow, (d)rop, (q)aff, (r)ead, (w)ear, (W)ield or (z)ap?",
            item_name(item)
        );
        self.print_status(&query);

        loop {
            match read().expect("event") {
                CrossEvent::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'd' => Some(Action::DropItem(item_key)),
                            'q' => Some(Action::Quaff(item_key)),
                            'r' => Some(Action::Read(item_key)),
                            'w' => Some(Action::EquipArmor(item_key)),
                            'W' => Some(Action::EquipWeapon(item_key)),
                            // 'e' => Some(Action::EquipFirearm(item_key)),
                            't' => self
                                .direction_query()
                                .map(|dir| Action::Throw { item_key, dir }),
                            'z' => self.direction_query().map(|dir| Action::Zap {
                                wand_key: item_key,
                                dir,
                            }),
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn confirm_query(&self, message: &str) -> Option<bool> {
        self.print_status(&format!("{} (y/n)", message));

        loop {
            match read().expect("event") {
                CrossEvent::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'y' | 'Y' => Some(true),
                            'n' | 'N' => Some(false),
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn direction_query(&self) -> Option<Dir> {
        self.print_status("In which direction? (n/s/w/e)");

        loop {
            match read().expect("event") {
                CrossEvent::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'n' | 'N' => Some(Dir::North),
                            's' | 'S' => Some(Dir::South),
                            'e' | 'E' => Some(Dir::East),
                            'w' | 'W' => Some(Dir::West),
                            _ => None,
                        },
                        KeyCode::Up => Some(Dir::North),
                        KeyCode::Down => Some(Dir::South),
                        KeyCode::Right => Some(Dir::East),
                        KeyCode::Left => Some(Dir::West),
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn stat_query(&self) -> Option<Stat> {
        self.print_status("Which stat do you want to increase? (c)onstitution; (s)trength; (d)exterity; (p)erception; (i)ntelligence");

        loop {
            match read().expect("event") {
                CrossEvent::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'c' | 'C' => Some(Stat::Con),
                            's' | 'S' => Some(Stat::Str),
                            'd' | 'D' => Some(Stat::Dex),
                            'p' | 'P' => Some(Stat::Per),
                            'i' | 'I' => Some(Stat::Int),
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn print_player_header(&self, _world_view: &WorldView) {
        let mut stdout = stdout();
        queue!(
            stdout,
            MoveTo(0, Self::STATUS_LN + 1),
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            SetAttribute(Attribute::Underlined),
            Print(&self.investigator),
            SetAttribute(Attribute::Reset),
        )
        .expect("print inventory header");
    }

    // Alt-stat line with one line per stat
    // fn print_stats(&self, world_view: &WorldView) {
    //     let actor = world_view.pov_actor();
    //     let mut stdout = stdout();

    //     let con_line = format!("CON: {}", actor.constitution);
    //     let dex_line = format!("DEX: {}", actor.dexterity);
    //     let int_line = format!("INT: {}", actor.intelligence);
    //     let per_line = format!("PER: {}", actor.perception);
    //     let str_line = format!("STR: {}", actor.strength);

    //     queue!(
    //         stdout,
    //         SetForegroundColor(Color::White),
    //         SetBackgroundColor(Color::Black),
    //         MoveTo(
    //             0,
    //             Self::PADDING_TOP + 7
    //         ),
    //         Print(con_line),
    //         MoveTo(
    //             0,
    //             Self::PADDING_TOP + 8
    //         ),
    //         Print(dex_line),
    //         MoveTo(
    //             0,
    //             Self::PADDING_TOP + 9
    //         ),
    //         Print(int_line),
    //         MoveTo(
    //             0,
    //             Self::PADDING_TOP + 10
    //         ),
    //         Print(per_line),
    //         MoveTo(
    //             0,
    //             Self::PADDING_TOP + 11
    //         ),
    //         Print(str_line),
    //     )
    //     .expect("print inventory header");
    // }

    fn print_stats(&self, world_view: &WorldView) {
        let actor = world_view.pov_actor();
        let mut stdout = stdout();

        let stats_line = format!(
            "C:{} D:{} I:{} P:{} S:{}",
            actor.constitution,
            actor.dexterity,
            actor.intelligence,
            actor.perception,
            actor.strength
        );

        queue!(
            stdout,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            MoveTo(0, Self::STATUS_LN + 7),
            Print(stats_line),
        )
        .expect("print inventory header");
    }

    fn print_life(&self, world_view: &WorldView) {
        let actor = world_view.pov_actor();
        self.print_bar(
            "HP",
            (
                actor.health_points,
                world_view
                    .pov_max_health_points()
                    .expect("pov health points"),
            ),
            Color::Red,
            Color::DarkRed,
            (0, Self::STATUS_LN + 3),
            Self::INVENTORY_WIDTH as u8 - 2,
        );
    }

    fn print_sanity(&self, world_view: &WorldView) {
        let actor = world_view.pov_actor();
        self.print_bar(
            "Sanity",
            (actor.sanity, Actor::SANITY),
            Color::Blue,
            Color::DarkBlue,
            (0, Self::STATUS_LN + 4),
            Self::INVENTORY_WIDTH as u8 - 2,
        );
    }

    fn print_evidence(&self, world_view: &WorldView) {
        self.print_bar(
            "Lore",
            (world_view.revealed_evidence(), WorldView::TOTAL_EVIDENCE),
            Color::Magenta,
            Color::DarkMagenta,
            (0, Self::STATUS_LN + 5),
            Self::INVENTORY_WIDTH as u8 - 2,
        );
    }

    fn print_bar(
        &self,
        label: &str,
        value: (u8, u8),
        fore_color: Color,
        back_color: Color,
        pos: (u16, u16),
        length: u8,
    ) {
        let mut stdout = stdout();

        let fore_length = length as usize * value.0 as usize / value.1 as usize;
        let label_value = format!(" {}: {}/{}", label, value.0, value.1);

        queue!(
            stdout,
            MoveTo(pos.0, pos.1),
            SetForegroundColor(Color::Black),
            SetBackgroundColor(fore_color),
        )
        .expect("set bar style");

        for (i, chr) in label_value
            .chars()
            .chain(" ".chars().cycle())
            .take(length as usize)
            .enumerate()
        {
            if i == fore_length {
                queue!(stdout, SetBackgroundColor(back_color)).expect("set back color");
            }

            queue!(stdout, Print(chr)).expect("print bar");
        }
    }

    fn burn(&self, floor_coord: FloorCoord) {
        let mut stdout = stdout();

        queue!(
            stdout,
            MoveTo(
                floor_coord.coord.col() as u16 + Self::INVENTORY_WIDTH,
                floor_coord.coord.row() as u16 + Self::STATUS_LN
            ),
            SetForegroundColor(Color::Red),
            SetBackgroundColor(Color::DarkRed),
            Print('*'),
        )
        .expect("print firebolt");

        stdout.flush().expect("flush output to stdout");
    }

    fn firebolt(&self, floor_coord: FloorCoord, dir: Dir) {
        let mut stdout = stdout();

        let symb = match dir {
            Dir::North | Dir::South => '|',
            Dir::East | Dir::West => '-',
        };

        queue!(
            stdout,
            MoveTo(
                floor_coord.coord.col() as u16 + Self::INVENTORY_WIDTH,
                floor_coord.coord.row() as u16 + Self::STATUS_LN
            ),
            SetForegroundColor(Color::Red),
            SetBackgroundColor(Color::DarkRed),
            Print(symb),
        )
        .expect("print firebolt");

        stdout.flush().expect("flush output to stdout");
    }

    fn lightning(&self, floor_coord: FloorCoord) {
        let mut stdout = stdout();

        queue!(
            stdout,
            MoveTo(
                floor_coord.coord.col() as u16 + Self::INVENTORY_WIDTH,
                floor_coord.coord.row() as u16 + Self::STATUS_LN
            ),
            SetForegroundColor(Color::DarkCyan),
            SetBackgroundColor(Color::Cyan),
            Print('~'),
        )
        .expect("print lightning");

        stdout.flush().expect("flush output to stdout");
    }

    fn shoot(&self, floor_coord: FloorCoord) {
        let mut stdout = stdout();

        queue!(
            stdout,
            MoveTo(
                floor_coord.coord.col() as u16 + Self::INVENTORY_WIDTH,
                floor_coord.coord.row() as u16 + Self::STATUS_LN
            ),
            SetForegroundColor(Color::Red),
            SetBackgroundColor(Color::Black),
            Print('*'),
        )
        .expect("print firebolt");

        stdout.flush().expect("flush output to stdout");
    }

    fn end_screen(&self) {
        self.print_status("Press <ESC> to exit the game.");

        loop {
            match read().expect("event") {
                CrossEvent::Key(event) if matches!(event.code, KeyCode::Esc) => break,
                _ => continue,
            }
        }
    }
}

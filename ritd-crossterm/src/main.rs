//! # Rogue in the Dungeon
//!
//! Rogue in the Dungeon is a traditional fantasy roguelike game.
//!
//! The player delves into a dungeon, fighting the creatures and monsters that inhabit it.
//! The goal is simply to descend as deep as possible before dying.
//!
//! This documentation is a technical reference for the internals of the game engine.

#![forbid(unsafe_code)]

use clap::{Parser, Subcommand};
use crossterm::{
    cursor, execute,
    style::{Attribute, Color, SetBackgroundColor, SetForegroundColor, Stylize},
    terminal::{
        disable_raw_mode, enable_raw_mode, size, Clear, ClearType, EnterAlternateScreen,
        LeaveAlternateScreen, SetSize,
    },
};
use ritd_event::State;
use std::{error::Error, io::stdout};

use ritd_crossterm::{
    create_savefile, delete, list_savegames, load_game, save_game, View, DESCRIPTION, RITD_COLS,
    RITD_ROWS, TITLE,
};
use ritd_game::Game;

#[derive(Parser)]
#[command(name = TITLE)]
#[command(author = "Enc <enc.cat@protonmail.com>")]
#[command(version = "in-developement (pre-alpha)")]
#[command(about = format!("{}{}{TITLE}{}\n{}{DESCRIPTION}{}",
    Attribute::Bold,
    Attribute::Underlined,
    Attribute::Reset,
    Attribute::Italic,
    Attribute::Reset),
    long_about = None
)]
struct Ritd {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Lists saved investigators
    List,
    /// Starts a new investigation
    New {
        /// The name of the new investigator
        // #[arg(short, long)]
        investigator: String,
    },
    /// Resume an investigation
    Resume {
        /// The name of the investigator to resume
        // #[arg(short, long)]
        investigator: String,
    },
    /// Delete an investigation
    Delete {
        /// The name of the investigator to resume
        // #[arg(short, long)]
        investigator: String,
    },
}

fn main() -> Result<(), Box<dyn Error>> {
    let ritd = Ritd::parse();

    match &ritd.command {
        Commands::List => {
            list_investigators()?;
        }
        Commands::New { investigator } => {
            new_game(investigator)?;
        }
        Commands::Resume { investigator } => {
            resume_game(investigator)?;
        }
        Commands::Delete { investigator } => delete_game(investigator),
    }

    Ok(())
}

fn list_investigators() -> Result<(), Box<dyn Error>> {
    let list = list_savegames()?;
    if list.is_empty() {
        println!("There are no ongoing investigations.");
    } else {
        println!("{}", "Investigators".bold().underlined());
        for actor in list {
            println!("{actor}");
        }
    }
    Ok(())
}

fn new_game(investigator: &str) -> Result<(), Box<dyn Error>> {
    match create_savefile(investigator) {
        Err(error) => {
            println!("Error: {}", error);
            println!("Investigator {investigator} could not be created");
            Ok(())
        }
        Ok(()) => {
            println!("Investigator {investigator} savefile created");
            let player = View::new(investigator.to_owned());
            let mut game = Game::generate(None, Box::new(player));
            println!("Game for investigator {investigator} generated");
            run_game(&mut game, investigator)
        }
    }
}

fn resume_game(investigator: &str) -> Result<(), Box<dyn Error>> {
    match load_game(investigator) {
        Ok(mut game) => run_game(&mut game, investigator),
        Err(error) => {
            println!("Error: {}", error);
            println!("Investigator {investigator} could not be resumed");
            Ok(())
        }
    }
}

fn delete_game(investigator: &str) {
    match delete(investigator) {
        Ok(()) => println!("Investigator {investigator} deleted"),
        Err(error) => {
            println!("Error: {}", error);
            println!("Investigator {investigator} could not be deleted");
        }
    }
}

// TODO: delete savegame when game ends
fn run_game(game: &mut Game, investigator: &str) -> Result<(), Box<dyn Error>> {
    let (cols, rows) = size()?;

    execute!(
        stdout(),
        EnterAlternateScreen,
        SetForegroundColor(Color::White),
        SetBackgroundColor(Color::Black),
        SetSize(RITD_COLS, RITD_ROWS),
        cursor::Hide,
        Clear(ClearType::All),
    )?;
    enable_raw_mode()?;

    // Trying to catch_unwind seems to not work...
    // let mut wrapper = AssertUnwindSafe(&mut *game);
    // let result = panic::catch_unwind(move || wrapper.run());
    let outcome = game.run();

    match game.state() {
        State::Running => panic!("game should not be running"),
        State::Paused => save_game(game, investigator)?,
        State::Corrupted => {}
        State::Victory | State::Defeat => delete(investigator)?,
    }

    execute!(
        stdout(),
        Clear(ClearType::All),
        cursor::Show,
        LeaveAlternateScreen,
        // Reset original size
        SetSize(cols, rows),
    )?;
    disable_raw_mode()?;

    match game.state() {
        State::Running => panic!("Game should not be running"),
        State::Paused => println!("Game closed without errors\nGame state for investigator {investigator} has been saved correctly"),
        // TODO: distinguish case when there is a previous savefile or not
        State::Corrupted => println!("Game state is corrupted\nPrevious save files have not been overwritten and can be resumed"),
        State::Victory => println!("Game closed without errors after victory\nInvestigator {investigator} has been deleted"),
        State::Defeat => println!("Game closed without errors after defeat\nInvestigator {investigator} has been deleted"),
    }

    // match result {
    //     Ok(outcome) => outcome?,
    //     Err(error) => panic::resume_unwind(error),
    // }
    outcome?;
    Ok(())
}

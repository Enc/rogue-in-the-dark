mod dictionary;
mod log;
mod status;
mod view;

pub use self::view::View;
use directories::ProjectDirs;
use ritd_game::ritd_world::*;
use ritd_game::Game;
use std::fs::remove_file;
use std::fs::DirBuilder;
use std::{
    fs::File,
    io::{BufReader, Read},
    path::PathBuf,
};

pub const RITD_ROWS: u16 = View::STATUS_LN + FLOOR_ROWS as u16 + View::LOG_LNS;
pub const RITD_COLS: u16 =
    View::PLAYER_WIDTH + View::PADDING + FLOOR_COLS as u16 + View::PADDING + View::INVENTORY_WIDTH;

pub const TITLE: &str = "Rogue in the Dark";
pub const FOLDER: &str = "RogueInTheDark";
pub const DESCRIPTION: &str = "A Lovecraftian horror game of mistery and madness";
// pub const INTRO: &str = "You ventured into the Mansion \
// following strange reports of suspicious activities taking place therein.
// As the heavy front door slams behind you, you realize that you are trapped inside,
// and your only hope is to solve the mistery before ending up dead… or worse!";
const EXTENSION: &str = "ritd";

fn savegame_dir() -> std::io::Result<PathBuf> {
    let path = ProjectDirs::from("", "", FOLDER)
        .expect("app data dir")
        .data_dir()
        .to_owned();
    DirBuilder::new().recursive(true).create(&path)?;
    Ok(path)
}

fn savegame_path(investigator: &str) -> std::io::Result<PathBuf> {
    let mut path = savegame_dir()?;
    path.push(investigator);
    path.set_extension(EXTENSION);
    Ok(path)
}

pub fn list_savegames() -> std::io::Result<Vec<String>> {
    let files = savegame_dir()?
        .read_dir()?
        .filter_map(|file| {
            file.ok().and_then(|f| {
                f.path()
                    .file_stem()
                    .and_then(|f| f.to_owned().into_string().ok())
            })
        })
        .collect();
    Ok(files)
}

pub fn create_savefile(investigator: &str) -> std::io::Result<()> {
    let path = savegame_path(investigator)?;
    let _ = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create_new(true)
        .open(path)?;
    Ok(())
}

pub fn delete(investigator: &str) -> std::io::Result<()> {
    let path = savegame_path(investigator)?;
    remove_file(path)
}

pub fn load_game(investigator: &str) -> std::io::Result<Game> {
    let path = savegame_path(investigator)?;
    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = Vec::new();
    buf_reader.read_to_end(&mut contents)?;
    let game: Game = bincode::deserialize(&contents).expect("deserialize");
    Ok(game)
}

pub fn save_game(game: &Game, investigator: &str) -> std::io::Result<()> {
    let savegame = bincode::serialize(game).expect("savegame");
    let path = savegame_path(investigator)?;
    std::fs::write(path, savegame)?;
    Ok(())
}

use ritd_world::{
    can_place, Actor, Alignment, Coord, CoordIndex, DoorState, FloorCoord, FloorKind, FloorPlan,
    Item, ItemCoord, Key, Room, RoomType, Tile, TileKind, TileMap, WallKind, World, FLOORS,
    FLOOR_COLS, FLOOR_ROWS,
};
use ritd_world::{Furniture, RoomPlan};

use crate::actor_gen::*;
use crate::floor_plan_gen::*;
use rand::prelude::*;
use rand::Rng;

fn tile_map_gen<R: Rng>(plan: &[FloorPlan; FLOORS], rng: &mut R) -> TileMap {
    let mut tile_map = TileMap::default();

    for (floor, floor_plan) in plan.iter().enumerate() {
        tile_map
            .add_floor([[Tile::new(TileKind::Floor(FloorKind::Stone)); FLOOR_ROWS]; FLOOR_COLS]);
        build_outer_walls(&mut tile_map, floor);

        for room in &floor_plan.rooms {
            build_room(&mut tile_map, room, floor, rng);
        }
    }

    // tile_map.add_water(rng);
    // tile_map.add_vegetation(rng);
    tile_map
}

pub fn world_gen<R: Rng>(rng: &mut R) -> World {
    let plan = floor_plan_gen(rng);
    let tile_map = tile_map_gen(&plan, rng);
    let mut world = World::new(plan, tile_map);

    add_windows(&mut world, rng);
    lock_front_door(&mut world);
    place_keys(&mut world, rng);
    furnish(&mut world, rng);
    // evidence(&mut world, rng);
    populate(&mut world, rng);

    world
}

fn choose_actor_coord<R: Rng>(
    world: &World,
    room: &Room,
    floor: CoordIndex,
    actor: &Actor,
    rng: &mut R,
) -> Option<FloorCoord> {
    room.plan
        .inside_coords()
        .into_iter()
        .map(|coord| FloorCoord { coord, floor })
        .filter(|&coord| can_place(world, actor, coord).is_ok())
        .choose(rng)
}

// Populate the floor with mobs.
fn populate<R: Rng>(world: &mut World, rng: &mut R) {
    let plan = world.plan().clone();

    for (floor, floor_plan) in plan.iter().enumerate() {
        for room in &floor_plan.rooms {
            let doors = room.plan.doors.len();
            let (min, max) = (doors.saturating_sub(2), doors);
            for _ in 0..rng.gen_range(min..=max) {
                if let Some(&actor_kind) = room.room_type.mob_kind().choose(rng) {
                    let actor = Actor::new(actor_kind, Alignment::Dungeon);
                    if let Some(coord) = choose_actor_coord(world, room, floor, &actor, rng) {
                        can_place(world, &actor, coord).expect("???");
                        // .choose_weighted(rng, |actor_kind| actor_kind.occurrence_prob(level))
                        let _ = gen_actor(world, actor, coord, rng);
                    }
                }
            }
        }
    }
}

fn choose_item_coord<R: Rng>(
    world: &World,
    room: &Room,
    floor: CoordIndex,
    rng: &mut R,
) -> Option<FloorCoord> {
    room.plan
        .inside_coords()
        .into_iter()
        .map(|coord| FloorCoord { coord, floor })
        .filter(|&coord| world.can_place_item(ItemCoord::Lay(coord)).is_ok())
        .choose(rng)
}

/// Furnish the floor with items.
fn furnish<R: Rng>(world: &mut World, rng: &mut R) {
    // const MAX_ITEMS: usize = 5;
    let plan = world.plan().clone();
    // let level = plan.floor as u8;

    for (floor, floor_plan) in plan.iter().enumerate() {
        for room in &floor_plan.rooms {
            // let items_of_furniture = rng.gen_range(1..=5);
            // for &coord in room
            //     .plan
            //     .inside_coords()
            //     .choose_multiple(rng, items_of_furniture)
            // {
            //     if let Some(&furniture) = room.room_type.furniture().choose(rng) {
            //         let center = FloorCoord {
            //             coord,
            //             floor: floor as CoordIndex,
            //         };
            //         world.add_furniture(center, furniture).expect("set feature");
            //     }
            // }

            // let doors = room.plan.doors.len();
            // let (min, max) = (0, MAX_ITEMS.saturating_sub(doors) + 1);
            for item in room.room_type.item_kind() {
                if let Some(coord) = choose_item_coord(world, room, floor as CoordIndex, rng) {
                    let _ = world.add_and_place_item(*item, ItemCoord::Lay(coord));
                }
            }
            // for _ in 0..rng.gen_range(min..=max) {
            //     if let Some(coord) = choose_item_coord(world, room, floor as CoordIndex, rng) {
            //         if let Ok(&item) = room
            //             .room_type
            //             .item_kind()
            //             .choose_weighted(rng, |item| occurrence(item, floor))
            //         {
            //             let _ = world.add_and_place_item(item, ItemCoord::Lay(coord));
            //             // world.place_item(item_key, coord).expect("place item");
            //         }
            //     }
            // }
        }
    }
}

// fn occurrence(_item: &Item, _floor: usize) -> u16 {
//     1
// }

// /// Add one piece of evidence in each floor.
// fn evidence<R: Rng>(world: &mut World, rng: &mut R) {
//     let plan = world.plan().clone();
//     for (floor, floor_plan) in plan.iter().enumerate() {
//         let room = floor_plan.rooms.choose(rng).expect("choose a room");
//         if let Some(coord) = choose_item_coord(world, room, floor as CoordIndex, rng) {
//             let _ = world.add_and_place_item(Item::Necronomicon, ItemCoord::Lay(coord));
//         }
//     }
// }

pub fn build_room<R: Rng>(tile_map: &mut TileMap, room: &Room, floor: CoordIndex, rng: &mut R) {
    let plan = &room.plan;

    let wall_type = room.room_type.wall();
    let floor_type = room.room_type.floor();

    // Corridors and Halls don't need walls
    if let RoomType::Corridor | RoomType::Hall = room.room_type {
        return;
    } else {
        for coord in plan.floor_coords() {
            tile_map
                .place_tile(
                    FloorCoord { coord, floor },
                    Tile::new(TileKind::Floor(floor_type)),
                )
                .expect("place tile");
        }
        for coord in plan.wall_coords() {
            tile_map
                .place_tile(
                    FloorCoord { coord, floor },
                    Tile::new(TileKind::Wall(wall_type)),
                )
                .expect("place tile");
        }
    }

    match room.room_type {
        RoomType::Stairs => build_stairs(plan, tile_map, floor),
        RoomType::Landing => build_landing(plan, tile_map, floor),
        RoomType::Library => build_library(tile_map, plan, floor, rng),
        RoomType::Dining => build_dining_room(tile_map, plan, floor),
        RoomType::Storage => build_storage(tile_map, plan, floor, rng),
        RoomType::Temple => build_temple(tile_map, plan, floor),
        RoomType::Kitchen => build_kitchen(tile_map, plan, floor),
        RoomType::Bedroom => build_bedroom(tile_map, plan, floor),
        _ => {}
    }

    // Add doors
    let door_state = match room.locked {
        Some(key) => DoorState::Locked(key),
        None => DoorState::Closed,
    };
    for (door_side, index) in &plan.doors {
        let coord = plan.door_coord(*door_side, *index);
        let floor_coord = FloorCoord { coord, floor };
        let door = Tile::new(TileKind::Door(door_state));
        let tile = tile_map.tile(floor_coord).expect("tile");
        if let TileKind::Door(DoorState::Locked(previous_key)) = tile.kind {
            if let Some(room_key) = room.locked {
                if previous_key < room_key {
                    tile_map.place_tile(floor_coord, door).expect("place tile");
                } else {
                    continue;
                }
            } else {
                continue;
            }
        } else {
            tile_map.place_tile(floor_coord, door).expect("place tile");
        }
    }
}

fn build_landing(plan: &RoomPlan, tile_map: &mut TileMap, floor: usize) {
    let center = plan.center();
    tile_map
        .place_tile(
            FloorCoord {
                coord: center,
                floor,
            },
            Tile::new(TileKind::StairsUp),
        )
        .expect("place tile");
}

fn build_stairs(plan: &RoomPlan, tile_map: &mut TileMap, floor: usize) {
    let center = plan.center();
    tile_map
        .place_tile(
            FloorCoord {
                coord: center,
                floor,
            },
            Tile::new(TileKind::StairsDown),
        )
        .expect("place tile");
}

pub fn build_outer_walls(tile_map: &mut TileMap, floor: CoordIndex) {
    for row in 0..FLOOR_ROWS {
        for col in 0..FLOOR_COLS {
            if row == 0 || row == FLOOR_ROWS - 1 || col == 0 || col == FLOOR_COLS - 1 {
                let coord = Coord::new(row, col).expect("coords already satisfy bounds");
                tile_map
                    .place_tile(
                        FloorCoord { coord, floor },
                        Tile::new(TileKind::Wall(WallKind::Brick)),
                    )
                    .expect("place tile");
            }
        }
    }
}

fn place_keys<R: Rng>(world: &mut World, rng: &mut R) {
    let is_unlocked: fn(&&Room) -> bool = |room| {
        room.locked.is_none() && !matches!(room.room_type, RoomType::Hall | RoomType::Corridor)
    };

    let is_locked: fn(&&Room, Key) -> bool = |room, key| room.locked == Some(key);

    let plan = world.plan().clone();

    for (floor, floor_plan) in plan.iter().enumerate() {
        let locked_rooms = |key| {
            floor_plan
                .rooms
                .iter()
                .filter(|room| is_locked(room, key))
                .count()
        };

        floor_plan
            .rooms
            .iter()
            .filter(is_unlocked)
            .choose_multiple(rng, locked_rooms(Key::Iron))
            .into_iter()
            .for_each(|room| place_key(world, room, floor, Key::Iron, rng));

        floor_plan
            .rooms
            .iter()
            .filter(|room| is_locked(room, Key::Iron))
            .choose_multiple(rng, locked_rooms(Key::Brass))
            .into_iter()
            .for_each(|room| place_key(world, room, floor, Key::Brass, rng));

        let _ = floor_plan
            .rooms
            .iter()
            .filter(|room| is_locked(room, Key::Brass))
            .choose(rng)
            .map(|room| place_key(world, room, floor, Key::Silver, rng));
        // .into_iter()
        // .for_each(|room| place_key(world, room, floor, Key::Silver, rng));
    }
}

fn place_key<R: Rng>(world: &mut World, room: &Room, floor: usize, key: Key, rng: &mut R) {
    if let Some(coord) = choose_item_coord(world, room, floor as CoordIndex, rng) {
        let _ = world.add_and_place_item(Item::Key(key), ItemCoord::Lay(coord));
    }
}

fn lock_front_door(world: &mut World) {
    let front_door = Coord::new(
        FLOOR_ROWS as CoordIndex - 1,
        FLOOR_COLS as CoordIndex / 2 - 1,
    )
    .expect("coord should exist");

    world
        .place_tile(
            FloorCoord {
                coord: front_door,
                floor: FLOORS as CoordIndex / 2,
            },
            Tile::new(TileKind::Door(DoorState::Locked(Key::Silver))),
        )
        .expect("place tile");
}

fn add_windows<R: Rng>(world: &mut World, rng: &mut R) {
    for i in 0..FLOOR_ROWS as CoordIndex / 2 {
        let row = i * 2 + 1;

        if rng.gen_bool(0.5) {
            world
                .place_tile(
                    FloorCoord {
                        coord: Coord::new(row, 0).expect("coord should exist"),
                        floor: FLOORS as CoordIndex / 2,
                    },
                    Tile::new(TileKind::Window),
                )
                .expect("place tile");
        }
        if rng.gen_bool(0.5) {
            world
                .place_tile(
                    FloorCoord {
                        coord: Coord::new(row, FLOOR_COLS as CoordIndex - 1)
                            .expect("coord should exist"),
                        floor: FLOORS as CoordIndex / 2,
                    },
                    Tile::new(TileKind::Window),
                )
                .expect("place tile");
        }
    }

    for j in 0..FLOOR_COLS as CoordIndex / 2 {
        let col = j * 2 + 1;

        if rng.gen_bool(0.5) {
            world
                .place_tile(
                    FloorCoord {
                        coord: Coord::new(0, col).expect("coord should exist"),
                        floor: FLOORS as CoordIndex / 2,
                    },
                    Tile::new(TileKind::Window),
                )
                .expect("place tile");
        }
        if rng.gen_bool(0.5) {
            world
                .place_tile(
                    FloorCoord {
                        coord: Coord::new(FLOOR_ROWS as CoordIndex - 1, col)
                            .expect("coord should exist"),
                        floor: FLOORS as CoordIndex / 2,
                    },
                    Tile::new(TileKind::Window),
                )
                .expect("place tile");
        }
    }
}

pub fn build_library<R: Rng>(
    tile_map: &mut TileMap,
    room_plan: &RoomPlan,
    floor: CoordIndex,
    rng: &mut R,
) {
    let vstep: usize = if rng.gen() { 2 } else { 1 };
    let hstep = 3 - vstep;
    for row in (room_plan.north..room_plan.south - 1)
        .skip(2)
        .step_by(vstep)
    {
        for col in (room_plan.west..room_plan.east - 1).skip(2).step_by(hstep) {
            let coord = FloorCoord {
                coord: Coord::new(row, col).expect("coords are within bounds"),
                floor: floor as CoordIndex,
            };
            tile_map
                .add_furniture(coord, Furniture::Library { searched: false })
                .expect("set feature");
        }
    }
}

pub fn build_dining_room(tile_map: &mut TileMap, room_plan: &RoomPlan, floor: CoordIndex) {
    if room_plan.south - room_plan.north > room_plan.east - room_plan.west {
        for row in (room_plan.north..room_plan.south - 1).skip(2) {
            let col = (room_plan.east + room_plan.west) / 2;
            let coord = FloorCoord {
                coord: Coord::new(row, col).expect("coords within bounds"),
                floor,
            };
            tile_map
                .add_furniture(coord, Furniture::Table)
                .expect("set feature");
            if row % 2 == 0 {
                let coord = FloorCoord {
                    coord: Coord::new(row, col - 1).expect("coords within bounds"),
                    floor,
                };
                tile_map
                    .add_furniture(coord, Furniture::Chair)
                    .expect("set feature");

                let coord = FloorCoord {
                    coord: Coord::new(row, col + 1).expect("coords within bounds"),
                    floor,
                };
                tile_map
                    .add_furniture(coord, Furniture::Chair)
                    .expect("set feature");
            }
        }
    } else {
        for col in (room_plan.west..room_plan.east - 1).skip(2) {
            let row = (room_plan.north + room_plan.south) / 2;
            let coord = FloorCoord {
                coord: Coord::new(row, col).expect("coords within bounds"),
                floor,
            };
            tile_map
                .add_furniture(coord, Furniture::Table)
                .expect("set feature");
            if col % 2 == 0 {
                let coord = FloorCoord {
                    coord: Coord::new(row - 1, col).expect("coords within bounds"),
                    floor,
                };
                tile_map
                    .add_furniture(coord, Furniture::Chair)
                    .expect("set feature");

                let coord = FloorCoord {
                    coord: Coord::new(row + 1, col).expect("coords within bounds"),
                    floor,
                };
                tile_map
                    .add_furniture(coord, Furniture::Chair)
                    .expect("set feature");
            }
        }
    }
}

fn build_storage<R: Rng>(
    tile_map: &mut TileMap,
    room_plan: &RoomPlan,
    floor: CoordIndex,
    rng: &mut R,
) {
    let crates =
        (room_plan.south - room_plan.north - 1) * (room_plan.east - room_plan.west - 1) / 4;
    for &coord in room_plan.inside_coords().choose_multiple(rng, crates) {
        let coord = FloorCoord {
            coord,
            floor: floor as CoordIndex,
        };
        tile_map
            .add_furniture(coord, Furniture::Crate { open: false })
            .expect("set feature");
    }
}

pub fn build_temple(tile_map: &mut TileMap, room_plan: &RoomPlan, floor: CoordIndex) {
    let center = room_plan.center();
    for delta_row in &[-1, 1] {
        for delta_col in &[-1, 1] {
            let coord = FloorCoord {
                coord: Coord::new(
                    (center.row() as isize + delta_row) as CoordIndex,
                    (center.col() as isize + delta_col) as CoordIndex,
                )
                .expect("coords within bounds"),
                floor,
            };
            tile_map
                .place_tile(coord, Tile::new(TileKind::Wall(WallKind::Stone)))
                .expect("coords within bounds");
        }
    }
    let coord = FloorCoord {
        coord: center,
        floor,
    };
    tile_map
        .add_furniture(coord, Furniture::Altar { used: false })
        .expect("set feature");
}

pub fn build_kitchen(tile_map: &mut TileMap, room_plan: &RoomPlan, floor: CoordIndex) {
    let center = room_plan.center();
    let coord = FloorCoord {
        coord: center,
        floor,
    };
    tile_map
        .add_furniture(coord, Furniture::Stove { searched: false })
        .expect("set feature");
    for delta_col in &[-1, 1] {
        let coord = FloorCoord {
            coord: Coord::new(
                center.row(),
                (center.col() as isize + delta_col) as CoordIndex,
            )
            .expect("coords within bounds"),
            floor,
        };
        tile_map
            .add_furniture(coord, Furniture::Table)
            .expect("coords within bounds");
    }
}

pub fn build_bedroom(tile_map: &mut TileMap, room_plan: &RoomPlan, floor: CoordIndex) {
    let center = room_plan.center();
    let coord = FloorCoord {
        coord: center,
        floor,
    };
    tile_map
        .add_furniture(coord, Furniture::Bed)
        .expect("set feature");
    // for delta_col in &[-1, 1] {
    //     let coord = FloorCoord {
    //         coord: Coord::new(
    //             center.row(),
    //             (center.col() as isize + delta_col) as CoordIndex,
    //         )
    //         .expect("coords within bounds"),
    //         floor,
    //     };
    //     tile_map
    //         .add_furniture(coord, Furniture::Table)
    //         .expect("coords within bounds");
    // }
}

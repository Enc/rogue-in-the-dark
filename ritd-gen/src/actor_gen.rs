use ritd_world::{Actor, ActorKey, FloorCoord, Item, World};

use rand::Rng;

pub fn gen_actor<R: Rng>(
    world: &mut World,
    actor: Actor,
    coord: FloorCoord,
    rng: &mut R,
) -> ActorKey {
    let equipment = actor.kind().equipment();
    let opt_armor = actor.kind().armor();
    let opt_weapon = actor.kind().weapon();
    let actor_key = world.insert_actor(actor, coord).expect("add actor");
    for &(item, prob) in equipment {
        if rng.gen_bool(prob) {
            let item_key = world.add_item(item).expect("add item");
            world
                .add_to_inventory(actor_key, item_key)
                .expect("add item to inventory");
        }
    }
    if let Some(armor_kind) = opt_armor {
        let armor = Item::Wearable(armor_kind);
        let item_key = world.add_item(armor).expect("add item");
        world
            .add_to_inventory(actor_key, item_key)
            .expect("add item to inventory");
        world
            .equip_armor_from_inventory(actor_key, item_key)
            .expect("equipped weapon");
    }

    if let Some(weapon_kind) = opt_weapon {
        let weapon = Item::Weapon(weapon_kind);
        let item_key = world.add_item(weapon).expect("add item");
        world
            .add_to_inventory(actor_key, item_key)
            .expect("add item to inventory");
        world
            .equip_weapon_from_inventory(actor_key, item_key)
            .expect("equipped weapon");
    }

    // heal(world, actor_key).expect("heal new mob");

    actor_key
}

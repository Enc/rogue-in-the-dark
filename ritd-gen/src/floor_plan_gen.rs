use core::panic;

use ritd_world::{
    CoordIndex, Dir, Floor, FloorPlan, Key, Room, RoomPlan, RoomType, FLOORS, FLOOR_COLS,
    FLOOR_ROWS,
};

use rand::prelude::*;
use rand::Rng;

pub fn floor_plan_gen<R: Rng>(rng: &mut R) -> [FloorPlan; FLOORS] {
    [
        // generate_floor_plan(Floor::Crypt, rng),
        // generate_floor_plan(Floor::Cellar, rng),
        generate_floor_plan(Floor::DayFloor, rng),
        // generate_floor_plan(Floor::NightFloor, rng),
        // generate_floor_plan(Floor::Attic, rng),
    ]
}

fn generate_floor_plan<R: Rng>(floor: Floor, rng: &mut R) -> FloorPlan {
    const IRON_DOORS: usize = 5;
    const BRASS_DOORS: usize = 3;
    // const SILVER_DOORS: usize = 1;
    const MAX_DOORS: usize = 3;

    let has_landing = false;
    let has_stairs = false;
    let mut rooms = Vec::<Room>::new();
    let mut stairs = None;
    let mut landing = None;
    let mut room_plans = bsp(rng);
    room_plans.shuffle(rng);

    let mut room_plans = room_plans
        .into_iter()
        .map(|plan| (plan, None))
        .collect::<Vec<(RoomPlan, Option<Key>)>>();
    // lock_rooms(&mut room_plans, Key::Silver, SILVER_DOORS, rng);
    lock_rooms(&mut room_plans, Key::Brass, BRASS_DOORS, rng);
    lock_rooms(&mut room_plans, Key::Iron, IRON_DOORS, rng);
    // lock_front_door(&mut room_plans);

    for (plan, locked) in room_plans {
        let is_corridor = plan.south - plan.north <= 2 || plan.east - plan.west <= 2;
        let room_type;

        if is_corridor {
            room_type = RoomType::Corridor;
        } else if has_stairs && stairs.is_none() {
            stairs = Some(plan.center());
            room_type = RoomType::Stairs;
        } else if has_landing && landing.is_none() {
            landing = Some(plan.center());
            room_type = RoomType::Landing;
        } else if let Some(key) = locked {
            room_type = *match key {
                // Key::Iron | Key::Brass => RoomType::private_rooms(floor),
                // Key::Silver => RoomType::secret_rooms(),
                Key::Iron => RoomType::private_rooms(floor),
                Key::Brass => RoomType::secret_rooms(),
                _ => panic!("there should be no room locked by Silver key"),
            }
            .choose(rng)
            .expect("choose a room type");
        } else if plan.doors.len() <= MAX_DOORS {
            room_type = *RoomType::public_rooms(floor)
                .choose(rng)
                .expect("choose a room type");
        } else {
            room_type = RoomType::Hall;
        };

        rooms.push(Room {
            room_type,
            plan,
            locked,
        });
    }

    FloorPlan {
        rooms,
        landing,
        stairs,
    }
}

fn bsp<R: Rng>(rng: &mut R) -> Vec<RoomPlan> {
    let front_door = (Dir::South, FLOOR_COLS as CoordIndex / 2 - 1);
    let starting_room = RoomPlan {
        north: 0,
        south: FLOOR_ROWS as CoordIndex - 1,
        west: 0,
        east: FLOOR_COLS as CoordIndex - 1,
        doors: vec![front_door],
    };

    let mut room_plans = vec![starting_room];
    let mut area_plans = Vec::default();
    let mut final_room_plans = Vec::default();

    // Generate areas' plans and corridors
    while let Some(room_plan) = room_plans.pop() {
        if let Some((a_plan, b_plan, corridor)) = room_plan.split_corridor(rng) {
            room_plans.push(a_plan);
            room_plans.push(b_plan);
            final_room_plans.push(corridor)
        } else {
            area_plans.push(room_plan);
        }
    }

    // Generate rooms' plans
    while let Some(room_plan) = area_plans.pop() {
        if let Some((a_plan, b_plan)) = room_plan.split(rng) {
            area_plans.push(a_plan);
            area_plans.push(b_plan);
        } else {
            final_room_plans.push(room_plan);
        }
    }

    final_room_plans
}

fn lock_rooms<R: Rng>(rooms: &mut [(RoomPlan, Option<Key>)], key: Key, num: usize, rng: &mut R) {
    let unlocked_rooms = rooms.to_owned();

    let lockable = |plan: &RoomPlan| {
        plan.doors
            .iter()
            .filter(|(door_side, index)| {
                let door_coord = plan.door_coord(*door_side, *index);
                if let Some((_, adjacent_room_key)) = unlocked_rooms.iter().find(|(room, _)| {
                    if let Ok(coord_dir) = door_coord + *door_side {
                        room.contains(coord_dir)
                    } else {
                        false
                    }
                }) {
                    adjacent_room_key.is_none()
                } else {
                    true
                }
            })
            .count()
            == 1
    };

    for (_, locked) in rooms
        .iter_mut()
        .filter(|(plan, locked)| locked.is_none() && lockable(plan))
        .choose_multiple(rng, num)
    {
        let _ = locked.replace(key);
    }
}

// fn lock_front_door(rooms: &mut [(RoomPlan, Option<Key>)]) {
//     let (_, key) = rooms
//         .iter_mut()
//         .find(|(plan, _)| {
//             plan.contains(Coord {
//                 row: FLOOR_ROWS as CoordIndex - 1,
//                 col: FLOOR_COLS as CoordIndex / 2 - 1,
//             })
//         })
//         .expect("All area is covered by rooms");
//     *key = Some(Key::Silver);
// }
